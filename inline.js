// check for theme
if (typeof(Storage) !== "undefined") {
	var root = document.getElementsByTagName( 'html' )[0],
		theme = localStorage.getItem("altair_theme");
	if(theme == 'app_theme_dark' || root.classList.contains('app_theme_dark')) {
		root.className += ' app_theme_dark';
	}
}

$( document ).ready(function() {
  var x = jstz.determine().name();

  date = new Date(); //  criando o COOKIE com a data atual
  date.setTime(date.getTime()+(1*24*60*60*1000));
  expires = date.toUTCString();

  document.cookie = "timezone="+x+";expires="+expires+";";
})


  $(function () {

	var error = "";
	if(error == 'user_error' || error == 'error_sent_email'){
		$("#login-content").hide();
		$("#forgot-password-content").show('slow'); 
	}
  });

  function validateLogin(){
	var usuario = $("#usuario").val();
	var senha   = $("#senha").val();   
	
	if(!usuario){
	  $("#resp-login").show('slow').html('<i class="material-icons uk-text-danger">info</i> Please enter a valid e-mail');
	  $("#usuario").focus();
	  return false;
	} 

	if(usuario){

	  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	  if(!usuario.match(mailformat)){
		$("#resp-login").show('slow').html('<i class="material-icons uk-text-danger">info</i> Please enter a valid e-mail');
		$("#usuario").focus();
		return false;
	  }      
	}    

	//armazena o email de acesso no local storage
	localStorage.setItem("4payUser", usuario);

	//4payUserBlocked: recebe Y para determinar que o bloqueio ocorreu, assim se o user atualizar a tela, o modal de bloqueio abre novamente baseado neste. Neste ponto mudamnos o valor para N, pois o usuário validou a senha.
	localStorage.setItem("4payUserBlocked", 'N');

	if(!senha){
	  $("#resp-login").show('slow').html('<i class="material-icons uk-text-danger">info</i> Please enter your password');
	  $("#senha").focus();
	  return false;
	}

  } // END validateLogin

  function validateForgotPass(){
	var email = $("#email").val();
	
	if(!email){
	  $("#resp-forgot").show('slow').html('<i class="material-icons uk-text-danger">info</i> Please enter a valid e-mail');
	  $("#email").focus();
	  return false;
	} 

	if(email){

	  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	  if(!email.match(mailformat)){
		$("#resp-forgot").show('slow').html('<i class="material-icons uk-text-danger">info</i> Please enter a valid e-mail');
		$("#email").focus();
		return false;
	  }      
	}    

  }// END validateForgotPass

  function LoginOrForgotLogin(visible){
	if(visible == 'login'){
	  $("#login-content").show('slow');
	  $("#forgot-password-content").hide();
	} else {
	  $("#login-content").hide();
	  $("#forgot-password-content").show('slow');
	}
	return false;
  }

  function resendEmail(visible){
	if(visible == 'login'){
	  $("#login-content").show('slow');
	  $("#resend-email-content").hide();
	} else {
	  $("#login-content").hide();
	  $("#resend-email-content").show('slow');
	}
	return false;
  }

  function validateResendEmail(){

	var email = $("#email_resend").val();
	//console.log('email= ' + email)
	if(!email){
	  $("#resp-resend").show('slow').html('<i class="material-icons uk-text-danger">info</i> Please enter a valid e-mail');
	  $("#email_resend").focus();
	  return false;
	} 

	if(email){

	  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	  if(!email.match(mailformat)){
		$("#resp-resend").show('slow').html('<i class="material-icons uk-text-danger">info</i> Please enter a valid e-mail');
		$("#email_resend").focus();
		return false;
	  }      
	}

	var sendData = {
	  email: email
	}

	$.ajax({ 
	  type: "POST",
	  url: "painel/accounts/resendEmail",
	  data: sendData,
	  dataType: "html",
	  beforeSend: function(){             
	  },
	  success: function(r){ 
		console.log(r);
		switch(r) {
			case 'Enviou':
				$(".msg-success").show('slow');
				$("#resp-resend").hide();
				$("#content-msg-resend").show('slow');
				$("#content-msg-success").html('An e-mail was sent to you with the access instructions.')
				break;
			case 'Não Enviou':
				$(".msg-success").hide();
				$("#content-msg-resend").show('slow');
				$("#resp-resend").show('slow').html('There was an error processing your request.');
				break;
			case 'email_not_exists':
				$(".msg-success").hide();
				$("#content-msg-resend").show('slow');
				$("#resp-resend").show('slow').html('E-mail not found.');
				break;                
			default:
				break;
		}

	  },
	  complete:function(){
		
	  },
	  error: function(r){
		//alert(r.status);
	  }
	});    
	return false;

  }// END validateResendEmail


  function validateRegister(){

	var NAME        = $("#NAME").val();//company
	var FST_NAME    = $("#FST_NAME").val();
	var LAST_NAME   = $("#LAST_NAME").val();
	var EMAIL_ADDR  = $("#EMAIL_ADDR").val();
	var WORK_PH_NUM = $("#WORK_PH_NUM").val();
	var CEL_PH_NUM  = $("#CEL_PH_NUM").val();
	var ALT_PH_NUM  = $("#ALT_PH_NUM").val();
	var ADDRESS     = $("#ADDRESS").val();
	var ADDRESS2    = $("#ADDRESS2").val();
	var CITY        = $("#CITY").val();
	var STATE       = $("#STATE").val();
	var COUNTRY     = $("#COUNTRY").val();
	var POSTALCODE  = $("#POSTALCODE").val();
	var erro_form   = "<span class='error uk-text-danger'>Este campo é obrigatório</span>";
	var erro_mail   = "<span class='error uk-text-danger'>Please enter a valid e-mail</span>"

	if (NAME){
		$('.error').remove();
	} else {
		$(erro_form).insertBefore("#NAME");
		$("#NAME").focus();
		return false;
	}

	if (FST_NAME){
		$('.error').remove();
	} else {
		$(erro_form).insertBefore("#FST_NAME");
		$("#FST_NAME").focus();
		return false;
	} 

	if (LAST_NAME){
		$('.error').remove();
	} else {
		$(erro_form).insertBefore("#LAST_NAME");
		$("#LAST_NAME").focus();
		return false;
	} 

	if (EMAIL_ADDR){
		$('.error').remove();
	} else {
		$(erro_mail).insertBefore("#EMAIL_ADDR");
		$("#EMAIL_ADDR").focus();
		return false;
	} 

	if(EMAIL_ADDR){
	  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	  if(!EMAIL_ADDR.match(mailformat)){
		$(erro_mail).insertBefore("#EMAIL_ADDR");
		$("#EMAIL_ADDR").focus();
		return false;
	  }      
	}

	if (ADDRESS){
		$('.error').remove();
	} else {
		$(erro_form).insertBefore("#ADDRESS");
		$("#ADDRESS").focus();
		return false;
	}

	if (CITY){
		$('.error').remove();
	} else {
		$(erro_form).insertBefore("#CITY");
		$("#CITY").focus();
		return false;
	}

	if (STATE){
		$('.error').remove();
	} else {
		$(erro_form).insertBefore("#STATE");
		$("#STATE").focus();
		return false;
	}

	if (COUNTRY){
		$('.error').remove();
	} else {
		$(erro_form).insertBefore("#COUNTRY");
		$("#COUNTRY").focus();
		return false;
	}

	if (POSTALCODE){
		$('.error').remove();
	} else {
		$(erro_form).insertBefore("#POSTALCODE");
		$("#POSTALCODE").focus();
		return false;
	}                        

	var NAME        = $("#NAME").val();//company
	var FST_NAME    = $("#FST_NAME").val();
	var LAST_NAME   = $("#LAST_NAME").val();
	var EMAIL_ADDR  = $("#EMAIL_ADDR").val();
	var WORK_PH_NUM = $("#WORK_PH_NUM").val();
	var CEL_PH_NUM  = $("#CEL_PH_NUM").val();
	var ALT_PH_NUM  = $("#ALT_PH_NUM").val();
	var ADDRESS     = $("#ADDRESS").val();
	var ADDRESS2    = $("#ADDRESS2").val();
	var CITY        = $("#CITY").val();
	var STATE       = $("#STATE").val();
	var COUNTRY     = $("#COUNTRY").val();
	var POSTALCODE  = $("#POSTALCODE").val();


	var sendData = {
		NAME        : NAME,
		FST_NAME    : FST_NAME,
		LAST_NAME   : LAST_NAME,
		EMAIL_ADDR  : EMAIL_ADDR,
		WORK_PH_NUM : WORK_PH_NUM,
		CEL_PH_NUM  : CEL_PH_NUM,
		ALT_PH_NUM  : ALT_PH_NUM,
		ADDRESS     : ADDRESS,
		ADDRESS2    : ADDRESS2,
		CITY        : CITY,
		STATE       : STATE,
		COUNTRY     : COUNTRY,
		POSTALCODE  : POSTALCODE,
		origem      : 'ENTRY-PAGE'
	}

	$.ajax({ 
	  type: "POST",
	  url: "painel/accounts/sign_up",
	  data: sendData,
	  dataType: "html",
	  beforeSend: function(){             
	  },
	  success: function(r){ 
		//console.log(r);
		data = JSON.parse(r);
		var error = data.error;
		var message = data.message;
		$(".msg-success").show('slow');
		$("#resp-register").hide();
		if(error == 0){
		  $("#content-msg-register").removeClass('uk-alert-danger').addClass('uk-alert-success').show('slow');
		  $("#content-msg-content-register").html(message);
		} else {
		  $("#content-msg-register").removeClass('uk-alert-success').addClass('uk-alert-danger').show('slow');
		  $("#content-msg-content-register").html(message);
		}


	  },
	  complete:function(){            
	  },
	  error: function(r){
		//alert(r.status);
	  }
	});    
	return false;

  }// END validateRegister      
