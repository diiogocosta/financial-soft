(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-reason-codes-reason-codes-module"],{

/***/ "./src/app/modules/reason-codes/form/reason-codes-form.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/modules/reason-codes/form/reason-codes-form.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#page_content_inner{\n    padding-bottom: 24px !important;\n}\n\n.uk-grid{\n    margin-top: 0px !important;\n}\n\n.uk-modal-footer{\n    margin-top: 0px !important;\n}\n\nmat-radio-button{\n    margin-right: 8px;\n}\n\nmat-label{\n    color: rgba(0, 0, 0, 0.54) !important;\n    font-size: 14px !important;\n}\n\n::ng-deep .mat-form-field-underline{    \n    background-color: rgb(225, 225, 225) !important;\n}\n\n::ng-deep \n.cdk-overlay-container {\n    z-index: 99999 !important;\n  }"

/***/ }),

/***/ "./src/app/modules/reason-codes/form/reason-codes-form.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/modules/reason-codes/form/reason-codes-form.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"uk-modal-header\">\n    <h3 class=\"uk-modal-title\"><i class=\"material-icons\">playlist_add</i> {{title()}}</h3>\n</div>\n<form class=\"form_validation\" [formGroup]=\"form\" novalidate>\n    <div class=\"uk-grid uk-margin-small2-top\" data-uk-grid-margin>\n        <div class=\"uk-width-medium-1-2 uk-row-first\">\n            <div class=\"parsley-row\">\n                <mat-form-field class=\"full-width\">\n                    <input required matInput placeholder=\"Code\" maxlength=\"10\" formControlName=\"CODE\">\n                    <mat-error *ngIf=\"f('CODE').invalid\">{{getErrorMessage(f('CODE'))}}</mat-error>\n                </mat-form-field>\n            </div>\n        </div>\n\n        <div class=\"uk-width-medium-1-2 uk-row-first\">\n            <div class=\"parsley-row\">\n                <mat-form-field class=\"full-width\">\n                    <input required matInput placeholder=\"Description\" maxlength=\"255\" formControlName=\"DESCRIPTION\">\n                    <mat-error *ngIf=\"f('DESCRIPTION').invalid\">{{getErrorMessage(f('DESCRIPTION'))}}</mat-error>\n                </mat-form-field>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"uk-grid uk-margin-small2-top\" data-uk-grid-margin>\n        <div class=\"uk-width-medium-1-2 uk-row-first\">\n            <div class=\"parsley-row\">\n                <mat-form-field class=\"full-width\">\n                    <mat-select placeholder=\"Provider\" formControlName=\"PROVIDER_ID\" required>\n                        <mat-option *ngFor=\"let provider of providers\" [value]=\"provider.ID\">\n                            {{provider.NAME}}\n                        </mat-option>\n                    </mat-select>\n                    <mat-error *ngIf=\"f('PROVIDER_ID').invalid\">{{getErrorMessage(f('PROVIDER_ID'))}}</mat-error>\n                </mat-form-field>\n            </div>\n        </div>\n\n        <div class=\"uk-width-medium-1-2 uk-row-first\">\n            <div class=\"parsley-row\">\n                <mat-form-field class=\"full-width\">\n                    <mat-select placeholder=\"Internal Code\" formControlName=\"INTERNAL_PROV_REASON_CODE_ID\" required>\n                        <mat-option *ngFor=\"let provider of providers\" [value]=\"provider.ID\">\n                            {{provider.NAME}}\n                        </mat-option>\n                    </mat-select>\n                    <mat-error *ngIf=\"f('INTERNAL_PROV_REASON_CODE_ID').invalid\">{{getErrorMessage(f('INTERNAL_PROV_REASON_CODE_ID'))}}</mat-error>\n                </mat-form-field>\n            </div>\n        </div>\n\n    </div>\n\n    <div class=\"uk-modal-footer uk-text-right\">\n        <div class=\"uk-width-medium-1-1 uk-row-first\">\n            <button type=\"button\" class=\"md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light\" (click)=\"doSave()\">Save</button>\n            <button type=\"button\" class=\"md-btn md-btn-secondary md-btn-wave-light waves-effect waves-button waves-light\" (click)=\"submit.emit()\">Back</button>\n        </div>\n    </div>\n</form>"

/***/ }),

/***/ "./src/app/modules/reason-codes/form/reason-codes-form.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/modules/reason-codes/form/reason-codes-form.component.ts ***!
  \**************************************************************************/
/*! exports provided: ReasonCodesFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReasonCodesFormComponent", function() { return ReasonCodesFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_services */ "./src/app/_services/index.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_models */ "./src/app/_models/index.ts");
/* harmony import */ var _shared_template_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../_shared/template.component */ "./src/app/modules/_shared/template.component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ReasonCodesFormComponent = /** @class */ (function (_super) {
    __extends(ReasonCodesFormComponent, _super);
    function ReasonCodesFormComponent(reasonCodeService, injector) {
        var _this = _super.call(this, injector) || this;
        _this.reasonCodeService = reasonCodeService;
        _this.reasonCode = new _models__WEBPACK_IMPORTED_MODULE_3__["ReasonCode"]();
        return _this;
    }
    ReasonCodesFormComponent.prototype.ngOnInit = function () {
        this.initForm([]);
    };
    ReasonCodesFormComponent.prototype.initForm = function (providers, id) {
        var _this = this;
        if (id === void 0) { id = null; }
        this.id = id;
        this.providers = providers;
        this.form = this.formBuilder.group({
            CODE: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            DESCRIPTION: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            PROVIDER_ID: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            INTERNAL_PROV_REASON_CODE_ID: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
        if (id) {
            this.reasonCodeService.get(id).subscribe(function (res) {
                _this.reasonCode = res.shift();
                delete _this.reasonCode.ID;
                _this.form.patchValue(_this.reasonCode);
            });
            this.save = this.reasonCodeService.update.bind(this.reasonCodeService);
        }
        else {
            this.save = this.reasonCodeService.create.bind(this.reasonCodeService);
        }
        this.form.markAsUntouched();
    };
    ReasonCodesFormComponent.prototype.doSave = function () {
        var _this = this;
        Object.assign(this.reasonCode, this.form.value);
        if (this.validate())
            return;
        this.save(this.id, this.reasonCode)
            .subscribe(function (data) {
            _this.toastr.success('Reason code saved!', 'Success');
            _this.submit.emit();
        }, function (error) {
            console.log(error);
            _this.toastr.error('Attention, make sure your record has been saved.', 'Danger');
        });
    };
    ReasonCodesFormComponent.prototype.title = function () {
        if (!this.id)
            return 'New';
        else
            return 'Edit';
    };
    ReasonCodesFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'reason-code-form',
            template: __webpack_require__(/*! ./reason-codes-form.component.html */ "./src/app/modules/reason-codes/form/reason-codes-form.component.html"),
            styles: [__webpack_require__(/*! ./reason-codes-form.component.css */ "./src/app/modules/reason-codes/form/reason-codes-form.component.css")]
        }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_2__["ReasonCodeService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]])
    ], ReasonCodesFormComponent);
    return ReasonCodesFormComponent;
}(_shared_template_component__WEBPACK_IMPORTED_MODULE_4__["TemplateComponent"]));



/***/ }),

/***/ "./src/app/modules/reason-codes/list/reason-codes-list.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/modules/reason-codes/list/reason-codes-list.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"page_content\">\n    \n    <div id=\"page_content_inner\">\n        <div class=\"md-card\">\n            <div class=\"md-card-toolbar\">\n                <h3 class=\"md-card-toolbar-heading-text\">\n                    <i class=\"material-icons\">format_list_numbered</i> Reason Codes\n                </h3>\n            </div>\n            <div class=\"md-card-content\">\n                <div class=\"dt_colVis_buttons\"></div>\n                <div class=\"uk-overflow-container\">\n                    <table datatable [dtTrigger]=\"dtTrigger\" [dtOptions]=\"dtOptions\" style=\"width:100%\" \n                    class=\"row-border hover uk-table uk-table-hover uk-table-striped dataTable nowrap\">                                            \n                        <thead>\n                            <tr>\n                                <th>Code</th>\n                                <th>Description</th>\n                                <th>Internal Code</th>\n                                <th>Provider</th>                                \n                                <th>Actions</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let reasonCode of reasonCodes\">\n                                <td>{{ reasonCode.CODE }}</td>\n                                <td>{{ reasonCode.DESCRIPTION }}</td>\n                                <td>{{ reasonCode.INTERNAL_PROV_REASON_CODE_ID }}</td>                                \n                                <td>{{ reasonCode.PROVIDER }}</td>                                \n                                <td>\n                                    <a id=\"bt-edit\" (click)=\"edit(form, reasonCode.ID)\" data-uk-modal><i class=\"md-icon material-icons\">mode_edit</i></a>\n                                    <a (click)=\"delete(reasonCode.ID)\"><i class=\"md-icon material-icons\">delete_forever</i></a>\n                                </td>\n                            </tr>\n                        </tbody>\n                    </table>\n                </div>                    \n            </div>\n        </div>\n    </div>\n</div>\n\n<div id=\"reason-code-form\" class=\"uk-modal\">\n    <div class=\"uk-modal-dialog uk-modal-dialog-large\" style=\"top: 30px\">\n      <a href=\"\" class=\"uk-modal-close uk-close\"></a>\n      <reason-code-form #form (submit)=\"closeModal()\"></reason-code-form>\n    </div>\n  </div>\n\n<div class=\"md-fab-wrapper\">     \n    <a class=\"md-fab md-fab-accent\" (click)=\"createNew(form)\" data-uk-modal>\n        <i class=\"material-icons\">&#xE145;</i>\n    </a>    \n</div>"

/***/ }),

/***/ "./src/app/modules/reason-codes/list/reason-codes-list.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/modules/reason-codes/list/reason-codes-list.component.ts ***!
  \**************************************************************************/
/*! exports provided: ReasonCodesListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReasonCodesListComponent", function() { return ReasonCodesListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReasonCodesListComponent = /** @class */ (function () {
    function ReasonCodesListComponent(reasonCodeService, providerService) {
        this.reasonCodeService = reasonCodeService;
        this.providerService = providerService;
        this.dtOptions = {};
        this.reasonCodes = [];
        this.providers = [];
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.modal = false;
    }
    ReasonCodesListComponent.prototype.ngOnInit = function () {
        this.load();
    };
    ReasonCodesListComponent.prototype.load = function () {
        var _this = this;
        var that = this;
        this.reasonCodeService.get()
            .subscribe(function (data) {
            that.reasonCodes = data;
            _this.providerService.get().subscribe(function (providers) {
                _this.providers = providers;
                that.reasonCodes.map(function (reasonCode) {
                    for (var i = 0; i < providers.length; i++) {
                        if (reasonCode.PROVIDER_ID == providers[i].ID)
                            reasonCode.PROVIDER = providers[i].NAME;
                    }
                    return reasonCode;
                });
                console.log(that.reasonCodes);
            });
            _this.rerender();
        }, function (error) {
            console.log(error);
        });
    };
    ReasonCodesListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    ReasonCodesListComponent.prototype.ngOnDestroy = function () {
        this.dtTrigger.unsubscribe();
    };
    ReasonCodesListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    ReasonCodesListComponent.prototype.delete = function (id) {
        var _this = this;
        this.reasonCodeService.delete(id)
            .subscribe(function (data) {
            _this.load();
        }, function (error) {
            console.log(error);
        });
    };
    ReasonCodesListComponent.prototype.createNew = function (form) {
        form.initForm(this.providers);
        this.showModal();
    };
    ReasonCodesListComponent.prototype.edit = function (form, id) {
        form.initForm(this.providers, id);
        this.showModal();
    };
    ReasonCodesListComponent.prototype.showModal = function () {
        UIkit.modal(document.getElementById('reason-code-form')).show();
    };
    ReasonCodesListComponent.prototype.closeModal = function () {
        UIkit.modal(document.getElementById('reason-code-form')).hide();
        this.load();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"]),
        __metadata("design:type", angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"])
    ], ReasonCodesListComponent.prototype, "dtElement", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('modalComponent'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ReasonCodesListComponent.prototype, "modalForm", void 0);
    ReasonCodesListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reason-codes-list',
            template: __webpack_require__(/*! ./reason-codes-list.component.html */ "./src/app/modules/reason-codes/list/reason-codes-list.component.html")
        }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_3__["ReasonCodeService"], _services__WEBPACK_IMPORTED_MODULE_3__["ProviderService"]])
    ], ReasonCodesListComponent);
    return ReasonCodesListComponent;
}());



/***/ }),

/***/ "./src/app/modules/reason-codes/reason-codes-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/modules/reason-codes/reason-codes-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ReasonCodesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReasonCodesRoutingModule", function() { return ReasonCodesRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _list_reason_codes_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./list/reason-codes-list.component */ "./src/app/modules/reason-codes/list/reason-codes-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _list_reason_codes_list_component__WEBPACK_IMPORTED_MODULE_2__["ReasonCodesListComponent"]
    },
];
var ReasonCodesRoutingModule = /** @class */ (function () {
    function ReasonCodesRoutingModule() {
    }
    ReasonCodesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ReasonCodesRoutingModule);
    return ReasonCodesRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/reason-codes/reason-codes.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/modules/reason-codes/reason-codes.module.ts ***!
  \*************************************************************/
/*! exports provided: ReasonCodesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReasonCodesModule", function() { return ReasonCodesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _reason_codes_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./reason-codes-routing.module */ "./src/app/modules/reason-codes/reason-codes-routing.module.ts");
/* harmony import */ var _list_reason_codes_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list/reason-codes-list.component */ "./src/app/modules/reason-codes/list/reason-codes-list.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/modules/_shared/shared.module.ts");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _form_reason_codes_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./form/reason-codes-form.component */ "./src/app/modules/reason-codes/form/reason-codes-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var ReasonCodesModule = /** @class */ (function () {
    function ReasonCodesModule() {
    }
    ReasonCodesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_5__["DataTablesModule"],
                _reason_codes_routing_module__WEBPACK_IMPORTED_MODULE_2__["ReasonCodesRoutingModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]
            ],
            declarations: [_list_reason_codes_list_component__WEBPACK_IMPORTED_MODULE_3__["ReasonCodesListComponent"], _form_reason_codes_form_component__WEBPACK_IMPORTED_MODULE_6__["ReasonCodesFormComponent"]],
            providers: [_services__WEBPACK_IMPORTED_MODULE_8__["ReasonCodeService"], _services__WEBPACK_IMPORTED_MODULE_8__["ProviderService"]]
        })
    ], ReasonCodesModule);
    return ReasonCodesModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-reason-codes-reason-codes-module.js.map