(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-virtual-terminal-virtual-terminal-module"],{

/***/ "./src/app/_components/list/list.component.html":
/*!******************************************************!*\
  !*** ./src/app/_components/list/list.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"uk-overflow-container\">\n  <table datatable [dtTrigger]=\"dtTrigger\" [dtOptions]=\"dtOptions\" style=\"width:100%\" \n  class=\"row-border nowrap datatable\">                                            \n      <thead>\n          <tr>\n              <th *ngFor=\"let label of labels\">\n                {{label | translate}}\n              </th>\n              <th>\n                {{'actions' | translate}}\n              </th>\n          </tr>\n      </thead>\n      <tbody>\n          <tr *ngFor=\"let row of rows\">\n              <td *ngFor=\"let column of columns\">\n                {{ row[column] }}\n              </td>\n              <td>\n                  <a id=\"bt-edit\" (click)=\"edit.emit(row)\" data-uk-modal><i class=\"md-icon material-icons\">mode_edit</i></a>\n                  <a (click)=\"remove.emit(row)\"><i class=\"md-icon material-icons\">delete_forever</i></a>\n              </td>\n          </tr>\n      </tbody>\n  </table>\n</div>"

/***/ }),

/***/ "./src/app/_components/list/list.component.scss":
/*!******************************************************!*\
  !*** ./src/app/_components/list/list.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table.dataTable thead th, table.dataTable thead td {\n  border-bottom: 2px solid rgba(0, 0, 0, 0.12); }\n\ntable.dataTable thead th, table.dataTable thead td {\n  font-size: 14px;\n  text-align: left; }\n\ntable {\n  margin-bottom: 15px; }\n"

/***/ }),

/***/ "./src/app/_components/list/list.component.ts":
/*!****************************************************!*\
  !*** ./src/app/_components/list/list.component.ts ***!
  \****************************************************/
/*! exports provided: ListConfiguration, ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListConfiguration", function() { return ListConfiguration; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ListConfiguration = /** @class */ (function () {
    function ListConfiguration() {
        this.labels = [];
        this.rows = [];
        this.columns = [];
    }
    return ListConfiguration;
}());

var ListComponent = /** @class */ (function () {
    function ListComponent() {
        this.edit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.remove = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this._dtOptions = {
            info: false,
            ordering: false,
            paging: false,
            searching: false
        };
        this.isLoaded = false;
    }
    Object.defineProperty(ListComponent.prototype, "rows", {
        get: function () {
            return this._rows;
        },
        set: function (rows) {
            this._rows = rows;
            if (rows.length > 0 && this.isLoaded)
                this.rerender();
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(ListComponent.prototype, "dtOptions", {
        get: function () {
            return this._dtOptions;
        },
        set: function (options) {
            Object.assign(this._dtOptions, options);
        },
        enumerable: true,
        configurable: true
    });
    ;
    ListComponent.prototype.ngOnInit = function () {
    };
    ListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    ListComponent.prototype.ngAfterViewInit = function () {
        this.isLoaded = true;
        this.dtTrigger.next();
        this.rerender();
    };
    ListComponent.prototype.ngOnDestroy = function () {
        this.dtTrigger.unsubscribe();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], ListComponent.prototype, "rows", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], ListComponent.prototype, "columns", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], ListComponent.prototype, "labels", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ListComponent.prototype, "edit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ListComponent.prototype, "remove", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"]),
        __metadata("design:type", angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"])
    ], ListComponent.prototype, "dtElement", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], ListComponent.prototype, "dtOptions", null);
    ListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'data-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/_components/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.scss */ "./src/app/_components/list/list.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ListComponent);
    return ListComponent;
}());



/***/ }),

/***/ "./src/app/_components/list/list.module.ts":
/*!*************************************************!*\
  !*** ./src/app/_components/list/list.module.ts ***!
  \*************************************************/
/*! exports provided: ListModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListModule", function() { return ListModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./list.component */ "./src/app/_components/list/list.component.ts");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modules/_shared/shared.module */ "./src/app/modules/_shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ListModule = /** @class */ (function () {
    function ListModule() {
    }
    ListModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_3__["DataTablesModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]
            ],
            exports: [_list_component__WEBPACK_IMPORTED_MODULE_2__["ListComponent"]],
            declarations: [_list_component__WEBPACK_IMPORTED_MODULE_2__["ListComponent"]]
        })
    ], ListModule);
    return ListModule;
}());



/***/ }),

/***/ "./src/app/modules/virtual-terminal/form/product-form.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/modules/virtual-terminal/form/product-form.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#page_content_inner{\n    padding-bottom: 24px !important;\n}\n\n.uk-grid{\n    margin-top: 0px !important;\n}\n\n.uk-modal-footer{\n    margin-top: 0px !important;\n    padding: 20px !important;\n}\n\nmat-radio-button{\n    margin-right: 8px;\n}\n\nmat-label{\n    color: rgba(0, 0, 0, 0.54) !important;\n    font-size: 14px !important;\n}\n\n::ng-deep .mat-form-field-underline{    \n    background-color: rgb(225, 225, 225) !important;\n}\n\n::ng-deep \n.cdk-overlay-container {\n    z-index: 99999 !important;\n  }\n\n.uk-tab-grid {\n    margin-bottom: 25px;\n}"

/***/ }),

/***/ "./src/app/modules/virtual-terminal/form/product-form.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/modules/virtual-terminal/form/product-form.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"uk-modal-header\">\n    <h3 class=\"uk-modal-title\"><i class=\"material-icons\">drafts</i>Produtos</h3>\n</div>\n\n<ul class=\"uk-tab uk-tab-grid\" data-uk-switcher=\"{connect:'#form', animation: 'slide-horizontal'}\">\n    <li class=\"uk-width-1-2\" [class.uk-active]=\"activePage == 0\"><a href=\"javascript:void(0)\" (click)=\"activePage = 0\">\n        <i class=\"material-icons\">list</i>Catálogo de produtos</a>\n    </li>\n    <li *ngIf=\"!id\" class=\"uk-width-1-2\" [class.uk-active]=\"activePage == 1\"><a href=\"javascript:void(0)\" (click)=\"activePage = 1\">\n        <i class=\"material-icons\">playlist_add</i>Novo produto</a>\n    </li>\n</ul>\n\n<!-- These are the containers of the content items -->\n<ul id=\"form\" class=\"uk-switcher\" #switcher>\n    <li><ng-container *ngTemplateOutlet=\"listTemplate\"></ng-container></li>\n    <li *ngIf=\"!id\"><ng-container *ngTemplateOutlet=\"listTemplate\"></ng-container></li>\n</ul>\n\n<ng-template #listTemplate>\n    <form class=\"form_validation\" [formGroup]=\"activeForm\" novalidate>\n        <div class=\"uk-grid uk-margin-small2-top\" data-uk-grid-margin>\n            <div class=\"uk-width-medium-1-2 uk-row-first\">\n                <div class=\"parsley-row\">                                                \n                    <mat-form-field class=\"full-width\">\n                        <mat-select #selectValue *ngIf=\"activePage == 0\" [(value)]=\"selectedProduct\" required placeholder=\"{{'product' | translate}}\" (selectionChange)=\"formList.patchValue($event.value)\">                      \n                            <mat-option *ngFor=\"let product of products\" [value]=\"product\">\n                            {{product.PRODUCT}}\n                            </mat-option>\n                        </mat-select>\n                        <input *ngIf=\"activePage == 1\" matInput required placeholder=\"{{'product' | translate}}\" maxlength=\"100\" formControlName=\"PRODUCT\">\n                        <mat-error *ngIf=\"f('PRODUCT').invalid\">{{getErrorMessage(f('PRODUCT'))}}</mat-error>\n                    </mat-form-field>\n                </div>\n            </div>\n\n            <div class=\"uk-width-medium-1-2 uk-row-first\">\n                <div class=\"parsley-row\">\n                    <mat-form-field class=\"full-width\">\n                        <input matInput placeholder=\"{{'productCode' | translate}}\" maxlength=\"100\" formControlName=\"SKU\">\n                        <mat-error *ngIf=\"f('SKU').invalid\">{{getErrorMessage(f('SKU'))}}</mat-error>\n                    </mat-form-field>\n                </div>\n            </div>\n        </div>   \n        \n        <div class=\"uk-grid\" data-uk-grid-margin=\"\">\n            <div class=\"uk-width-medium-1-4 uk-row-first\">\n                <mat-form-field class=\"full-width\">\n                    <input required matInput placeholder=\"{{'quantity' | translate}}\" type=\"number\" min=\"1\" formControlName=\"QUANTITY\">\n                    <mat-error *ngIf=\"f('QUANTITY').invalid\">{{getErrorMessage(f('QUANTITY'))}}</mat-error>\n                </mat-form-field>\n            </div>\n            <div class=\"uk-width-medium-1-4 uk-row-first\">\n                <mat-form-field class=\"full-width\">\n                    <input required matInput placeholder=\"{{'unitPrice' | translate}}\" type=\"number\" min=\"1\" formControlName=\"UNIT_PRICE\">\n                    <mat-error *ngIf=\"f('UNIT_PRICE').invalid\">{{getErrorMessage(f('UNIT_PRICE'))}}</mat-error>\n                </mat-form-field>\n            </div>\n            <div class=\"uk-width-medium-1-4 uk-row-first\">\n                <mat-form-field class=\"full-width\">\n                    <input matInput placeholder=\"{{'taxValue' | translate}}\" type=\"number\" min=\"1\" formControlName=\"SALES_TAX\">\n                    <mat-error *ngIf=\"f('SALES_TAX').invalid\">{{getErrorMessage(f('SALES_TAX'))}}</mat-error>\n                </mat-form-field>\n            </div>\t\n            <div class=\"uk-width-medium-1-4 uk-row-first\">\n                <mat-form-field class=\"full-width\">\n                    <input matInput placeholder=\"{{'totalValue' | translate}}\" type=\"number\" min=\"1\" readonly formControlName=\"TOTAL_VALUE\" value=\"{{getTotal()}}\">\n                    <mat-error *ngIf=\"f('TOTAL_VALUE').invalid\">{{getErrorMessage(f('TOTAL_VALUE'))}}</mat-error>\n                </mat-form-field>\n            </div>\t\t\t\t\t\t\t\n        </div> \n    </form> \n\n    <div class=\"uk-modal-footer uk-grid uk-margin-small2-top\">\n        <div class=\"uk-width-medium-1-2 uk-row-first uk-text-left\" [style.padding.px]=\"0\">\n            <mat-error *ngIf=\"productAdded && !id && activePage == 0\">\n                <i class=\"material-icons uk-text-danger\">error</i>\n                This product is already added.\n            </mat-error>\n            <mat-error *ngIf=\"productAdded && id\" class=\"uk-text-success\">\n                <i class=\"material-icons uk-text-success\">error</i>\n                Product updated successfully.\n            </mat-error>\n            <mat-error *ngIf=\"productAdded && !id && activePage == 1\" class=\"uk-text-danger\">\n                <i class=\"material-icons uk-text-danger\">error</i>\n                Product already exists.\n            </mat-error>\n        </div>\n        <div class=\"uk-width-medium-1-2 uk-row-first uk-text-right\">\n            <button type=\"button\" class=\"md-btn md-btn-secondary md-btn-wave-light waves-effect waves-button waves-light\" (click)=\"productAdded = false; submit.emit()\">{{'close' | translate}}</button>\n            <button type=\"button\" class=\"md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light\" (click)=\"addToList()\">{{'modify' | translate}}</button>\n        </div>\n\n    </div>\n</ng-template> \n    \n"

/***/ }),

/***/ "./src/app/modules/virtual-terminal/form/product-form.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/modules/virtual-terminal/form/product-form.component.ts ***!
  \*************************************************************************/
/*! exports provided: ProductFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductFormComponent", function() { return ProductFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_models */ "./src/app/_models/index.ts");
/* harmony import */ var _shared_template_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../_shared/template.component */ "./src/app/modules/_shared/template.component.ts");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../_services/product.service */ "./src/app/_services/product.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProductFormComponent = /** @class */ (function (_super) {
    __extends(ProductFormComponent, _super);
    function ProductFormComponent(productService, injector) {
        var _this = _super.call(this, injector) || this;
        _this.productService = productService;
        _this.product = new _models__WEBPACK_IMPORTED_MODULE_2__["Product"]();
        _this.selectedProduct = new _models__WEBPACK_IMPORTED_MODULE_2__["Product"]();
        _this.productAdded = false;
        _this.activePage = 0;
        _this.add = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        _this.productList = [];
        return _this;
    }
    ProductFormComponent.prototype.ngOnInit = function () {
        this.initForm([]);
    };
    ProductFormComponent.prototype.initForm = function (products, id) {
        var _this = this;
        if (id === void 0) { id = null; }
        this.id = id;
        this.products = products;
        this.activePage = 0;
        this.selectedProduct = new _models__WEBPACK_IMPORTED_MODULE_2__["Product"]();
        if (this.selectValue && !id) {
            this.selectValue.value = null;
            this.selectValue.setDisabledState(false);
        }
        this.form = this.formBuilder.group({
            PRODUCT: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            SKU: [''],
            SALES_TAX: [''],
            UNIT_PRICE: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            QUANTITY: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            TOTAL_VALUE: ['']
        });
        this.formList = this.formBuilder.group({
            ID: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            PRODUCT: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            SKU: [''],
            SALES_TAX: [''],
            UNIT_PRICE: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            QUANTITY: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            TOTAL_VALUE: ['']
        });
        if (id) {
            this.selectedProduct = this.productList.filter(function (product) { return product.ID == id; }).shift();
            this.formList.patchValue(this.selectedProduct);
            this.selectedProduct = this.products.filter(function (product) { return product.ID == id; })[0];
            setTimeout(function () {
                _this.selectValue.setDisabledState(true);
            }, 500);
        }
        this.form.markAsUntouched();
        UIkit.switcher(this.switcher.nativeElement, { connect: '#form', animation: 'slide-horizontal', active: 0 });
        UIkit.switcher(this.switcher.nativeElement).show(0);
    };
    ProductFormComponent.prototype.addToList = function () {
        var _this = this;
        var product = new _models__WEBPACK_IMPORTED_MODULE_2__["Product"]();
        this.activeForm.controls['TOTAL_VALUE'].setValue(this.getTotal());
        Object.assign(this.selectedProduct, this.activeForm.value);
        Object.assign(product, this.selectedProduct);
        if (this.validate(this.activeForm))
            return;
        if (this.activePage == 1) {
            delete product.QUANTITY;
            delete product.TOTAL_VALUE;
            delete product.ID;
            this.productService.getByName(product.PRODUCT).subscribe(function (res) {
                if (res.length > 0)
                    return _this.productAdded = true;
                _this.productService.create(null, product).subscribe(function (res) {
                    Object.assign(product, res[0]);
                    Object.assign(product, _this.selectedProduct);
                    _this.productAdded = false;
                    _this.add.emit(product);
                    _this.submit.emit();
                });
            });
        }
        else {
            if (this.productList.map(function (row) { return row.ID; }).includes(product.ID)) {
                if (this.id) {
                    Object.assign(this.productList[this.productList.indexOf(this.productList.filter(function (product) { return product.ID == _this.id; })[0])], this.selectedProduct);
                }
                return this.productAdded = true;
            }
            this.productAdded = false;
            this.add.emit(product);
            this.submit.emit();
        }
    };
    ProductFormComponent.prototype.getTotal = function () {
        return (parseInt(this.activeForm.controls['UNIT_PRICE'].value || 0) + parseInt(this.activeForm.controls['SALES_TAX'].value || 0)) * this.activeForm.controls['QUANTITY'].value;
    };
    Object.defineProperty(ProductFormComponent.prototype, "activeForm", {
        get: function () {
            if (this.activePage === 0)
                return this.formList;
            else
                return this.form;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ProductFormComponent.prototype, "add", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], ProductFormComponent.prototype, "productList", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelect"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelect"])
    ], ProductFormComponent.prototype, "selectValue", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('switcher'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProductFormComponent.prototype, "switcher", void 0);
    ProductFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'product-form',
            template: __webpack_require__(/*! ./product-form.component.html */ "./src/app/modules/virtual-terminal/form/product-form.component.html"),
            styles: [__webpack_require__(/*! ./product-form.component.css */ "./src/app/modules/virtual-terminal/form/product-form.component.css")]
        }),
        __metadata("design:paramtypes", [_services_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]])
    ], ProductFormComponent);
    return ProductFormComponent;
}(_shared_template_component__WEBPACK_IMPORTED_MODULE_3__["TemplateComponent"]));



/***/ }),

/***/ "./src/app/modules/virtual-terminal/virtual-terminal-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/modules/virtual-terminal/virtual-terminal-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: VirtualTerminalRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VirtualTerminalRoutingModule", function() { return VirtualTerminalRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _virtual_terminal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./virtual-terminal.component */ "./src/app/modules/virtual-terminal/virtual-terminal.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _virtual_terminal_component__WEBPACK_IMPORTED_MODULE_2__["VirtualTerminalComponent"]
    }
];
var VirtualTerminalRoutingModule = /** @class */ (function () {
    function VirtualTerminalRoutingModule() {
    }
    VirtualTerminalRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], VirtualTerminalRoutingModule);
    return VirtualTerminalRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/virtual-terminal/virtual-terminal.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/modules/virtual-terminal/virtual-terminal.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"page_content\">\n    <div id=\"page_content_inner\">\n            <h3 class=\"heading_b uk-margin-bottom\">{{'terminalVirtual' | translate}}</h3>\n        <div class=\"md-card\">\n            <div class=\"md-card-toolbar\">\n                <h3 class=\"md-card-toolbar-heading-text\">\n                    <i class=\"material-icons\">reorder</i> {{'orderDetails' | translate}}\n                </h3>\n            </div>\n            <div class=\"md-card-content\">\n                <form class=\"form_validation\" [formGroup]=\"form\" novalidate>\n                    <div class=\"uk-grid uk-margin-small2-top\" data-uk-grid-margin>\n                        <div class=\"uk-width-medium-1-2 uk-row-first\">\n                            <div class=\"parsley-row\">                                                \n                                <mat-form-field class=\"full-width\">\n                                    <mat-select placeholder=\"{{'transactionOrigin' | translate}}\" formControlName=\"TXN_ORIGIN\">                      \n                                        <mat-option *ngFor=\"let transactionOrigin of transactionOrigins\" [value]=\"transactionOrigin.ID\">\n                                        {{transactionOrigin.TXN_ORIGIN}}\n                                        </mat-option>\n                                    </mat-select>\n                                </mat-form-field>\n                            </div>\n                        </div>\n                \n                        <div class=\"uk-width-medium-1-2 uk-row-first\">\n                            <div class=\"parsley-row\">\n                                <mat-form-field class=\"full-width\">\n                                    <input required matInput placeholder=\"{{'refererCode' | translate}}\" maxlength=\"100\" formControlName=\"REF_CODE\">\n                                    <mat-error *ngIf=\"f('REF_CODE').invalid\">{{getErrorMessage(f('REF_CODE'))}}</mat-error>\n                                </mat-form-field>\n                            </div>\n                        </div>\n                    </div>   \n                </form>\n            </div>\n        </div>\n\n        <div class=\"md-card\">\n            <div class=\"md-card-toolbar\">\n                <h3 class=\"md-card-toolbar-heading-text\">\n                    <i class=\"material-icons\">reorder</i> {{'items' | translate}}\n                </h3>\n            </div>\n            <div class=\"md-card-content\">\n                <div class=\"uk-grid uk-margin-small2-top\" data-uk-grid-margin>\n                    <div class=\"uk-width-medium-1-1 uk-row-first\">\n                        <div class=\"parsley-row\">                                                \n                            <data-list \n                                [rows]=\"productList.rows\" \n                                [columns]=\"productList.columns\" \n                                [labels]=\"productList.labels\"\n                                (remove)=\"remove($event)\"\n                                (edit)=\"edit(modalForm, $event)\">\n                            </data-list>\n                        </div>\n                        <div class=\"parsley-row\">                                                \n                            <button type=\"button\" class=\"md-btn md-btn-primary md-btn-wave-light\" (click)=\"createNew(modalForm)\" data-uk-modal>\n                                <i class=\"material-icons\">playlist_add</i> {{'addProducts' | translate}}\n                            </button>\n                        </div>\n                        <div class=\"parsley-row uk-margin-top\">\n                            <mat-form-field class=\"full-width\">\n                                <input matInput placeholder=\"{{'totalValue' | translate}}\" type=\"number\" readonly value=\"{{getTotal()}}\">\n                            </mat-form-field>\n                        </div>\n                    </div>\n                </div>   \n            </div>\n        </div>        \n        \n        <div class=\"md-card\">\n            <div class=\"md-card-content\">\n                <div class=\"uk-width-medium-1-10\">\n                    <button type=\"button\" class=\"md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light\" (click)=\"doSave()\">{{'save' | translate}}</button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div id=\"product-form\" class=\"uk-modal\">\n    <div class=\"uk-modal-dialog uk-modal-dialog\" style=\"top: 30px\">\n      <a href=\"\" class=\"uk-modal-close uk-close\"></a>\n      <product-form #modalForm (submit)=\"closeModal()\" (add)=\"addToList($event)\" [productList]=\"productList.rows\"></product-form>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/modules/virtual-terminal/virtual-terminal.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/modules/virtual-terminal/virtual-terminal.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-radio-button {\n  margin-right: 8px; }\n\nmat-label {\n  color: rgba(0, 0, 0, 0.54) !important;\n  font-size: 14px !important; }\n\n::ng-deep .mat-form-field-underline {\n  background-color: #e1e1e1 !important; }\n"

/***/ }),

/***/ "./src/app/modules/virtual-terminal/virtual-terminal.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/modules/virtual-terminal/virtual-terminal.component.ts ***!
  \************************************************************************/
/*! exports provided: VirtualTerminalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VirtualTerminalComponent", function() { return VirtualTerminalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_template_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../_shared/template.component */ "./src/app/modules/_shared/template.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/_services */ "./src/app/_services/index.ts");
/* harmony import */ var src_app_components_list_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/_components/list/list.component */ "./src/app/_components/list/list.component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var VirtualTerminalComponent = /** @class */ (function (_super) {
    __extends(VirtualTerminalComponent, _super);
    function VirtualTerminalComponent(injector, productService, transactionOriginService) {
        var _this = _super.call(this, injector) || this;
        _this.productService = productService;
        _this.transactionOriginService = transactionOriginService;
        _this.products = [];
        _this.productList = new src_app_components_list_list_component__WEBPACK_IMPORTED_MODULE_4__["ListConfiguration"]();
        _this.transactionOrigins = [];
        _this.productList.labels = ['product', 'code', 'quantity', 'unitPrice', 'tax', 'total'];
        _this.productList.columns = ['PRODUCT', 'SKU', 'QUANTITY', 'UNIT_PRICE', 'SALES_TAX', 'TOTAL_VALUE'];
        return _this;
    }
    VirtualTerminalComponent.prototype.ngOnInit = function () {
        this.initForm();
        this.load();
    };
    VirtualTerminalComponent.prototype.initForm = function () {
        this.form = this.formBuilder.group({
            TXN_ORIGIN: [],
            REF_CODE: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    VirtualTerminalComponent.prototype.load = function () {
        var _this = this;
        this.transactionOriginService.get().subscribe(function (res) { return _this.transactionOrigins = res; });
        this.productService.get().subscribe(function (res) {
            _this.products = res;
        });
    };
    VirtualTerminalComponent.prototype.createNew = function (form) {
        form.initForm(this.products);
        this.showModal();
    };
    VirtualTerminalComponent.prototype.showModal = function () {
        UIkit.modal(document.getElementById('product-form')).show();
    };
    VirtualTerminalComponent.prototype.closeModal = function () {
        UIkit.modal(document.getElementById('product-form')).hide();
    };
    VirtualTerminalComponent.prototype.addToList = function (product) {
        this.toastr.success('Product added.', 'Success');
        this.productList.rows.push(product);
        this.load();
    };
    VirtualTerminalComponent.prototype.getTotal = function () {
        if (this.productList.rows.length > 0) {
            var total = this.productList.rows.reduce(function (a, b) { return a + b.TOTAL_VALUE; }, 0);
            return total;
        }
    };
    VirtualTerminalComponent.prototype.remove = function (product) {
        this.productList.rows.splice(this.productList.rows.indexOf(product), 1);
    };
    VirtualTerminalComponent.prototype.edit = function (form, product) {
        form.initForm(this.products, product.ID);
        this.showModal();
    };
    VirtualTerminalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'virtual-terminal',
            template: __webpack_require__(/*! ./virtual-terminal.component.html */ "./src/app/modules/virtual-terminal/virtual-terminal.component.html"),
            styles: [__webpack_require__(/*! ./virtual-terminal.component.scss */ "./src/app/modules/virtual-terminal/virtual-terminal.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], src_app_services__WEBPACK_IMPORTED_MODULE_3__["ProductService"], src_app_services__WEBPACK_IMPORTED_MODULE_3__["TransactionOriginService"]])
    ], VirtualTerminalComponent);
    return VirtualTerminalComponent;
}(_shared_template_component__WEBPACK_IMPORTED_MODULE_1__["TemplateComponent"]));



/***/ }),

/***/ "./src/app/modules/virtual-terminal/virtual-terminal.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/modules/virtual-terminal/virtual-terminal.module.ts ***!
  \*********************************************************************/
/*! exports provided: VirtualTerminalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VirtualTerminalModule", function() { return VirtualTerminalModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _virtual_terminal_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./virtual-terminal-routing.module */ "./src/app/modules/virtual-terminal/virtual-terminal-routing.module.ts");
/* harmony import */ var _virtual_terminal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./virtual-terminal.component */ "./src/app/modules/virtual-terminal/virtual-terminal.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/modules/_shared/shared.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/_services */ "./src/app/_services/index.ts");
/* harmony import */ var _components_list_list_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../_components/list/list.module */ "./src/app/_components/list/list.module.ts");
/* harmony import */ var _form_product_form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./form/product-form.component */ "./src/app/modules/virtual-terminal/form/product-form.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var VirtualTerminalModule = /** @class */ (function () {
    function VirtualTerminalModule() {
    }
    VirtualTerminalModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _virtual_terminal_routing_module__WEBPACK_IMPORTED_MODULE_2__["VirtualTerminalRoutingModule"],
                _components_list_list_module__WEBPACK_IMPORTED_MODULE_7__["ListModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]
            ],
            declarations: [_virtual_terminal_component__WEBPACK_IMPORTED_MODULE_3__["VirtualTerminalComponent"], _form_product_form_component__WEBPACK_IMPORTED_MODULE_8__["ProductFormComponent"]],
            providers: [src_app_services__WEBPACK_IMPORTED_MODULE_6__["ProductService"], src_app_services__WEBPACK_IMPORTED_MODULE_6__["TransactionOriginService"]]
        })
    ], VirtualTerminalModule);
    return VirtualTerminalModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-virtual-terminal-virtual-terminal-module.js.map