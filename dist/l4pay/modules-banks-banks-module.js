(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-banks-banks-module"],{

/***/ "./src/app/modules/banks/banks-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/modules/banks/banks-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: BanksRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BanksRoutingModule", function() { return BanksRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _list_banks_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./list/banks-list.component */ "./src/app/modules/banks/list/banks-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _list_banks_list_component__WEBPACK_IMPORTED_MODULE_2__["BankListComponent"]
    },
];
var BanksRoutingModule = /** @class */ (function () {
    function BanksRoutingModule() {
    }
    BanksRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], BanksRoutingModule);
    return BanksRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/banks/banks.module.ts":
/*!***********************************************!*\
  !*** ./src/app/modules/banks/banks.module.ts ***!
  \***********************************************/
/*! exports provided: BankModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankModule", function() { return BankModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _banks_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./banks-routing.module */ "./src/app/modules/banks/banks-routing.module.ts");
/* harmony import */ var _list_banks_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list/banks-list.component */ "./src/app/modules/banks/list/banks-list.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/modules/_shared/shared.module.ts");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _form_banks_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./form/banks-form.component */ "./src/app/modules/banks/form/banks-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var BankModule = /** @class */ (function () {
    function BankModule() {
    }
    BankModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_5__["DataTablesModule"],
                _banks_routing_module__WEBPACK_IMPORTED_MODULE_2__["BanksRoutingModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]
            ],
            declarations: [_list_banks_list_component__WEBPACK_IMPORTED_MODULE_3__["BankListComponent"], _form_banks_form_component__WEBPACK_IMPORTED_MODULE_6__["BankFormComponent"]],
            providers: [_services__WEBPACK_IMPORTED_MODULE_8__["BankService"], _services__WEBPACK_IMPORTED_MODULE_8__["ProviderService"]]
        })
    ], BankModule);
    return BankModule;
}());



/***/ }),

/***/ "./src/app/modules/banks/form/banks-form.component.css":
/*!*************************************************************!*\
  !*** ./src/app/modules/banks/form/banks-form.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#page_content_inner{\n    padding-bottom: 24px !important;\n}\n\n.uk-grid{\n    margin-top: 0px !important;\n}\n\n.uk-modal-footer{\n    margin-top: 0px !important;\n}\n\nmat-radio-button{\n    margin-right: 8px;\n}\n\nmat-label{\n    color: rgba(0, 0, 0, 0.54) !important;\n    font-size: 14px !important;\n}\n\n::ng-deep .mat-form-field-underline{    \n    background-color: rgb(225, 225, 225) !important;\n}\n\n::ng-deep \n.cdk-overlay-container {\n    z-index: 99999 !important;\n  }"

/***/ }),

/***/ "./src/app/modules/banks/form/banks-form.component.html":
/*!**************************************************************!*\
  !*** ./src/app/modules/banks/form/banks-form.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"uk-modal-header\">\n    <h3 class=\"uk-modal-title\"><i class=\"material-icons\">playlist_add</i> {{title() | translate}}</h3>\n</div>\n<form class=\"form_validation\" [formGroup]=\"form\" novalidate>\n    <div class=\"uk-grid uk-margin-small2-top\" data-uk-grid-margin>\n        <div class=\"uk-width-medium-1-2 uk-row-first\">\n            <div class=\"parsley-row\">                \n                <mat-form-field class=\"full-width\">\n                    <mat-select placeholder=\"{{'provider' | translate}}\" formControlName=\"PROVIDER_ID\" required>                      \n                      <mat-option *ngFor=\"let provider of providers\" [value]=\"provider.ID\">\n                        {{provider.NAME}}\n                      </mat-option>\n                    </mat-select>\n                    <mat-error *ngIf=\"f('PROVIDER_ID').invalid\">{{getErrorMessage(f('PROVIDER_ID')) | translate}}</mat-error>                                        \n                </mat-form-field>\n            </div>\n        </div>\n              \n        <div class=\"uk-width-medium-1-2 uk-row-first\">\n            <div class=\"parsley-row\">\n                <mat-label>{{'status' | translate}}</mat-label>\n                <div class=\"uk-margin-small2-top\">\n                    <mat-radio-group formControlName=\"ACTIVE\">\n                        <mat-radio-button value=\"Y\">{{'active' | translate}}</mat-radio-button>\n                        <mat-radio-button value=\"N\">{{'disabled' | translate}}</mat-radio-button>\n                    </mat-radio-group>\n                </div>\n            </div>\n        </div>\n    </div>        \n\n    <div class=\"uk-grid uk-margin-small3-top\" data-uk-grid-margin>\n        <div class=\"uk-width-medium-1-3 uk-row-first\">\n            <div class=\"parsley-row\">\n                <mat-form-field class=\"full-width\">\n                    <input required matInput placeholder=\"{{'bank' | translate}}\" maxlength=\"100\" formControlName=\"BANK\">\n                    <mat-error *ngIf=\"f('BANK').invalid\">{{getErrorMessage(f('BANK')) | translate}}</mat-error>\n                </mat-form-field>\n            </div>\n        </div>\n\n        <div class=\"uk-width-medium-1-3 uk-row-first\">\n            <div class=\"parsley-row\">\n                <mat-form-field class=\"full-width\">\n                    <input required matInput placeholder=\"{{'bankType' | translate}}\" maxlength=\"3\" formControlName=\"TYPE\">\n                    <mat-error *ngIf=\"f('TYPE').invalid\">{{getErrorMessage(f('TYPE')) | translate}}</mat-error>\n                </mat-form-field>                \n            </div>\n        </div>        \n\n        <div class=\"uk-width-medium-1-3 uk-row-first\">\n            <div class=\"parsley-row\">\n                <mat-form-field class=\"full-width\">\n                    <input required matInput placeholder=\"{{'providerBank' | translate}}\" maxlength=\"255\" formControlName=\"PROVIDER_CODE\">\n                    <mat-error *ngIf=\"f('PROVIDER_CODE').invalid\">{{getErrorMessage(f('PROVIDER_CODE')) | translate}}</mat-error>\n                </mat-form-field>                \n            </div>\n        </div>        \n    </div>    \n       \n    <div class=\"uk-modal-footer uk-text-right\">\n        <div class=\"uk-width-medium-1-1 uk-row-first\">\n            <button type=\"button\" class=\"md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light\" (click)=\"doSave()\">{{'save' | translate}}</button>\n            <button type=\"button\" class=\"md-btn md-btn-secondary md-btn-wave-light waves-effect waves-button waves-light\" (click)=\"submit.emit()\">{{'back' | translate}}</button>\n        </div>\n    </div>\n</form>"

/***/ }),

/***/ "./src/app/modules/banks/form/banks-form.component.ts":
/*!************************************************************!*\
  !*** ./src/app/modules/banks/form/banks-form.component.ts ***!
  \************************************************************/
/*! exports provided: BankFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankFormComponent", function() { return BankFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_models */ "./src/app/_models/index.ts");
/* harmony import */ var _shared_template_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../_shared/template.component */ "./src/app/modules/_shared/template.component.ts");
/* harmony import */ var _services_bank_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../_services/bank.service */ "./src/app/_services/bank.service.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BankFormComponent = /** @class */ (function (_super) {
    __extends(BankFormComponent, _super);
    function BankFormComponent(bankService, injector) {
        var _this = _super.call(this, injector) || this;
        _this.bankService = bankService;
        _this.bank = new _models__WEBPACK_IMPORTED_MODULE_2__["Bank"]();
        return _this;
    }
    BankFormComponent.prototype.ngOnInit = function () {
        this.initForm([]);
    };
    BankFormComponent.prototype.initForm = function (providers, id) {
        var _this = this;
        if (id === void 0) { id = null; }
        this.id = id;
        this.providers = providers;
        this.form = this.formBuilder.group({
            PROVIDER_ID: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            BANK: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            ACTIVE: ['Y', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            TYPE: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            PROVIDER_CODE: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
        if (id) {
            this.bankService.get(id).subscribe(function (res) {
                _this.bank = res.shift();
                delete _this.bank.ID;
                _this.form.patchValue(_this.bank);
            });
            this.save = this.bankService.update.bind(this.bankService);
        }
        else {
            this.save = this.bankService.create.bind(this.bankService);
        }
        this.form.markAsUntouched();
    };
    BankFormComponent.prototype.doSave = function () {
        var _this = this;
        Object.assign(this.bank, this.form.value);
        if (this.validate())
            return;
        this.save(this.id, this.bank)
            .subscribe(function (data) {
            _this.toastr.success(_this.translate('registerSaved'), _this.translate('success'));
            _this.submit.emit();
        }, function (error) {
            _this.toastr.error(_this.translate('makeSureAllRecordSaved'), _this.translate('danger'));
        });
    };
    BankFormComponent.prototype.title = function () {
        if (!this.id)
            return 'new';
        else
            return 'edit';
    };
    BankFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'bank-form',
            template: __webpack_require__(/*! ./banks-form.component.html */ "./src/app/modules/banks/form/banks-form.component.html"),
            styles: [__webpack_require__(/*! ./banks-form.component.css */ "./src/app/modules/banks/form/banks-form.component.css")]
        }),
        __metadata("design:paramtypes", [_services_bank_service__WEBPACK_IMPORTED_MODULE_4__["BankService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]])
    ], BankFormComponent);
    return BankFormComponent;
}(_shared_template_component__WEBPACK_IMPORTED_MODULE_3__["TemplateComponent"]));



/***/ }),

/***/ "./src/app/modules/banks/list/banks-list.component.html":
/*!**************************************************************!*\
  !*** ./src/app/modules/banks/list/banks-list.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"page_content\">\n\n    <div id=\"page_content_inner\">\n        <div class=\"md-card\">\n            <div class=\"md-card-toolbar\">\n                <h3 class=\"md-card-toolbar-heading-text\">\n                    <i class=\"material-icons\">account_balance</i> {{'banks' | translate}}\n                </h3>\n            </div>\n            <div class=\"md-card-content\">\n                <div class=\"dt_colVis_buttons\"></div>\n                <div class=\"uk-overflow-container\">\n                    <table datatable [dtTrigger]=\"dtTrigger\" [dtOptions]=\"dtOptions\" style=\"width:100%\" class=\"row-border hover uk-table uk-table-hover uk-table-striped dataTable nowrap\">\n                        <thead>\n                            <tr>\n                                <th>{{'bank' | translate}}</th>\n                                <th>{{'provider' | translate}}</th>\n                                <th>{{'providerBank' | translate}}</th>\n                                <th>{{'bankType' | translate}}</th>\n                                <th>{{'actions' | translate}}</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let bank of banks\">\n                                <td>{{ bank.BANK }}</td>\n                                <td>{{ bank.PROVIDER }}</td>\n                                <td>{{ bank.PROVIDER_CODE }}</td>\n                                <td>{{ bank.TYPE }}</td>                                                                \n                                <td>\n                                    <a id=\"bt-edit\" (click)=\"edit(form, bank.ID)\" data-uk-modal><i class=\"md-icon material-icons\">mode_edit</i></a>\n                                    <a (click)=\"delete(bank.ID)\"><i class=\"md-icon material-icons\">delete_forever</i></a>\n                                </td>\n                            </tr>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div id=\"bank-form\" class=\"uk-modal\">\n    <div class=\"uk-modal-dialog uk-modal-dialog-large\" style=\"top: 30px\">\n        <a href=\"\" class=\"uk-modal-close uk-close\"></a>\n        <bank-form #form (submit)=\"closeModal()\"></bank-form>\n    </div>\n</div>\n\n<div class=\"md-fab-wrapper\">\n    <a class=\"md-fab md-fab-accent\" (click)=\"createNew(form)\" data-uk-modal>\n        <i class=\"material-icons\">&#xE145;</i>\n    </a>\n</div>"

/***/ }),

/***/ "./src/app/modules/banks/list/banks-list.component.ts":
/*!************************************************************!*\
  !*** ./src/app/modules/banks/list/banks-list.component.ts ***!
  \************************************************************/
/*! exports provided: BankListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankListComponent", function() { return BankListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BankListComponent = /** @class */ (function () {
    function BankListComponent(bankService, providerService) {
        this.bankService = bankService;
        this.providerService = providerService;
        this.dtOptions = {};
        this.banks = [];
        this.providers = [];
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.modal = false;
    }
    BankListComponent.prototype.ngOnInit = function () {
        this.load();
    };
    BankListComponent.prototype.load = function () {
        var _this = this;
        var that = this;
        this.bankService.get()
            .subscribe(function (data) {
            that.banks = data;
            _this.providerService.get().subscribe(function (providers) {
                _this.providers = providers;
                that.banks.map(function (bank) {
                    for (var i = 0; i < providers.length; i++) {
                        if (bank.PROVIDER_ID == providers[i].ID)
                            bank.PROVIDER = providers[i].NAME;
                    }
                    return bank;
                });
            });
            _this.rerender();
        }, function (error) {
            console.log(error);
        });
    };
    BankListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    BankListComponent.prototype.ngOnDestroy = function () {
        this.dtTrigger.unsubscribe();
    };
    BankListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    BankListComponent.prototype.delete = function (id) {
        var _this = this;
        this.bankService.delete(id)
            .subscribe(function (data) {
            _this.load();
        }, function (error) {
            console.log(error);
        });
    };
    BankListComponent.prototype.createNew = function (form) {
        form.initForm(this.providers);
        this.showModal();
    };
    BankListComponent.prototype.edit = function (form, id) {
        form.initForm(this.providers, id);
        this.showModal();
    };
    BankListComponent.prototype.showModal = function () {
        UIkit.modal(document.getElementById('bank-form')).show();
    };
    BankListComponent.prototype.closeModal = function () {
        UIkit.modal(document.getElementById('bank-form')).hide();
        this.load();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"]),
        __metadata("design:type", angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"])
    ], BankListComponent.prototype, "dtElement", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('modalComponent'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], BankListComponent.prototype, "modalForm", void 0);
    BankListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bank-list',
            template: __webpack_require__(/*! ./banks-list.component.html */ "./src/app/modules/banks/list/banks-list.component.html")
        }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_3__["BankService"], _services__WEBPACK_IMPORTED_MODULE_3__["ProviderService"]])
    ], BankListComponent);
    return BankListComponent;
}());



/***/ })

}]);
//# sourceMappingURL=modules-banks-banks-module.js.map