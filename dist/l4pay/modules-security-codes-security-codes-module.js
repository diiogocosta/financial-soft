(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-security-codes-security-codes-module"],{

/***/ "./src/app/modules/security-codes/form/security-codes-form.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/modules/security-codes/form/security-codes-form.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#page_content_inner{\n    padding-bottom: 24px !important;\n}\n\n.uk-grid{\n    margin-top: 0px !important;\n}\n\n.uk-modal-footer{\n    margin-top: 0px !important;\n}\n\nmat-radio-button{\n    margin-right: 8px;\n}\n\nmat-label{\n    color: rgba(0, 0, 0, 0.54) !important;\n    font-size: 14px !important;\n}\n\n::ng-deep .mat-form-field-underline{    \n    background-color: rgb(225, 225, 225) !important;\n}\n\n::ng-deep \n.cdk-overlay-container {\n    z-index: 99999 !important;\n  }"

/***/ }),

/***/ "./src/app/modules/security-codes/form/security-codes-form.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/modules/security-codes/form/security-codes-form.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"uk-modal-header\">\n    <h3 class=\"uk-modal-title\"><i class=\"material-icons\">playlist_add</i> {{title()}}</h3>\n</div>\n<form class=\"form_validation\" [formGroup]=\"form\" novalidate>\n    <div class=\"uk-grid uk-margin-small2-top\" data-uk-grid-margin>\n        <div class=\"uk-width-medium-1-2 uk-row-first\">\n            <div class=\"parsley-row\">\n\n                <mat-form-field class=\"full-width\">\n                    <mat-select placeholder=\"Type\" formControlName=\"CODE_TYPE\" required>                      \n                      <mat-option *ngFor=\"let type of ['AVS','CV']\" [value]=\"type\">\n                        {{type}}\n                      </mat-option>\n                    </mat-select>\n                    <mat-error *ngIf=\"f('CODE_TYPE').invalid\">{{getErrorMessage(f('CODE_TYPE'))}}</mat-error>                                        \n                </mat-form-field>                \n            </div>\n        </div>\n              \n        <div class=\"uk-width-medium-1-2 uk-row-first\">\n            <div class=\"parsley-row\">\n                <mat-form-field class=\"full-width\">\n                    <input required matInput placeholder=\"Code\" maxlength=\"10\" formControlName=\"CODE\">\n                    <mat-error *ngIf=\"f('CODE').invalid\">{{getErrorMessage(f('CODE'))}}</mat-error>\n                </mat-form-field>\n            </div>\n        </div>\n    </div>        \n\n    <div class=\"uk-grid uk-margin-small2-top\" data-uk-grid-margin>\n        <div class=\"uk-width-medium-1-2 uk-row-first\">\n            <div class=\"parsley-row\">                                                \n                <mat-form-field class=\"full-width\">\n                    <mat-select placeholder=\"Provider\" formControlName=\"PROVIDER_ID\" required>                      \n                      <mat-option *ngFor=\"let provider of providers\" [value]=\"provider.ID\">\n                        {{provider.NAME}}\n                      </mat-option>\n                    </mat-select>\n                    <mat-error *ngIf=\"f('PROVIDER_ID').invalid\">{{getErrorMessage(f('PROVIDER_ID'))}}</mat-error>                                        \n                </mat-form-field>\n            </div>\n        </div>\n\n        <div class=\"uk-width-medium-1-2 uk-row-first\">\n            <div class=\"parsley-row\">\n                <mat-form-field class=\"full-width\">\n                    <input required matInput placeholder=\"Description\" maxlength=\"100\" formControlName=\"DESCRIPTION\">\n                    <mat-error *ngIf=\"f('DESCRIPTION').invalid\">{{getErrorMessage(f('DESCRIPTION'))}}</mat-error>\n                </mat-form-field>\n            </div>\n        </div>\n    </div>\n       \n    <div class=\"uk-modal-footer uk-text-right\">\n        <div class=\"uk-width-medium-1-1 uk-row-first\">\n            <button type=\"button\" class=\"md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light\" (click)=\"doSave()\">Save</button>\n            <button type=\"button\" class=\"md-btn md-btn-secondary md-btn-wave-light waves-effect waves-button waves-light\" (click)=\"submit.emit()\">Back</button>\n        </div>\n    </div>\n</form>"

/***/ }),

/***/ "./src/app/modules/security-codes/form/security-codes-form.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/modules/security-codes/form/security-codes-form.component.ts ***!
  \******************************************************************************/
/*! exports provided: SecurityCodesFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityCodesFormComponent", function() { return SecurityCodesFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_services */ "./src/app/_services/index.ts");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_models */ "./src/app/_models/index.ts");
/* harmony import */ var _shared_template_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../_shared/template.component */ "./src/app/modules/_shared/template.component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SecurityCodesFormComponent = /** @class */ (function (_super) {
    __extends(SecurityCodesFormComponent, _super);
    function SecurityCodesFormComponent(securityCodeService, injector) {
        var _this = _super.call(this, injector) || this;
        _this.securityCodeService = securityCodeService;
        _this.securityCode = new _models__WEBPACK_IMPORTED_MODULE_3__["SecurityCode"]();
        return _this;
    }
    SecurityCodesFormComponent.prototype.ngOnInit = function () {
        this.initForm([]);
    };
    SecurityCodesFormComponent.prototype.initForm = function (providers, id) {
        var _this = this;
        if (id === void 0) { id = null; }
        this.id = id;
        this.providers = providers;
        this.form = this.formBuilder.group({
            CODE: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            CODE_TYPE: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            PROVIDER_ID: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            DESCRIPTION: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
        if (id) {
            this.securityCodeService.get(id).subscribe(function (res) {
                console.log(res);
                _this.securityCode = res.shift();
                delete _this.securityCode.ID;
                _this.form.patchValue(_this.securityCode);
            });
            this.save = this.securityCodeService.update.bind(this.securityCodeService);
        }
        else {
            this.save = this.securityCodeService.create.bind(this.securityCodeService);
        }
        this.form.markAsUntouched();
    };
    SecurityCodesFormComponent.prototype.doSave = function () {
        var _this = this;
        Object.assign(this.securityCode, this.form.value);
        if (this.validate())
            return;
        this.save(this.id, this.securityCode)
            .subscribe(function (data) {
            console.log(data);
            _this.toastr.success('Security code saved!', 'Success');
            _this.submit.emit();
        }, function (error) {
            console.log(error);
            _this.toastr.error('Attention, make sure your record has been saved.', 'Danger');
        });
    };
    SecurityCodesFormComponent.prototype.title = function () {
        if (!this.id)
            return 'New';
        else
            return 'Edit';
    };
    SecurityCodesFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'security-codes-form',
            template: __webpack_require__(/*! ./security-codes-form.component.html */ "./src/app/modules/security-codes/form/security-codes-form.component.html"),
            styles: [__webpack_require__(/*! ./security-codes-form.component.css */ "./src/app/modules/security-codes/form/security-codes-form.component.css")]
        }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_2__["SecurityCodeService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]])
    ], SecurityCodesFormComponent);
    return SecurityCodesFormComponent;
}(_shared_template_component__WEBPACK_IMPORTED_MODULE_4__["TemplateComponent"]));



/***/ }),

/***/ "./src/app/modules/security-codes/list/security-codes-list.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/modules/security-codes/list/security-codes-list.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"page_content\">\n    \n    <div id=\"page_content_inner\">\n        <div class=\"md-card\">\n            <div class=\"md-card-toolbar\">\n                <h3 class=\"md-card-toolbar-heading-text\">\n                    <i class=\"material-icons\">spellcheck</i> Security Codes\n                </h3>\n            </div>\n            <div class=\"md-card-content\">\n                <div class=\"dt_colVis_buttons\"></div>\n                <div class=\"uk-overflow-container\">\n                    <table datatable [dtTrigger]=\"dtTrigger\" [dtOptions]=\"dtOptions\" style=\"width:100%\" \n                    class=\"row-border hover uk-table uk-table-hover uk-table-striped dataTable nowrap\">                                            \n                        <thead>\n                            <tr>\n                                <th>Type</th>\n                                <th>Code</th>\n                                <th>Provider</th>\n                                <th>Description</th>                                \n                                <th>Actions</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let securityCode of securityCodes\">\n                                <td>{{ securityCode.CODE_TYPE }}</td>\n                                <td>{{ securityCode.CODE }}</td>\n                                <td>{{ securityCode.PROVIDER }}</td>                                \n                                <td>{{ securityCode.DESCRIPTION }}</td>                                \n                                <td>\n                                    <a id=\"bt-edit\" (click)=\"editSecurityCode(form, securityCode.ID)\" data-uk-modal><i class=\"md-icon material-icons\">mode_edit</i></a>\n                                    <a (click)=\"removeSecurityCode(securityCode.ID)\"><i class=\"md-icon material-icons\">delete_forever</i></a>\n                                </td>\n                            </tr>\n                        </tbody>\n                    </table>\n                </div>                    \n            </div>\n        </div>\n    </div>\n</div>\n\n<div id=\"security-code-form\" class=\"uk-modal\">\n    <div class=\"uk-modal-dialog uk-modal-dialog-large\" style=\"top: 30px\">\n      <a href=\"\" class=\"uk-modal-close uk-close\"></a>\n      <security-codes-form #form (submit)=\"closeModal()\"></security-codes-form>\n    </div>\n  </div>\n\n<div class=\"md-fab-wrapper\">     \n    <a class=\"md-fab md-fab-accent\" (click)=\"newSecurityCode(form)\" data-uk-modal>\n        <i class=\"material-icons\">&#xE145;</i>\n    </a>    \n</div>"

/***/ }),

/***/ "./src/app/modules/security-codes/list/security-codes-list.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/modules/security-codes/list/security-codes-list.component.ts ***!
  \******************************************************************************/
/*! exports provided: SecurityCodesListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityCodesListComponent", function() { return SecurityCodesListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SecurityCodesListComponent = /** @class */ (function () {
    function SecurityCodesListComponent(securityCodeService, providerService) {
        this.securityCodeService = securityCodeService;
        this.providerService = providerService;
        this.dtOptions = {};
        this.securityCodes = [];
        this.providers = [];
        this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.modal = false;
    }
    SecurityCodesListComponent.prototype.ngOnInit = function () {
        this.loadSecurityCodes();
    };
    SecurityCodesListComponent.prototype.loadSecurityCodes = function () {
        var _this = this;
        var that = this;
        this.securityCodeService.get()
            .subscribe(function (data) {
            that.securityCodes = data;
            _this.providerService.get().subscribe(function (providers) {
                _this.providers = providers;
                that.securityCodes.map(function (securityCode) {
                    for (var i = 0; i < providers.length; i++) {
                        if (securityCode.PROVIDER_ID == providers[i].ID)
                            securityCode.PROVIDER = providers[i].NAME;
                    }
                    return securityCode;
                });
                console.log(that.securityCodes);
            });
            _this.rerender();
        }, function (error) {
            console.log(error);
        });
    };
    SecurityCodesListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    SecurityCodesListComponent.prototype.ngOnDestroy = function () {
        this.dtTrigger.unsubscribe();
    };
    SecurityCodesListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    SecurityCodesListComponent.prototype.removeSecurityCode = function (id) {
        var _this = this;
        this.securityCodeService.delete(id)
            .subscribe(function (data) {
            _this.loadSecurityCodes();
        }, function (error) {
            console.log(error);
        });
    };
    SecurityCodesListComponent.prototype.newSecurityCode = function (form) {
        form.initForm(this.providers);
        this.showModal();
    };
    SecurityCodesListComponent.prototype.editSecurityCode = function (form, id) {
        form.initForm(this.providers, id);
        this.showModal();
    };
    SecurityCodesListComponent.prototype.showModal = function () {
        UIkit.modal(document.getElementById('security-code-form')).show();
    };
    SecurityCodesListComponent.prototype.closeModal = function () {
        UIkit.modal(document.getElementById('security-code-form')).hide();
        this.loadSecurityCodes();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"]),
        __metadata("design:type", angular_datatables__WEBPACK_IMPORTED_MODULE_1__["DataTableDirective"])
    ], SecurityCodesListComponent.prototype, "dtElement", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('modalComponent'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], SecurityCodesListComponent.prototype, "modalForm", void 0);
    SecurityCodesListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-security-codes-list',
            template: __webpack_require__(/*! ./security-codes-list.component.html */ "./src/app/modules/security-codes/list/security-codes-list.component.html")
        }),
        __metadata("design:paramtypes", [_services__WEBPACK_IMPORTED_MODULE_3__["SecurityCodeService"], _services__WEBPACK_IMPORTED_MODULE_3__["ProviderService"]])
    ], SecurityCodesListComponent);
    return SecurityCodesListComponent;
}());



/***/ }),

/***/ "./src/app/modules/security-codes/security-codes-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/modules/security-codes/security-codes-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: SecurityCodesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityCodesRoutingModule", function() { return SecurityCodesRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _list_security_codes_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./list/security-codes-list.component */ "./src/app/modules/security-codes/list/security-codes-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _list_security_codes_list_component__WEBPACK_IMPORTED_MODULE_2__["SecurityCodesListComponent"]
    },
];
var SecurityCodesRoutingModule = /** @class */ (function () {
    function SecurityCodesRoutingModule() {
    }
    SecurityCodesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], SecurityCodesRoutingModule);
    return SecurityCodesRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/security-codes/security-codes.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/modules/security-codes/security-codes.module.ts ***!
  \*****************************************************************/
/*! exports provided: SecurityCodesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityCodesModule", function() { return SecurityCodesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _security_codes_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./security-codes-routing.module */ "./src/app/modules/security-codes/security-codes-routing.module.ts");
/* harmony import */ var _list_security_codes_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./list/security-codes-list.component */ "./src/app/modules/security-codes/list/security-codes-list.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/modules/_shared/shared.module.ts");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _form_security_codes_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./form/security-codes-form.component */ "./src/app/modules/security-codes/form/security-codes-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var SecurityCodesModule = /** @class */ (function () {
    function SecurityCodesModule() {
    }
    SecurityCodesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                angular_datatables__WEBPACK_IMPORTED_MODULE_5__["DataTablesModule"],
                _security_codes_routing_module__WEBPACK_IMPORTED_MODULE_2__["SecurityCodesRoutingModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]
            ],
            declarations: [_list_security_codes_list_component__WEBPACK_IMPORTED_MODULE_3__["SecurityCodesListComponent"], _form_security_codes_form_component__WEBPACK_IMPORTED_MODULE_6__["SecurityCodesFormComponent"]],
            providers: [_services__WEBPACK_IMPORTED_MODULE_8__["SecurityCodeService"], _services__WEBPACK_IMPORTED_MODULE_8__["ProviderService"]]
        })
    ], SecurityCodesModule);
    return SecurityCodesModule;
}());



/***/ })

}]);
//# sourceMappingURL=modules-security-codes-security-codes-module.js.map