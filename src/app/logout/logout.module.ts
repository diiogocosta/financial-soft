import { NgModule } from '@angular/core';
import { LogoutComponent } from './logout.component';
import { SharedModule } from '../modules/_shared/shared.module';

@NgModule({
  imports: [
    SharedModule    
  ],
  declarations: [LogoutComponent],
  providers: []
})
export class LogoutModule { }
