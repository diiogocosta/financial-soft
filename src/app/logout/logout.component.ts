﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService, AuthenticationService } from '../_services';

@Component({
    templateUrl: 'logout.component.html',
    styleUrls: ['../../assets/css/custom.css']
})

export class LogoutComponent implements OnInit {
    
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService) {}

    ngOnInit() {
        this.authenticationService.logout();
        this.router.navigate(['/']);
    }

}
