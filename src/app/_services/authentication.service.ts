﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, first } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {

    private waitingRefresh = false;

    constructor(private http: HttpClient) {}


    private saveToken(resp) {
        // login successful if there's a jwt token in the response
        var tokenInfo = resp.token.split('.');
        var header = atob(tokenInfo[0]);
        var body = atob(tokenInfo[1]);        
        var signature = tokenInfo[2];

        if (resp.token) {
            localStorage.setItem('token-get', (Math.floor(Date.now() / 1000)).toString());
            localStorage.setItem('token-bearer', `Bearer ${resp.token}`);
            localStorage.setItem('token-header', header);
            localStorage.setItem('token-body', body);
            localStorage.setItem('token-signature', signature);
        }
        return JSON.parse(body);
    };

    login(username: string, password: string) {        

        return this.http.post<any>(environment.apiUrl + '/authentication/token', { username: username, password: password })
            .pipe(map(resp => {
                return this.saveToken(resp);
            }));
    }

    refresh(timeout, callback) {
        if(this.waitingRefresh)  // se já tenho o timer, não inicio novamente
            return false;
        
        setTimeout(()=>{
            this.http.post<any>(environment.apiUrl + '/authentication/refresh', {})
            .subscribe(
                resp => {
                    this.waitingRefresh = false;
                    this.saveToken(resp);
                    callback();
                },
                error => {
                    this.logout();
                });
        }, timeout*950 ); // diminui um pouco o timeout pra garantir, da pra pensar em algo melhor isso é 95%, mas convertendo de mili pra seg
        this.waitingRefresh = true;
    }

    logout() {
        // remove token info from local storage to log user out
        localStorage.removeItem('token-header');
        localStorage.removeItem('token-body');
        localStorage.removeItem('token-signature');
        localStorage.removeItem('token-bearer');
        localStorage.removeItem('token-get');
    }
}