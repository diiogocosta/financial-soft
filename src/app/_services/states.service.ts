import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AvailableCountries } from "../_services/countries.service";


@Injectable()
export class StatesService {
    
    constructor() {}

    getStates(Country : string) : Observable<string[]> {

        console.log(Country);

        let states = [];
        
        switch (Country) {
            case AvailableCountries.Brazil: 
                states = [
                    'Acre',
                    'Alagoas',
                    'Amapá',
                    'Amazonas',
                    'Bahia',
                    'Ceará',
                    'Distrito Federal',
                    'Espírito Santo',
                    'Goiás',
                    'Maranhão',
                    'Mato Grosso',
                    'Mato Grosso do Sul',
                    'Minas Gerais',
                    'Pará',
                    'Paraíba',
                    'Paraná',
                    'Pernambuco',
                    'Piauí',
                    'Rio de Janeiro',
                    'Rio Grande do Norte',
                    'Rio Grande do Sul',
                    'Rondônia',
                    'Roraima',
                    'Santa Catarina',
                    'São Paulo',
                    'Sergipe',
                    'Tocantins'
                ];
            break;
            case AvailableCountries.Canada: 
                states = [
                    'Alberta',
                    'British Columbia',
                    'Manitoba',
                    'New Brunswick',
                    'Newfoundland',
                    'Northwest Territories',
                    'Nova Scotia',
                    'Nunavut',
                    'Ontario',
                    'Prince Edward Island',
                    'Quebec',
                    'Saskatchewan',
                    'Yukon'
                ];
            break;
            case AvailableCountries.UnitedStates: 
                states = [
                    'Alabama',
                    'Alaska',
                    'Arizona',
                    'Arkansas',
                    'California',
                    'Colorado',
                    'Connecticut',
                    'Delaware',
                    'District of Columbia',
                    'Florida',
                    'Georgia',
                    'Hawaii',
                    'Idaho',
                    'Illinois',
                    'Indiana',
                    'Iowa',
                    'Kansas',
                    'Kentucky',
                    'Louisiana',
                    'Maine',
                    'Maryland',
                    'Massachusetts',
                    'Michigan',
                    'Minnesota',
                    'Mississippi',
                    'Missouri',
                    'Montana',
                    'Nebraska',
                    'Nevada',
                    'New Hampshire',
                    'New Jersey',
                    'New Mexico',
                    'New York',
                    'North Carolina',
                    'North Dakota',
                    'Ohio',
                    'Oklahoma',
                    'Oregon',
                    'Pennsylvania',
                    'Rhode Island',
                    'South Carolina',
                    'South Dakota',
                    'Tennessee',
                    'Texas',
                    'Utah',
                    'Vermont',
                    'Virginia',
                    'Washington',
                    'West Virginia',
                    'Wisconsin',
                    'Wyoming'
                ];
            break;
            default:
                console.log('Outro país selecionado!');
                states = [];
            break;
        }

        return of(states);
    }
}