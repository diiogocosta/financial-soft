import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class ProductService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/profile_product/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<Product[]>(url);
    }

    create(id = null, product: Object) {
        return this.http.post<Product[]>(this.serverURL, product);
    }

    update(id: string, product: Object) {
        return this.http.put<Product[]>(this.serverURL + id, product);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<Product[]>(url);
    }

    getByName(name) {
        return this.http.get<Product[]>(environment.apiUrl +'/api/profile_product?PRODUCT='+name);
    }
}