import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReasonCode, InternalReasonCode } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class ReasonCodeService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/provider_reason_code/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<ReasonCode[]>(url);
    }

    create(id = null, reasonCode: Object) {
        return this.http.post<ReasonCode[]>(this.serverURL, reasonCode);
    }

    update(id: string, reasonCode: Object) {
        return this.http.put<ReasonCode[]>(this.serverURL + id, reasonCode);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<ReasonCode[]>(url);
    }

    getInternal(){
        return this.http.delete<InternalReasonCode[]>(environment.apiUrl+'/api/internal_reason_code/');
    }
}