import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SecurityCode } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class SecurityCodeService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/provider_security_code/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<SecurityCode[]>(url);
    }

    create(id = null, securityCode: Object) {
        return this.http.post<SecurityCode[]>(this.serverURL, securityCode);
    }

    update(id: string, securityCode: Object) {
        return this.http.put<SecurityCode[]>(this.serverURL + id, securityCode);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<SecurityCode[]>(url);
    }
}