import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Provider } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class ProviderService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/provider/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<Provider[]>(url);
    }

    create(provider: Object) {
        return this.http.post<Provider[]>(this.serverURL, provider);
    }

    update(id: string, provider: Object) {
        return this.http.put<Provider[]>(this.serverURL + id, provider);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<Provider[]>(url);
    }
}