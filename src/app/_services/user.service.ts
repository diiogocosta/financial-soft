﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    private serverURL = 'http://localhost:3000';

    getAll() {
        return this.http.get<User[]>(this.serverURL + '/api/users');
    }

    getById(id: number) {
        return this.http.get(this.serverURL + '/api/users/' + id);
    }

    create(user: User) {
        return this.http.post(this.serverURL + '/api/account', user);
    }

    update(user: User) {
        return this.http.put(this.serverURL + '/api/users/' + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(this.serverURL + '/api/users/' + id);
    }
}