import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Settings } from '../_models';

@Injectable()
export class SettingsService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/settings/';

    get() {        
        return this.http.get<Settings[]>(this.serverURL);
    }
    
    update(id,settings: Object) {
        return this.http.put<Settings[]>(`${this.serverURL}${id}`,settings);
    }    
}