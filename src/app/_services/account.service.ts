import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Account } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class AccountService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/account/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<Account[]>(url);
    }

    create(id = null, account: Object) {
        return this.http.post<Account[]>(this.serverURL, account);
    }

    update(id: string, account: Object) {
        return this.http.put<Account[]>(this.serverURL + id, account);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<Account[]>(url);
    }
}