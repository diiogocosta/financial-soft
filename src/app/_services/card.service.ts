import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Card } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class CardService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/provider_card/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<Card[]>(url);
    }

    create(id = null, card: Card) {
        return this.http.post<Card[]>(this.serverURL, card);
    }

    update(id: string, card: Card) {
        return this.http.put<Card[]>(this.serverURL + id, card);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<Card[]>(url);
    }

    getExpirationYear() : Number[] {

        let arr = [];

        for (let i = 0; i <= 10; i++){
            
            arr.push(new Date().getFullYear() + i);
        }

        return arr;
    }

    applyCardFees(cc : Card, amount : number) : { 
        serviceFee : number;
        serviceFeePercentage : number;
        amountFeePercentage : number;
        totalFees : number;
        grandTotal : number;
    } {

        if (!cc)
            return { serviceFee : 0, serviceFeePercentage : 0, amountFeePercentage : 0, totalFees : 0, grandTotal : 0 };

        amount = (amount) ? amount : 0;
        let serviceFeePercentage = (cc.SERVICE_FEE_PERCENTAGE) ? parseFloat(cc.SERVICE_FEE_PERCENTAGE) : 0;
        let amountFeePercentage = ((Math.round(serviceFeePercentage * 100) * Math.round(amount * 100) / 100) / 100) /100;

        let serviceFee = (cc.SERVICE_FEE) ? parseFloat(cc.SERVICE_FEE) : 0;        
        let totalFees = Math.round((amountFeePercentage * 100) + (serviceFee * 100)) / 100;
        let grandTotal = Math.round((amount * 100) + (totalFees * 100)) / 100;
        
        return {
            serviceFee : serviceFee,
            serviceFeePercentage : serviceFeePercentage,
            amountFeePercentage : amountFeePercentage,
            totalFees : totalFees,
            grandTotal : grandTotal
        };
    }
}