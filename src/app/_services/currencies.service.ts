import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Currency } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class CurrenciesService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/currency/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<Currency[]>(url);
    }

    create(id = null, currency: Object) {
        return this.http.post<Currency[]>(this.serverURL, currency);
    }

    update(id: string, currency: Object) {
        return this.http.put<Currency[]>(this.serverURL + id, currency);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<Currency[]>(url);
    }
}