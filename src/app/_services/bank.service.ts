import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bank } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class BankService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/provider_bank/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<Bank[]>(url);
    }

    create(id = null, bank: Object) {
        return this.http.post<Bank[]>(this.serverURL, bank);
    }

    update(id: string, bank: Object) {
        return this.http.put<Bank[]>(this.serverURL + id, bank);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<Bank[]>(url);
    }
}