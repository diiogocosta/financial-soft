import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

export interface Month {
    name : string,
    num : number
}

@Injectable()
export class MonthService {
    
    constructor() { }

    getMonths() : Observable<Month[]> {

        let months = [
            {
                name : 'January',
                num : 1
            },
            {
                name : 'February',
                num : 2
            },
            {
                name : 'March',
                num : 3
            },
            {
                name : 'April',
                num : 4
            },
            {
                name : 'May',
                num : 5
            },
            {
                name : 'June',
                num : 6
            },
            {
                name : 'July',
                num : 7
            },
            {
                name : 'August',
                num : 8
            },
            {
                name : 'September',
                num : 9
            },
            {
                name : 'October',
                num : 10
            },
            {
                name : 'November',
                num : 11
            },
            {
                name : 'December',
                num : 12
            }
        ];

        return of(months);
    }
}