import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CustomerOrder, CustomerOrderData, OrderItem, TxnOperation, Payment, Card, PaymentMethod } from '../_models';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

export interface PaymentResponse {
    ORDERS : CustomerOrderData[]
}

@Injectable()
export class OrderPaymentService {
    constructor(private http: HttpClient) { }

    private _orderUrl = '/api/order/';
    
    getCustomerOrders() : Observable<any> {
        let url = `${environment.apiUrl + this._orderUrl}customer_orders?TRANSACTION_TYPE=Payment`;
        return this.http.get<CustomerOrder[]>(url);
    }

    getCustomerOrderData(id : string, billToEmail : string) : Observable<any> {
        let url = `${environment.apiUrl + this._orderUrl}customer_order_data?TRANSACTION_TYPE=Payment&CUSTOMERID=${id}&BILLTO_EMAIL=${billToEmail}`;
        return this.http.get<CustomerOrderData[]>(url);
    }

    getExternalCustomerOrderData(data : string, billToEmail : string) : Observable<any> {
        let url = `${environment.apiUrl}/api/order_external/customer_order_data?TRANSACTION_TYPE=Payment&DATA=${data}&BILLTO_EMAIL=${billToEmail}`;
        return this.http.get<CustomerOrderData[]>(url);
    }

    getOrderItems(id : string = null) : Observable<OrderItem[]> {
        let url = `${environment.apiUrl}/api/order_item?ORDER_ID=${id}`;
        return this.http.get<OrderItem[]>(url);
    }

    
    getPaymentMethodList(data ?: string) : Observable<any[]> {
        let url = (data) ? `${environment.apiUrl}/api/profile_payment_method_external?ORDER_WEB_PAYMENT_FLG=Y&DATA=${data}` : `${environment.apiUrl}/api/profile_payment_method?ORDER_UI_PAYMENT_FLG=Y`;
        return this.http.get<PaymentMethod[]>(url);
    }

    getTxnOperation(requestId : string) : Observable<any> {
        let url = `${environment.apiUrl}/api/txn_operation?REQUESTID=${requestId}`;
        return this.http.get<TxnOperation[]>(url);
    }

    getListValidCards(data ?: string) : Observable<Card[]> {
        let url = (data) ? `${environment.apiUrl}/api/profile_payment_method_external/list_valid_cards?ORDER_WEB_PAYMENT_FLG=Y&DATA=${data}` : `${environment.apiUrl}/api/profile_payment_method/list_valid_cards`;
        return this.http.get<Card[]>(url);
    }

    postPayment(payment : Payment) : Observable<PaymentResponse> {

        let url = (payment.DATA) ? `${environment.apiUrl}/api/order/payment?DATA=${payment.DATA}` : `${environment.apiUrl}/api/order/payment`
        return this.http.post<PaymentResponse>(url,payment);
    }
}