import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TransactionType } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class TransactionTypeService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/provider_txn_type/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<TransactionType[]>(url);
    }

    create(id = null, transactionType: Object) {
        return this.http.post<TransactionType[]>(this.serverURL, transactionType);
    }

    update(id: string, transactionType: Object) {
        return this.http.put<TransactionType[]>(this.serverURL + id, transactionType);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<TransactionType[]>(url);
    }
}