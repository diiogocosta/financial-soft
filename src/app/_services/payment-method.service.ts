import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PaymentMethod } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class PaymentMethodService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/payment_method/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<PaymentMethod[]>(url);
    }

    create(id = null, paymentMethod: Object) {
        return this.http.post<PaymentMethod[]>(this.serverURL, paymentMethod);
    }

    update(id: string, paymentMethod: Object) {
        return this.http.put<PaymentMethod[]>(this.serverURL + id, paymentMethod);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<PaymentMethod[]>(url);
    }
}