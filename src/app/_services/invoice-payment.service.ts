import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { CustomerInvoice } from '../_models/customer-invoice';
import { CustomerInvoiceData } from '../_models/customer-invoice-data';
import { PaymentMethod } from '../_models';

@Injectable()
export class InvoicePaymentService {
    constructor(private http: HttpClient) { }

    private _url = '/api/invoice/';
    
    getCustomerOrders() : Observable<any> {
        let url = `${environment.apiUrl + this._url}customer_invoices`;
        return this.http.get<CustomerInvoice[]>(url);
    }

    getCustomerInvoiceData(id : string) : Observable<any> {
        let url = `${environment.apiUrl + this._url}customer_invoice_data?CUSTOMERID=${id}`;
        return this.http.get<CustomerInvoiceData[]>(url);
    }
    
    getPaymentMethodList() : Observable<any[]> {
        let url = `${environment.apiUrl}/api/profile_payment_method?INVOICE_UI_PAYMENT_FLG=Y`;
        return this.http.get<PaymentMethod[]>(url);
    }

    // getTxnOperation(requestId : string) : Observable<any> {
    //     let url = `${environment.apiUrl}/api/txn_operation?REQUESTID=${requestId}`;
    //     return this.http.get<TxnOperation[]>(url);
    // }

    // getListValidCards(data ?: string) : Observable<Card[]> {
    //     let url = (data) ? `${environment.apiUrl}/api/profile_payment_method_external/list_valid_cards?ORDER_WEB_PAYMENT_FLG=Y&DATA=${data}` : `${environment.apiUrl}/api/profile_payment_method/list_valid_cards`;
    //     return this.http.get<Card[]>(url);
    // }

    // postPayment(payment : Payment) : Observable<PaymentResponse> {

    //     let url = (payment.DATA) ? `${environment.apiUrl}/api/order/payment?DATA=${payment.DATA}` : `${environment.apiUrl}/api/order/payment`
    //     return this.http.post<PaymentResponse>(url,payment);
    // }
}