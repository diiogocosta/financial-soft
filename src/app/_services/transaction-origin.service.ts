import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TransactionOrigin } from '../_models';
import { environment } from '../../environments/environment';

@Injectable()
export class TransactionOriginService {
    constructor(private http: HttpClient) { }

    private serverURL = environment.apiUrl +'/api/txn_origin/';

    get(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.get<TransactionOrigin[]>(url);
    }

    create(id = null, transactionOrigin: Object) {
        return this.http.post<TransactionOrigin[]>(this.serverURL, transactionOrigin);
    }

    update(id: string, transactionOrigin: Object) {
        return this.http.put<TransactionOrigin[]>(this.serverURL + id, transactionOrigin);
    }

    delete(id = null) {
        var url = id ? this.serverURL + id : this.serverURL;
        return this.http.delete<TransactionOrigin[]>(url);
    }
}