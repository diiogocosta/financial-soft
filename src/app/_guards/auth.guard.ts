﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {AuthenticationService} from '../_services/authentication.service'

@Injectable()
export class AuthGuard implements CanActivate {
    private permissions = new Object();

    constructor(private router: Router, private authenticationService : AuthenticationService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {                
        var token = localStorage.getItem('token-body');
        if (!token) {
            this.router.navigate(['login'], { queryParams: { returnUrl: state.url }});
            return false;
        }        

        token = JSON.parse(token);
        
        // horário que pegou o token
        var tokenGet = parseInt(localStorage.getItem('token-get'));
        
        var current_timestamp = Math.floor(Date.now() / 1000)
        
        // token expirou
        if(current_timestamp - tokenGet > token['expiry_date'] - token['create_date']){
            this.router.navigate(['login'], { queryParams: { returnUrl: state.url }});
            return false;
        }
        
        this.permissions = JSON.parse(localStorage.getItem("token-body")).permissions;

        //valida se possui permissão à tela/operacao
        
        if(state.url != '/'){
            var url = state.url.split('/')
            var tela = url[1].toUpperCase();
            var operacao = url[2] || 'list';
            //não possui acesso à tela
            if (tela == 'PAGE') tela = 'DASHBOARD';
            if(!this.permissions[tela]){
                this.router.navigate(['/']);
                return false;
            }

            
            if(operacao != ''){
                switch (operacao) {
                    case 'list':
                        if(this.permissions[tela]['READ_FLAG'] != 'Y')
                            this.router.navigate(['']);
                        break;
                    case 'create':
                        if(this.permissions[tela]['INSERT_FLAG'] != 'Y')
                            this.router.navigate(['' + tela.toLowerCase()]);
                        break;
                    case 'edit':
                        if(this.permissions[tela]['UPDATE_FLAG'] != 'Y')
                            this.router.navigate(['' + tela.toLowerCase()]);
                        break;
                    default:
                        break;
                }
            }
        }
        var timeoutRefreshToken = ((current_timestamp + tokenGet) - (token['expiry_date'] + token['create_date'])) * -1
        var that = this;
        var refreshToken = function (){
            that.authenticationService.refresh(timeoutRefreshToken, refreshToken);
        };
        refreshToken();
        return true;
    }
}