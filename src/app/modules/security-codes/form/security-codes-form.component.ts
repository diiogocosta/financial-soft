import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { SecurityCodeService } from '../../../_services';
import { SecurityCode, Provider } from '../../../_models';
import { TemplateComponent } from '../../_shared/template.component';

@Component({
    selector: 'security-codes-form',
    templateUrl: './security-codes-form.component.html',
    styleUrls: ['./security-codes-form.component.css']
})

export class SecurityCodesFormComponent extends TemplateComponent {    
    providers: Provider[];
    securityCode: SecurityCode = new SecurityCode();        

    constructor(private securityCodeService: SecurityCodeService, injector: Injector) { 
        super(injector);    
    }

    ngOnInit() {
        this.initForm([]);
    }            

    initForm(providers: Provider[],id = null){     
        this.id = id;    
        this.providers = providers;

        this.form = this.formBuilder.group({                
                CODE: ['',Validators.required],
                CODE_TYPE: ['',Validators.required],
                PROVIDER_ID: ['',Validators.required],
                DESCRIPTION: ['', Validators.required],                
        });         

        if (id){                
            this.securityCodeService.get(id).subscribe((res: SecurityCode[])=>{                                
                console.log(res);
                this.securityCode = res.shift();                
                delete this.securityCode.ID;
                this.form.patchValue(this.securityCode);                  
            })
            this.save = this.securityCodeService.update.bind(this.securityCodeService);            
        } else {
            this.save = this.securityCodeService.create.bind(this.securityCodeService);            
        }    
        
        this.form.markAsUntouched();
    }
    
    doSave() {
        Object.assign(this.securityCode,this.form.value);
                       
        if (this.validate()) return 

        this.save(this.id,this.securityCode)
        .subscribe(
            data => {
                console.log(data);
                this.toastr.success('Security code saved!','Success')    
                this.submit.emit();
            },
            error => {                              
                console.log(error);
                this.toastr.error('Attention, make sure your record has been saved.','Danger')
            }
        );
    }    

    title(){
        if (!this.id)
            return 'New'
        else 
            return 'Edit'
    }
}