import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecurityCodesRoutingModule } from './security-codes-routing.module';

import { SecurityCodesListComponent } from './list/security-codes-list.component';
import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { SecurityCodesFormComponent } from './form/security-codes-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SecurityCodeService, ProviderService } from '../../_services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    SecurityCodesRoutingModule,
    SharedModule    
  ],
  declarations: [SecurityCodesListComponent, SecurityCodesFormComponent],
  providers: [SecurityCodeService, ProviderService]
})
export class SecurityCodesModule { }
