import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SecurityCodesListComponent } from './list/security-codes-list.component';
const routes: Routes = [
  {
    path: '', component: SecurityCodesListComponent
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityCodesRoutingModule { }
