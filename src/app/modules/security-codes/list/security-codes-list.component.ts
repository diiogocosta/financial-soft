import { Component, OnInit, OnDestroy, ViewChild, ElementRef  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { SecurityCode, Provider } from '../../../_models';
import { SecurityCodeService, ProviderService } from '../../../_services';
import { SecurityCodesFormComponent } from '../form/security-codes-form.component';

declare var UIkit: any;

@Component({
  selector: 'app-security-codes-list',
  templateUrl: './security-codes-list.component.html'
})

export class SecurityCodesListComponent implements OnDestroy, OnInit {    
    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('modalComponent') modalForm: ElementRef;
    
    dtOptions: DataTables.Settings = {};
    securityCodes: SecurityCode[] = [];
    providers: Provider[] = [];
    dtTrigger= new Subject();
    modal = false;

    constructor(private securityCodeService: SecurityCodeService, private providerService: ProviderService) {}
    
    ngOnInit() {
        this.loadSecurityCodes();
    }
    
    private loadSecurityCodes() {
        const that = this;

        this.securityCodeService.get()
        .subscribe(
            data => {
                that.securityCodes = data;    
                this.providerService.get().subscribe(providers=>{
                    this.providers = providers;
                    that.securityCodes.map(securityCode=>{
                        for (let i=0; i < providers.length; i++){
                            if (securityCode.PROVIDER_ID == providers[i].ID)   
                                securityCode.PROVIDER = providers[i].NAME;                                                        
                        }
                        return securityCode;                                                                                                            
                    })
                    console.log(that.securityCodes);
                })
                this.rerender();            
            },
            error => {
                console.log(error);
            }
        );
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
          dtInstance.destroy();          
          this.dtTrigger.next();
        });
      }

    ngOnDestroy(): void {                        
        this.dtTrigger.unsubscribe();
    }

    ngAfterViewInit(){
        this.dtTrigger.next();                
    }

    removeSecurityCode(id){

        this.securityCodeService.delete(id)
        .subscribe(
            data => {                                
                this.loadSecurityCodes();
            },
            error => {        
                console.log(error);
            }
        );
    }

    newSecurityCode(form: SecurityCodesFormComponent){
        form.initForm(this.providers);        
        this.showModal();
    }

    editSecurityCode(form: SecurityCodesFormComponent,id){
        form.initForm(this.providers,id);
        this.showModal();
    }    

    showModal(){
        UIkit.modal(document.getElementById('security-code-form')).show();        
    }

    closeModal(){                
        UIkit.modal(document.getElementById('security-code-form')).hide();
        this.loadSecurityCodes();
    }    
}