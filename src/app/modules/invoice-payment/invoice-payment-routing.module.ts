import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvoicePaymentPanelComponent } from './invoice-payment-panel/invoice-payment-panel.component';

const routes: Routes = [
  {
    path: '', component: InvoicePaymentPanelComponent
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicePaymentRoutingModule { }
