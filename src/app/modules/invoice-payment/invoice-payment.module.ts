import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicePaymentRoutingModule } from './invoice-payment-routing.module';
import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvoicePaymentService } from '../../_services/invoice-payment.service';
import { InvoicePaymentPanelComponent } from './invoice-payment-panel/invoice-payment-panel.component';
import { InvoiceDetailsComponent } from "./invoice-details/invoice-details.component";
import { CountriesService } from '../../_services/countries.service';
import { StatesService } from '../../_services/states.service';
import { CardService } from '../../_services/card.service';
import { MonthService } from '../../_services';
import { OrderPaymentModule } from '../order-payment/order-payment.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    InvoicePaymentRoutingModule,
    SharedModule,
    OrderPaymentModule
  ],
  declarations: [InvoicePaymentPanelComponent, InvoiceDetailsComponent],
  providers: [ InvoicePaymentService, CountriesService, StatesService, CardService, MonthService ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class InvoicePaymentModule { }
