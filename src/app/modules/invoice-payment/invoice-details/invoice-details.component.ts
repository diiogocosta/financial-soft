import { Component, OnInit, OnDestroy, EventEmitter, AfterViewInit, ViewChildren, QueryList  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { TranslateService } from '../../../_translate/translate.service';
import { InvoicePaymentService } from '../../../_services/invoice-payment.service';
import { CustomerInvoice } from '../../../_models/customer-invoice';
import { CustomerInvoiceData } from '../../../_models/customer-invoice-data';

@Component({
  selector: 'app-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.css'],
  inputs: ['customerInvoice','showMinSelectError','billToEmail'],
  outputs: ['onChangeSelection']
})

export class InvoiceDetailsComponent implements OnDestroy, OnInit, AfterViewInit {
    
    @ViewChildren(DataTableDirective) dtElement: QueryList<DataTableDirective>;

    dtInvoice : DataTableDirective;

    dtOptions : DataTables.Settings = {};
    dtOptionsCustomized : DataTables.Settings = {};
    dtTrigger = new Subject();
    dtTriggerCustomized = new Subject();
    childRows = [];

    customerInvoice : CustomerInvoice;

    invoiceDetails : CustomerInvoiceData[];
    onChangeSelection = new EventEmitter<any>();
    invoiceAllChecked : boolean;

    showMinSelectError : boolean;
    totalAmount : number = 0;
    totalPaid : number = 0;
    totalOpen : number = 0;
    totalAmountToPay : number = 0;
    
    constructor(private _invoice: InvoicePaymentService, private translate : TranslateService) {
    }
    
    ngOnInit() {
        this._loadAllProviders();
    }

    private _loadAllProviders() {
        const that = this;
        this.onChangeSelection.emit({selectedInvoices : [], ignoreMinSelectError : true});
        
        this._invoice.getCustomerInvoiceData(this.customerInvoice.CUSTOMERID)
            .subscribe(customerInvoiceData => {
                that.invoiceDetails = customerInvoiceData;

                this._sumTotal();
                this._renderer();
            }, error => {
                console.log(error);
            });
    }

    private _renderer() {

        this.dtInvoice = this.dtElement.first;

        this.dtInvoice.dtOptions.lengthChange = false;
        this.dtInvoice.dtOptions.searching = false;
        this.dtInvoice.dtOptions.paging = false;
        this.dtInvoice.dtOptions.ordering = true;

        this.dtInvoice.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
        });
    }

    private _sumTotal() {
        this.invoiceDetails.map((i) => {
            
            i.AMOUNT_TO_PAY = '0';
            i.TOTAL_AMOUNT = (i.TOTAL_AMOUNT) ? i.TOTAL_AMOUNT : '0';
            i.PAID_AMOUNT = (i.PAID_AMOUNT) ? i.PAID_AMOUNT : '0';            
            i.OPEN_AMOUNT = String(Math.round((parseFloat(i.TOTAL_AMOUNT) * 100) - (parseFloat(i.PAID_AMOUNT) * 100)) / 100);
            
            this.totalAmount = Math.round((this.totalAmount * 100) + (parseFloat(i.TOTAL_AMOUNT) * 100)) / 100;
            this.totalPaid = Math.round((this.totalPaid * 100) + (parseFloat(i.PAID_AMOUNT) * 100)) / 100;
        });

        this.totalOpen = Math.round((this.totalAmount * 100) - (this.totalPaid * 100)) / 100;
    }

    check(invoice : CustomerInvoiceData, checked : boolean) {

        invoice.SELECTED = checked;
        invoice.AMOUNT_TO_PAY = (checked) ? invoice.OPEN_AMOUNT : '0';
        this.sumTotalAmountToPay();

        this.invoiceAllChecked = this.invoiceDetails.every(i => i.SELECTED);
        this.onChangeSelection.emit({selectedInvoices : this.invoiceDetails.filter((i) => i.SELECTED)});
    }

    checkAll(checked : boolean) {

        this.invoiceAllChecked = checked;
        this.invoiceDetails.forEach(i => this.check(i, checked));
    }

    ngAfterViewInit(): void {
        this.dtTrigger.next();
    }

    ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
    }

    indexTrackBy(index: number, invoice: CustomerInvoiceData) {
        return invoice.ID;
    }
    
    onChangeAmountToPay(invoice : CustomerInvoiceData) {
        invoice.AMOUNT_TO_PAY = (invoice.AMOUNT_TO_PAY) ? invoice.AMOUNT_TO_PAY : '0';
        this.sumTotalAmountToPay();       
    }

    sumTotalAmountToPay() {
        this.totalAmountToPay = 0;
        this.invoiceDetails.forEach(i => this.totalAmountToPay = Math.round((this.totalAmountToPay * 100) + (parseFloat(i.AMOUNT_TO_PAY) * 100)) / 100)
    }

}