import { Component  } from '@angular/core';
import {  CustomerOrder, PaymentMethod } from '../../../_models';
import { Subject } from 'rxjs';
import { CustomerInvoice } from '../../../_models/customer-invoice';
import { CustomerInvoiceData } from '../../../_models/customer-invoice-data';

@Component({
  selector: 'app-invoice-payment-panel',
  templateUrl: './invoice-payment-panel.component.html',
  styleUrls: ['./invoice-payment-panel.component.css']
})

export class InvoicePaymentPanelComponent {
    
    customer : CustomerOrder;
    paymentMethod : PaymentMethod;
    selectedInvoices : CustomerInvoiceData[] = [];
    minSelectError : boolean;

    private eventsSubject: Subject<void> = new Subject<void>();
    private paymentMethodSubject: Subject<PaymentMethod> = new Subject<PaymentMethod>();

    CUSTOMERID : string;
    BILLTO_FIRSTNAME : string;
    BILLTO_LASTNAME : string;
    BILLTO_EMAIL : string;

    constructor() {
    }

    emitRemovePaymentMethod() {
        this.eventsSubject.next();
    }

    customerSelected(customer : CustomerInvoice){

        this.CUSTOMERID = customer.CUSTOMERID;
        this.BILLTO_FIRSTNAME = customer.BILLTO_FIRSTNAME;
        this.BILLTO_LASTNAME = customer.BILLTO_LASTNAME;
        this.BILLTO_EMAIL = customer.BILLTO_EMAIL;

        this.customer = customer;
        this.emitRemovePaymentMethod();
    }

    setPaymentMethod(paymentMethod) {

        this.paymentMethod = paymentMethod;
      
        if (this.paymentMethod && !this.selectedInvoices.length){

            this.emitRemovePaymentMethod();
            this.minSelectError = true;
        } else {
            
            this.paymentMethodSubject.next(this.paymentMethod);
        }
    }
    
    setSelectedInvoices(obj : { selectedInvoices : CustomerInvoiceData[], ignoreMinSelectError ?: Boolean}){

        this.selectedInvoices = obj.selectedInvoices;
        if (!obj.ignoreMinSelectError) {

            if (this.paymentMethod && !this.selectedInvoices.length){
                this.minSelectError = true;
            } else {
                this.minSelectError = false;
            }
        }

        this.emitRemovePaymentMethod();
    }
}