import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReasonCodesListComponent } from './list/reason-codes-list.component';
const routes: Routes = [
  {
    path: '', component: ReasonCodesListComponent
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReasonCodesRoutingModule { }
