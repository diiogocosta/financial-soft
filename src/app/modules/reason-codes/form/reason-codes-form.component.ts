import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ReasonCodeService } from '../../../_services';
import { ReasonCode, Provider } from '../../../_models';
import { TemplateComponent } from '../../_shared/template.component';

@Component({
    selector: 'reason-code-form',
    templateUrl: './reason-codes-form.component.html',
    styleUrls: ['./reason-codes-form.component.css']
})

export class ReasonCodesFormComponent extends TemplateComponent {    
    providers: Provider[];
    reasonCode: ReasonCode = new ReasonCode();        

    constructor(private reasonCodeService: ReasonCodeService, injector: Injector) { 
        super(injector);    
    }

    ngOnInit() {
        this.initForm([]);
    }            

    initForm(providers: Provider[],id = null){     
        this.id = id;    
        this.providers = providers;

        this.form = this.formBuilder.group({                
                CODE: ['',Validators.required],
                DESCRIPTION: ['',Validators.required],
                PROVIDER_ID: ['',Validators.required],
                INTERNAL_PROV_REASON_CODE_ID: ['', Validators.required],                
        });         

        if (id){                
            this.reasonCodeService.get(id).subscribe((res: ReasonCode[])=>{                                                
                this.reasonCode = res.shift();                
                delete this.reasonCode.ID;
                this.form.patchValue(this.reasonCode);                  
            })
            this.save = this.reasonCodeService.update.bind(this.reasonCodeService);            
        } else {
            this.save = this.reasonCodeService.create.bind(this.reasonCodeService);            
        }    
        
        this.form.markAsUntouched();
    }
    
    doSave() {
        Object.assign(this.reasonCode,this.form.value);
                       
        if (this.validate()) return 

        this.save(this.id,this.reasonCode)
        .subscribe(
            data => {                
                this.toastr.success('Reason code saved!','Success')    
                this.submit.emit();
            },
            error => {                              
                console.log(error);
                this.toastr.error('Attention, make sure your record has been saved.','Danger')
            }
        );
    }    

    title(){
        if (!this.id)
            return 'New'
        else 
            return 'Edit'
    }
}