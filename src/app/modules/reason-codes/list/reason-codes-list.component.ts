import { Component, OnInit, OnDestroy, ViewChild, ElementRef  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { ReasonCode, Provider } from '../../../_models';
import { ReasonCodeService, ProviderService } from '../../../_services';
import { ReasonCodesFormComponent } from '../form/reason-codes-form.component';

declare var UIkit: any;

@Component({
  selector: 'app-reason-codes-list',
  templateUrl: './reason-codes-list.component.html'
})

export class ReasonCodesListComponent implements OnDestroy, OnInit {    
    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('modalComponent') modalForm: ElementRef;
    
    dtOptions: DataTables.Settings = {};
    reasonCodes: ReasonCode[] = [];
    providers: Provider[] = [];
    dtTrigger= new Subject();
    modal = false;

    constructor(private reasonCodeService: ReasonCodeService, private providerService: ProviderService) {}
    
    ngOnInit() {
        this.load();
    }
    
    private load() {
        const that = this;

        this.reasonCodeService.get()
        .subscribe(
            data => {
                that.reasonCodes = data;    
                this.providerService.get().subscribe(providers=>{
                    this.providers = providers;
                    that.reasonCodes.map(reasonCode=>{
                        for (let i=0; i < providers.length; i++){
                            if (reasonCode.PROVIDER_ID == providers[i].ID)   
                                reasonCode.PROVIDER = providers[i].NAME;                                                        
                        }
                        return reasonCode;                                                                                                            
                    })
                    console.log(that.reasonCodes);
                })
                this.rerender();            
            },
            error => {
                console.log(error);
            }
        );
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
          dtInstance.destroy();          
          this.dtTrigger.next();
        });
      }

    ngOnDestroy(): void {                        
        this.dtTrigger.unsubscribe();
    }

    ngAfterViewInit(){
        this.dtTrigger.next();                
    }

    delete(id){

        this.reasonCodeService.delete(id)
        .subscribe(
            data => {                                
                this.load();
            },
            error => {        
                console.log(error);
            }
        );
    }

    createNew(form: ReasonCodesFormComponent){
        form.initForm(this.providers);        
        this.showModal();
    }

    edit(form: ReasonCodesFormComponent,id){
        form.initForm(this.providers,id);
        this.showModal();
    }    

    showModal(){
        UIkit.modal(document.getElementById('reason-code-form')).show();        
    }

    closeModal(){                
        UIkit.modal(document.getElementById('reason-code-form')).hide();
        this.load();
    }    
}