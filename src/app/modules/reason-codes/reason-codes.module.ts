import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReasonCodesRoutingModule } from './reason-codes-routing.module';

import { ReasonCodesListComponent } from './list/reason-codes-list.component';
import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { ReasonCodesFormComponent } from './form/reason-codes-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReasonCodeService, ProviderService } from '../../_services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    ReasonCodesRoutingModule,
    SharedModule    
  ],
  declarations: [ReasonCodesListComponent, ReasonCodesFormComponent],
  providers: [ReasonCodeService, ProviderService]
})
export class ReasonCodesModule { }
