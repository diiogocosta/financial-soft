import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardsRoutingModule } from './cards-routing.module';

import { CardListComponent } from './list/cards-list.component';
import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { CardFormComponent } from './form/cards-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProviderService, CardService } from '../../_services';
import { NgxCurrencyModule } from "ngx-currency";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    NgxCurrencyModule,
    CardsRoutingModule,
    SharedModule    
  ],
  declarations: [CardListComponent, CardFormComponent],
  providers: [CardService, ProviderService]
})
export class CardModule { }
