import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Card, Provider } from '../../../_models';
import { TemplateComponent } from '../../_shared/template.component';
import { CardService } from '../../../_services/index';

@Component({
    selector: 'card-form',
    templateUrl: './cards-form.component.html',
    styleUrls: ['./cards-form.component.css']
})

export class CardFormComponent extends TemplateComponent {
    providers: Provider[];
    installments = Array.from(Array(60).keys());
    card: Card = new Card();

    currencyMask = {
        prefix: '',
        thousands: '.',
        decimal: ',',
        align: 'left',
    }

    constructor(private cardService: CardService, injector: Injector) {
        super(injector);
    }

    ngOnInit() {
        this.initForm([]);
    }

    initForm(providers: Provider[], id = null) {
        this.id = id;
        this.providers = providers;        

        this.form = this.formBuilder.group({
            PROVIDER_ID: ['', Validators.required],
            CARD: ['', Validators.required],
            ACTIVE: ['Y', Validators.required],
            TYPE: ['', Validators.required],
            PROVIDER_CODE: ['', Validators.required],
            REGEX_VALID_CARD: ['', Validators.required],
            PRE_AUTH_AMOUNT: [''],
            AUTH_REVERS_DAYS: [''],
            INSTALLMENT_TYPE: [''],
            INSTALLMENT: ['']
        });

        if (id) {
            this.cardService.get(id).subscribe((res: Card[]) => {
                this.card = res.shift();
                delete this.card.ID;
                this.form.patchValue(this.card);
            })
            this.save = this.cardService.update.bind(this.cardService);
        } else {
            this.save = this.cardService.create.bind(this.cardService);
        }

        this.form.markAsUntouched();
    }

    doSave() {
        Object.assign(this.card, this.form.value);

        if (this.validate()) return

        this.save(this.id, this.card)
            .subscribe(
                data => {
                    this.toastr.success('Card saved!', 'Success')
                    this.submit.emit();
                },
                error => {
                    this.toastr.error('Attention, make sure your record has been saved.', 'Danger')
                }
            );
    }

    title() {
        if (!this.id)
            return 'New'
        else
            return 'Edit'
    }

    getInstallmentLabel(value) {
        if (value < 10)
            return '0'+value;
        else 
            return value;
        
    }

    strValue(value: number){
        return value.toString();
    }
}