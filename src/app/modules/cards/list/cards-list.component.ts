import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { Card, Provider } from '../../../_models';
import { ProviderService, CardService } from '../../../_services';
import { CardFormComponent } from '../form/cards-form.component';

declare var UIkit: any;

@Component({
  selector: 'app-card-list',
  templateUrl: './cards-list.component.html'
})

export class CardListComponent implements OnDestroy, OnInit {    
    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('modalComponent') modalForm: ElementRef;
    
    dtOptions: DataTables.Settings = {};
    cards: Card[] = [];    
    providers: Provider[] = [];
    dtTrigger= new Subject();
    modal = false;

    constructor(private cardService: CardService, private providerService: ProviderService) {}
    
    ngOnInit() {
        this.load();
    }
    
    private load() {
        const that = this;

        this.cardService.get()
        .subscribe(
            data => {                
                that.cards = data;                    
                this.providerService.get().subscribe(providers=>{
                    this.providers = providers;
                    that.cards.map(card=>{
                        for (let i=0; i < providers.length; i++){
                            if (card.PROVIDER_ID == providers[i].ID)   
                                card.PROVIDER = providers[i].NAME;                                                        
                        }
                        return card;                                                                                                            
                    })                    
                })                
                this.rerender();            
            },
            error => {
                console.log(error);
            }
        );
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
          dtInstance.destroy();          
          this.dtTrigger.next();
        });
      }

    ngOnDestroy(): void {                        
        this.dtTrigger.unsubscribe();
    }

    ngAfterViewInit(){
        this.dtTrigger.next();                
    }

    delete(id){

        this.cardService.delete(id)
        .subscribe(
            data => {                                
                this.load();
            },
            error => {        
                console.log(error);
            }
        );
    }

    createNew(form: CardFormComponent){
        form.initForm(this.providers);        
        this.showModal();
    }

    edit(form: CardFormComponent,id){
        form.initForm(this.providers,id);
        this.showModal();
    }    

    showModal(){
        UIkit.modal(document.getElementById('card-form')).show();        
    }

    closeModal(){                
        UIkit.modal(document.getElementById('card-form')).hide();
        this.load();
    }    
}