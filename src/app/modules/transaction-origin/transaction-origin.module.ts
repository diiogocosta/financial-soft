import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionOriginRoutingModule } from './transaction-origin-routing.module';

import { TransactionOriginListComponent } from './list/transaction-origin-list.component';
import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { TransactionOriginFormComponent } from './form/transaction-origin-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransactionOriginService } from '../../_services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    TransactionOriginRoutingModule,
    SharedModule    
  ],
  declarations: [TransactionOriginListComponent, TransactionOriginFormComponent],
  providers: [TransactionOriginService]
})
export class TransactionOriginModule { }
