import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TransactionOrigin } from '../../../_models';
import { TemplateComponent } from '../../_shared/template.component';
import { TransactionOriginService } from '../../../_services/transaction-origin.service';

@Component({
    selector: 'transaction-origin-form',
    templateUrl: './transaction-origin-form.component.html',
    styleUrls: ['./transaction-origin-form.component.css']
})

export class TransactionOriginFormComponent extends TemplateComponent {    
    transactionOrigin: TransactionOrigin = new TransactionOrigin();        

    constructor(private transactionOriginService: TransactionOriginService, injector: Injector) { 
        super(injector);    
    }

    ngOnInit() {
        this.initForm();
    }            

    initForm(id=null){     
        this.id = id;            
        
        this.form = this.formBuilder.group({                
                TXN_ORIGIN: ['',Validators.required],
                DESCRIPTION: ['',Validators.required],                                
        });         

        if (id){                
            this.transactionOriginService.get(id).subscribe((res: TransactionOrigin[])=>{                                                
                this.transactionOrigin = res.shift();                
                delete this.transactionOrigin.ID;
                this.form.patchValue(this.transactionOrigin);                  
            })
            this.save = this.transactionOriginService.update.bind(this.transactionOriginService);            
        } else {
            this.save = this.transactionOriginService.create.bind(this.transactionOriginService);            
        }    
        
        this.form.markAsUntouched();
    }
    
    doSave() {
        Object.assign(this.transactionOrigin,this.form.value);
                       
        if (this.validate()) return 

        this.save(this.id,this.transactionOrigin)
        .subscribe(
            data => {                
                this.toastr.success('Transaction origin saved!','Success')    
                this.submit.emit();
            },
            error => {                                              
                this.toastr.error('Attention, make sure your record has been saved.','Danger')
            }
        );
    }    

    title(){
        if (!this.id)
            return 'New'
        else 
            return 'Edit'
    }
}