import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransactionOriginListComponent } from './list/transaction-origin-list.component';
const routes: Routes = [
  {
    path: '', component: TransactionOriginListComponent
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionOriginRoutingModule { }
