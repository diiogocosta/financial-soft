import { Component, OnInit, OnDestroy, ViewChild, ElementRef  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { TransactionOrigin } from '../../../_models';
import { TransactionOriginService } from '../../../_services';
import { TransactionOriginFormComponent } from '../form/transaction-origin-form.component';

declare var UIkit: any;

@Component({
  selector: 'app-transaction-origin-list',
  templateUrl: './transaction-origin-list.component.html'
})

export class TransactionOriginListComponent implements OnDestroy, OnInit {    
    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('modalComponent') modalForm: ElementRef;
    
    dtOptions: DataTables.Settings = {};
    transactionsOrigin: TransactionOrigin[] = [];    
    dtTrigger= new Subject();
    modal = false;

    constructor(private transactionOriginService: TransactionOriginService,) {}
    
    ngOnInit() {
        this.load();
    }
    
    private load() {
        const that = this;

        this.transactionOriginService.get()
        .subscribe(
            data => {                
                that.transactionsOrigin = data;                    
                this.rerender();            
            },
            error => {
                console.log(error);
            }
        );
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
          dtInstance.destroy();          
          this.dtTrigger.next();
        });
      }

    ngOnDestroy(): void {                        
        this.dtTrigger.unsubscribe();
    }

    ngAfterViewInit(){
        this.dtTrigger.next();                
    }

    delete(id){

        this.transactionOriginService.delete(id)
        .subscribe(
            data => {                                
                this.load();
            },
            error => {        
                console.log(error);
            }
        );
    }

    createNew(form: TransactionOriginFormComponent){
        form.initForm();        
        this.showModal();
    }

    edit(form: TransactionOriginFormComponent,id){
        form.initForm(id);
        this.showModal();
    }    

    showModal(){
        UIkit.modal(document.getElementById('transaction-origin-form')).show();        
    }

    closeModal(){                
        UIkit.modal(document.getElementById('transaction-origin-form')).hide();
        this.load();
    }    
}