import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TransactionTypeService } from '../../../_services';
import { TransactionType, Provider } from '../../../_models';
import { TemplateComponent } from '../../_shared/template.component';

@Component({
    selector: 'transaction-type-form',
    templateUrl: './transaction-types-form.component.html',
    styleUrls: ['./transaction-types-form.component.css']
})

export class TransactionTypesFormComponent extends TemplateComponent {    
    providers: Provider[];
    transactionType: TransactionType = new TransactionType();        

    constructor(private transactionTypeService: TransactionTypeService, injector: Injector) { 
        super(injector);    
    }

    ngOnInit() {
        this.initForm([]);
    }            

    initForm(providers: Provider[],id = null){     
        this.id = id;    
        this.providers = providers;
        
        this.form = this.formBuilder.group({                
                PROVIDER_ID: ['',Validators.required],
                TXN_NAME: ['',Validators.required],
                DISPLAY_NAME: ['',Validators.required],
                TYPE: ['', Validators.required],                
                DISPLAY_FLG: ['Y'],                
        });         

        if (id){                
            this.transactionTypeService.get(id).subscribe((res: TransactionType[])=>{                                                
                this.transactionType = res.shift();                
                delete this.transactionType.ID;
                this.form.patchValue(this.transactionType);                  
            })
            this.save = this.transactionTypeService.update.bind(this.transactionTypeService);            
        } else {
            this.save = this.transactionTypeService.create.bind(this.transactionTypeService);            
        }    
        
        this.form.markAsUntouched();
    }
    
    doSave() {
        Object.assign(this.transactionType,this.form.value);
                       
        if (this.validate()) return 

        this.save(this.id,this.transactionType)
        .subscribe(
            data => {                
                this.toastr.success('Transaction type saved!','Success')    
                this.submit.emit();
            },
            error => {                                              
                this.toastr.error('Attention, make sure your record has been saved.','Danger')
            }
        );
    }    

    title(){
        if (!this.id)
            return 'New'
        else 
            return 'Edit'
    }
}