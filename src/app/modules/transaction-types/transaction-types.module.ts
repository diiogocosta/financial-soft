import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionTypesRoutingModule } from './transaction-types-routing.module';

import { TransactionTypesListComponent } from './list/transaction-types-list.component';
import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { TransactionTypesFormComponent } from './form/transaction-types-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProviderService, TransactionTypeService } from '../../_services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    TransactionTypesRoutingModule,
    SharedModule    
  ],
  declarations: [TransactionTypesListComponent, TransactionTypesFormComponent],
  providers: [TransactionTypeService, ProviderService]
})
export class TransactionTypesModule { }
