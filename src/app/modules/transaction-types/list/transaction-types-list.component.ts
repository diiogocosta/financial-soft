import { Component, OnInit, OnDestroy, ViewChild, ElementRef  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { Provider, TransactionType } from '../../../_models';
import { TransactionTypeService, ProviderService } from '../../../_services';
import { TransactionTypesFormComponent } from '../form/transaction-types-form.component';

declare var UIkit: any;

@Component({
  selector: 'app-transaction-types-list',
  templateUrl: './transaction-types-list.component.html'
})

export class TransactionTypesListComponent implements OnDestroy, OnInit {    
    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('modalComponent') modalForm: ElementRef;
    
    dtOptions: DataTables.Settings = {};
    transactionTypes: TransactionType[] = [];
    providers: Provider[] = [];
    dtTrigger= new Subject();
    modal = false;

    constructor(private transactionTypeService: TransactionTypeService, private providerService: ProviderService) {}
    
    ngOnInit() {
        this.load();
    }
    
    private load() {
        const that = this;

        this.transactionTypeService.get()
        .subscribe(
            data => {
                that.transactionTypes = data;    
                this.providerService.get().subscribe(providers=>{
                    this.providers = providers;
                    that.transactionTypes.map(transactionType=>{
                        for (let i=0; i < providers.length; i++){
                            if (transactionType.PROVIDER_ID == providers[i].ID)   
                                transactionType.PROVIDER = providers[i].NAME;                                                        
                        }
                        return transactionType;                                                                                                            
                    })                    
                })
                this.rerender();            
            },
            error => {
                console.log(error);
            }
        );
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
          dtInstance.destroy();          
          this.dtTrigger.next();
        });
      }

    ngOnDestroy(): void {                        
        this.dtTrigger.unsubscribe();
    }

    ngAfterViewInit(){
        this.dtTrigger.next();                
    }

    delete(id){

        this.transactionTypeService.delete(id)
        .subscribe(
            data => {                                
                this.load();
            },
            error => {        
                console.log(error);
            }
        );
    }

    createNew(form: TransactionTypesFormComponent){
        form.initForm(this.providers);        
        this.showModal();
    }

    edit(form: TransactionTypesFormComponent,id){
        form.initForm(this.providers,id);
        this.showModal();
    }    

    showModal(){
        UIkit.modal(document.getElementById('transaction-type-form')).show();        
    }

    closeModal(){                
        UIkit.modal(document.getElementById('transaction-type-form')).hide();
        this.load();
    }    
}