import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';

import { AccountListComponent } from './list/account-list.component';
import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { AccountFormComponent } from './form/account-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountService } from '../../_services';
import { TranslatePipe } from '../../_translate/translate.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    AccountRoutingModule,
    SharedModule
  ],
  declarations: [AccountListComponent, AccountFormComponent],
  providers: [AccountService]
})
export class AccountModule { }
