import { Component, OnInit, OnDestroy, ViewChild, ElementRef  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { Account } from '../../../_models';
import { AccountService } from '../../../_services';
import { AccountFormComponent } from '../form/account-form.component';

declare var UIkit: any;

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html'
})

export class AccountListComponent implements OnDestroy, OnInit {    
    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('modalComponent') modalForm: ElementRef;
    
    dtOptions: DataTables.Settings = {};
    accounts: Account[] = [];
    dtTrigger= new Subject();
    modal = false;

    constructor(private accountService: AccountService) {}
    
    ngOnInit() {
        this.loadAllProviders();
    }
    
    private loadAllProviders() {
        const that = this;

        this.accountService.get()
        .subscribe(
            data => {
                that.accounts = data;    
                this.rerender();            
            },
            error => {
                console.log(error);
            }
        );
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
          dtInstance.destroy();          
          this.dtTrigger.next();
        });
      }

    ngOnDestroy(): void {                        
        this.dtTrigger.unsubscribe();
    }

    ngAfterViewInit(){
        this.dtTrigger.next();                
    }

    removeAccount(id){

        this.accountService.delete(id)
        .subscribe(
            data => {                                
                this.loadAllProviders();
            },
            error => {        
                console.log(error);
            }
        );
    }

    newAccount(form: AccountFormComponent){
        form.initForm();        
        this.showModal();
    }

    editAccount(form: AccountFormComponent,id){
        form.initForm(id);
        this.showModal();
    }    

    showModal(){
        UIkit.modal(document.getElementById('account-form')).show();        
    }

    closeModal(){                
        UIkit.modal(document.getElementById('account-form')).hide();
        this.loadAllProviders();
    }    
}