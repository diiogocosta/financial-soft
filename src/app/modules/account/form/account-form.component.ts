import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountService } from '../../../_services';
import { Account } from '../../../_models';
import { TemplateComponent } from '../../_shared/template.component';

@Component({
    selector: 'account-form',
    templateUrl: './account-form.component.html',
    styleUrls: ['./account-form.component.css']
})

export class AccountFormComponent extends TemplateComponent {    

    account: Account = new Account();        

    constructor(private accountService: AccountService, injector: Injector) { 
        super(injector);    
    }

    ngOnInit() {
        this.initForm();
    }            

    initForm(id = null){     
        this.id = id;   
        this.form = this.formBuilder.group({                
                NAME: ['',Validators.required],
                ADDRESS: ['',Validators.required],
                CITY: ['',Validators.required],
                STATE: ['',[Validators.required, Validators.minLength(2)]],
                COUNTY: [''],
                COUNTRY: ['',Validators.required],
                POSTALCODE: ['',Validators.required],
                EMAIL: ['',[Validators.required,Validators.email]],            
        });

        if (id){                
            this.accountService.get(id).subscribe((res: Account[])=>{                                
                this.account = res.shift();                
                delete this.account.ID;
                this.form.patchValue(this.account);                  
            })
            this.save = this.accountService.update.bind(this.accountService);            
        } else {
            this.save = this.accountService.create.bind(this.accountService);            
        }    
        
        this.form.markAsUntouched();
    }
    
    doSave() {
        Object.assign(this.account,this.form.value);
                       
        if (this.validate()) return 

        this.save(this.id,this.account)
        .subscribe(
            data => {                
                this.toastr.success('Account saved!','Success')    
                this.submit.emit();
            },
            error => {                              
                console.log(error);
                this.toastr.error('Attention, make sure your record has been saved.','Danger')
            }
        );
    }    
}