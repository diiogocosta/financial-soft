import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Injector  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { Settings } from '../../_models';
import { SettingsService } from '../../_services';
import { TemplateComponent } from '../_shared/template.component';
import { RichEditorComponent } from '../../_components/rich-editor/rich-editor.component';

declare var UIkit: any;

@Component({
  selector: 'settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent extends TemplateComponent implements OnInit {                    
    @ViewChild('editor') editor: RichEditorComponent;
    settings : Settings;
    settingsService : SettingsService;
    
    constructor(injector: Injector) {
        super(injector);
        this.settingsService = injector.get(SettingsService);
    }
    
    ngOnInit() {        
        this.form = this.formBuilder.group({                
            FTP_INVOICE_FOLDER: [''],
            FTP_ORDER_FOLDER: [''],
            URL_REST: [''],            
            MAINTENANCE_MODE: ['Y'],            
        });
        this.loadService();        
        this.save = this.settingsService.update.bind(this.settingsService);            
    }
    
    private loadService() {              
        this.settingsService.get()
        .subscribe(
            data => {                               
                this.settings = data.shift();    
                this.id = this.settings.ID;                                 
                this.form.patchValue(this.settings);                        
                this.editor.init(this.settings.MAINTENANCE_DESCRIPTION);
            },
            error => {
                console.log(error);
            }
        );
    }

    changeValue(value){        
        this.settings.MAINTENANCE_DESCRIPTION = value;
    }
    
    doSave() {
        Object.assign(this.settings,this.form.value);     
        delete this.settings.ID;                     
                
        this.save(this.id,this.settings)
        .subscribe(
            data => {
                this.toastr.success('Settings saved!','Success')    
                this.loadService();
            },
            error => {                                              
                this.loadService();
                this.toastr.error('Attention, make sure your record has been saved.','Danger')
            }
        );
    }        
}