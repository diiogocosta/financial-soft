import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Bank, Provider } from '../../../_models';
import { TemplateComponent } from '../../_shared/template.component';
import { BankService } from '../../../_services/bank.service';

@Component({
    selector: 'bank-form',
    templateUrl: './banks-form.component.html',
    styleUrls: ['./banks-form.component.css']
})

export class BankFormComponent extends TemplateComponent {    
    providers: Provider[];
    bank: Bank = new Bank();        

    constructor(private bankService: BankService, injector: Injector) { 
        super(injector);    
    }

    ngOnInit() {
        this.initForm([]);
    }            

    initForm(providers: Provider[],id = null){     
        this.id = id;            
        this.providers = providers;
        
        this.form = this.formBuilder.group({                
                PROVIDER_ID: ['',Validators.required],
                BANK: ['',Validators.required],                                
                ACTIVE: ['Y',Validators.required],                                
                TYPE: ['',Validators.required],                                
                PROVIDER_CODE: ['',Validators.required],                                
        });         

        if (id){                
            this.bankService.get(id).subscribe((res: Bank[])=>{                                                
                this.bank = res.shift();                
                delete this.bank.ID;
                this.form.patchValue(this.bank);                  
            })
            this.save = this.bankService.update.bind(this.bankService);            
        } else {
            this.save = this.bankService.create.bind(this.bankService);            
        }    
        
        this.form.markAsUntouched();
    }
    
    doSave() {
        Object.assign(this.bank,this.form.value);
                       
        if (this.validate()) return 

        this.save(this.id,this.bank)
        .subscribe(
            data => {                
                this.toastr.success(this.translate('registerSaved'),this.translate('success'))    
                this.submit.emit();
            },
            error => {                                              
                this.toastr.error(this.translate('makeSureAllRecordSaved'),this.translate('danger'))
            }
        );
    }    

    title(){
        if (!this.id)
            return 'new'
        else 
            return 'edit'
    }
}