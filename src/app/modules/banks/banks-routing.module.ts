import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BankListComponent } from './list/banks-list.component';
const routes: Routes = [
  {
    path: '', component: BankListComponent
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BanksRoutingModule { }
