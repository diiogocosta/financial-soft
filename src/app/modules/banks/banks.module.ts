import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BanksRoutingModule } from './banks-routing.module';

import { BankListComponent } from './list/banks-list.component';
import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { BankFormComponent } from './form/banks-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BankService, ProviderService } from '../../_services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    BanksRoutingModule,
    SharedModule    
  ],
  declarations: [BankListComponent, BankFormComponent],
  providers: [BankService, ProviderService]
})
export class BankModule { }
