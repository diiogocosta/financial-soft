import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { Bank, Provider } from '../../../_models';
import { ProviderService, BankService } from '../../../_services';
import { BankFormComponent } from '../form/banks-form.component';

declare var UIkit: any;

@Component({
  selector: 'app-bank-list',
  templateUrl: './banks-list.component.html'
})

export class BankListComponent implements OnDestroy, OnInit {    
    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('modalComponent') modalForm: ElementRef;
    
    dtOptions: DataTables.Settings = {};
    banks: Bank[] = [];    
    providers: Provider[] = [];
    dtTrigger= new Subject();
    modal = false;

    constructor(private bankService: BankService, private providerService: ProviderService) {}
    
    ngOnInit() {
        this.load();
    }
    
    private load() {
        const that = this;

        this.bankService.get()
        .subscribe(
            data => {                
                that.banks = data;                    
                this.providerService.get().subscribe(providers=>{
                    this.providers = providers;
                    that.banks.map(bank=>{
                        for (let i=0; i < providers.length; i++){
                            if (bank.PROVIDER_ID == providers[i].ID)   
                                bank.PROVIDER = providers[i].NAME;                                                        
                        }
                        return bank;                                                                                                            
                    })                    
                })                
                this.rerender();            
            },
            error => {
                console.log(error);
            }
        );
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
          dtInstance.destroy();          
          this.dtTrigger.next();
        });
      }

    ngOnDestroy(): void {                        
        this.dtTrigger.unsubscribe();
    }

    ngAfterViewInit(){
        this.dtTrigger.next();                
    }

    delete(id){

        this.bankService.delete(id)
        .subscribe(
            data => {                                
                this.load();
            },
            error => {        
                console.log(error);
            }
        );
    }

    createNew(form: BankFormComponent){
        form.initForm(this.providers);        
        this.showModal();
    }

    edit(form: BankFormComponent,id){
        form.initForm(this.providers,id);
        this.showModal();
    }    

    showModal(){
        UIkit.modal(document.getElementById('bank-form')).show();        
    }

    closeModal(){                
        UIkit.modal(document.getElementById('bank-form')).hide();
        this.load();
    }    
}