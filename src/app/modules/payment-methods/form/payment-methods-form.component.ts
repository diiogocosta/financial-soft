import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { PaymentMethod } from '../../../_models';
import { TemplateComponent } from '../../_shared/template.component';
import { PaymentMethodService } from '../../../_services';

@Component({
    selector: 'payment-method-form',
    templateUrl: './payment-methods-form.component.html',
    styleUrls: ['./payment-methods-form.component.css']
})

export class PaymentMethodFormComponent extends TemplateComponent {
    paymentMethod: PaymentMethod = new PaymentMethod();

    constructor(private paymentMethodService: PaymentMethodService, injector: Injector) {
        super(injector);
    }

    ngOnInit() {
        this.initForm();
    }

    initForm(id = null) {
        this.id = id;        

        this.form = this.formBuilder.group({
            PAYMENT_METHOD: ['', Validators.required],
            METHOD_TYPE: ['', Validators.required],
            PROVIDER_DECRYPT_FLG: ['', Validators.required]
        });

        if (id) {
            this.paymentMethodService.get(id).subscribe((res: PaymentMethod[]) => {
                this.paymentMethod = res.shift();
                delete this.paymentMethod.ID;
                this.form.patchValue(this.paymentMethod);
            })
            this.save = this.paymentMethodService.update.bind(this.paymentMethodService);
        } else {
            this.save = this.paymentMethodService.create.bind(this.paymentMethodService);
        }

        this.form.markAsUntouched();
    }

    doSave() {
        Object.assign(this.paymentMethod, this.form.value);

        if (this.validate()) return

        this.save(this.id, this.paymentMethod)
            .subscribe(
                data => {
                    this.toastr.success('Payment method saved!', 'Success')
                    this.submit.emit();
                },
                error => {
                    this.toastr.error('Attention, make sure your record has been saved.', 'Danger')
                }
            );
    }

    title() {
        if (!this.id)
            return 'New'
        else
            return 'Edit'
    }
}