import { Component, OnInit, OnDestroy, ViewChild, ElementRef  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { PaymentMethod } from '../../../_models';
import { PaymentMethodService } from '../../../_services';
import { PaymentMethodFormComponent } from '../form/payment-methods-form.component';

declare var UIkit: any;

@Component({
  selector: 'app-payment-method-list',
  templateUrl: './payment-methods-list.component.html'
})

export class PaymentMethodListComponent implements OnDestroy, OnInit {    
    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('modalComponent') modalForm: ElementRef;
    
    dtOptions: DataTables.Settings = {};
    paymentMethods: PaymentMethod[] = [];    
    dtTrigger= new Subject();
    modal = false;

    constructor(private paymentMethodService: PaymentMethodService,) {}
    
    ngOnInit() {
        this.load();
    }
    
    private load() {
        const that = this;

        this.paymentMethodService.get()
        .subscribe(
            data => {                
                that.paymentMethods = data;                    
                this.rerender();            
            },
            error => {
                console.log(error);
            }
        );
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
          dtInstance.destroy();          
          this.dtTrigger.next();
        });
      }

    ngOnDestroy(): void {                        
        this.dtTrigger.unsubscribe();
    }

    ngAfterViewInit(){
        this.dtTrigger.next();                
    }

    delete(id){

        this.paymentMethodService.delete(id)
        .subscribe(
            data => {                                
                this.load();
            },
            error => {        
                console.log(error);
            }
        );
    }

    createNew(form: PaymentMethodFormComponent){
        form.initForm();        
        this.showModal();
    }

    edit(form: PaymentMethodFormComponent,id){
        form.initForm(id);
        this.showModal();
    }    

    showModal(){
        UIkit.modal(document.getElementById('payment-method-form')).show();        
    }

    closeModal(){                
        UIkit.modal(document.getElementById('payment-method-form')).hide();
        this.load();
    }    
}