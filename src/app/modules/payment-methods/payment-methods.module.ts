import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';




import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaymentMethodService } from '../../_services';
import { PaymentMethodRoutingModule } from './payment-methods-routing.module';
import { PaymentMethodListComponent } from './list/payment-methods-list.component';
import { PaymentMethodFormComponent } from './form/payment-methods-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    PaymentMethodRoutingModule,
    SharedModule    
  ],
  declarations: [PaymentMethodListComponent, PaymentMethodFormComponent],
  providers: [PaymentMethodService]
})
export class PaymentMethodModule { }
