import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProviderListComponent } from './provider-list/provider-list.component';
import { ProviderCreateComponent } from './provider-create/provider-create.component';
import { ProviderEditComponent } from './provider-edit/provider-edit.component';
import { AuthGuard } from '../../_guards';

const routes: Routes = [
  {
    path: '', component: ProviderListComponent
  },
  {
    path: 'list', component: ProviderListComponent, canActivate: [AuthGuard]
  },
  {
    path: 'create', component: ProviderCreateComponent, canActivate: [AuthGuard]
  },
  {
    path: 'edit/:id', component: ProviderEditComponent, canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderRoutingModule { }
