import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ProviderService } from '../../../_services';
import { Provider } from '../../../_models';

@Component({
  selector: 'app-provider-edit',
  templateUrl: './provider-edit.component.html'
})
export class ProviderEditComponent implements OnInit {
    providerForm: FormGroup;
    provider: Provider = new Provider();

    constructor(private formBuilder: FormBuilder, private providerService: ProviderService, private route: ActivatedRoute, private router: Router) { 

        this.provider.ID =this.route.snapshot.params.id;
        this.providerService.get(this.provider.ID)
        .subscribe(
            data => {
                this.provider = data[0];
                this.ngOnInit();
            },
            error => {
                console.log(error);
            }
        );
    }

    ngOnInit() {
        this.providerForm = this.formBuilder.group({
            NAME: [this.provider.NAME],
            PROVIDER_CODE: [this.provider.PROVIDER_CODE],
            ACTIVE: [this.provider.ACTIVE],
            PRD_FLAG: [this.provider.PRD_FLAG],
            ACCOUNT_UPDATE_FLAG: [this.provider.ACCOUNT_UPDATE_FLAG],
            TOKENIZATION_FLG: [this.provider.TOKENIZATION_FLG],
            COUNTRY_ISO_CODE: [this.provider.COUNTRY_ISO_CODE],
            DECIMAL_SEPARATOR: [this.provider.DECIMAL_SEPARATOR],
            RECONC_PURCHASE_REFUND_URL: [this.provider.RECONC_PURCHASE_REFUND_URL],
        });
    }

    f() { return this.providerForm.controls; }

    updateProp(prop, val){
        this.provider[prop] = val;
    }

    onSubmit() {

        var providerUpdate = new Provider();
        providerUpdate.NAME = this.provider.NAME;
        providerUpdate.PROVIDER_CODE= this.provider.PROVIDER_CODE;
        providerUpdate.ACTIVE= this.provider.ACTIVE;
        providerUpdate.PRD_FLAG= this.provider.PRD_FLAG;
        providerUpdate.ACCOUNT_UPDATE_FLAG= this.provider.ACCOUNT_UPDATE_FLAG;
        providerUpdate.TOKENIZATION_FLG= this.provider.TOKENIZATION_FLG;
        providerUpdate.COUNTRY_ISO_CODE= this.provider.COUNTRY_ISO_CODE;
        providerUpdate.DECIMAL_SEPARATOR= this.provider.DECIMAL_SEPARATOR;
        providerUpdate.RECONC_PURCHASE_REFUND_URL= this.provider.RECONC_PURCHASE_REFUND_URL;

        console.log(providerUpdate);
        this.providerService.update(this.provider.ID, providerUpdate)
        .subscribe(
            data => {
                console.log(data);
                this.router.navigate(['/providers']);
            },
            error => {
                console.log(error);
            }
        );
    }

}
