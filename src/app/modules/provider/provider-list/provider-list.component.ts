import { Component, OnInit, OnDestroy, ViewChild  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { Provider } from '../../../_models';
import { ProviderService } from '../../../_services';


@Component({
  selector: 'app-provider-list',
  templateUrl: './provider-list.component.html'
})

export class ProviderListComponent implements OnDestroy, OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    
    dtOptions: DataTables.Settings = {};
    providers: Provider[] = [];
    dtTrigger= new Subject();

    constructor(private providerService: ProviderService) {}
    ngOnInit() {
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 10
        };
        this.loadAllProviders();
    }
    
    private loadAllProviders() {
        const that = this;

        this.providerService.get()
        .subscribe(
            data => {
                that.providers = data;
                this.rerender();
            },
            error => {
                console.log(error);
            }
        );
    }

    rerender(): void {
        this.dtTrigger.next();
    }

    ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
    }

    removeProvider(id){

        this.providerService.delete(id)
        .subscribe(
            data => {
                this.ngOnDestroy();
                this.loadAllProviders();
            },
            error => {
                console.log(error);
            }
        );
    }
}