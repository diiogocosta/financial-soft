import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ProviderService } from '../../../_services';
import { Provider } from '../../../_models';

@Component({
    selector: 'app-provider-create',
    templateUrl: './provider-create.component.html'
})

export class ProviderCreateComponent implements OnInit {
    providerForm: FormGroup;
    provider: Provider = new Provider();

    constructor(private formBuilder: FormBuilder, private providerService: ProviderService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
        this.providerForm = this.formBuilder.group({
            NAME: [''],
            CODE: [''],
            ACTIVE: [''],
            PRD_FLAG: [''],
            ACCOUNT_UPDATE_FLAG: [''],
            TOKENIZATION_FLG: [''],
            PROVIDER_CODE: [''],
            COUNTRY_ISO_CODE: [''],
            DECIMAL_SEPARATOR: ['']
        });
    }
    
    f() { return this.providerForm.controls; }

    updateProp(prop, val){
        this.provider[prop] = val;
    }
    onSubmit() {

        this.providerService.create(this.provider)
        .subscribe(
            data => {
                this.router.navigate(['/providers']);
            },
            error => {
                console.log(error);
            }
        );

    }
}