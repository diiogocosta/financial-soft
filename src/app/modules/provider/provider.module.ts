import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProviderRoutingModule } from './provider-routing.module';
import { ProviderListComponent } from './provider-list/provider-list.component';
import { DataTablesModule } from 'angular-datatables';
import { ProviderCreateComponent } from './provider-create/provider-create.component';
import { ProviderService } from '../../_services';
import { ProviderEditComponent } from './provider-edit/provider-edit.component';

import { SharedModule } from '../_shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ProviderRoutingModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    ProviderService
  ],
  declarations: [ProviderListComponent, ProviderCreateComponent, ProviderEditComponent ]
})
export class ProviderModule {  }
