import { OnInit, Injector, Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '../../_translate/translate.service';

@Component({
    template: '',
    styles: [`::ng-deep .mat-form-field-underline{        
                background-color: rgb(225, 225, 225) !important;
            }`]
})
export class TemplateComponent implements OnInit {    
    @Output() submit : EventEmitter<any> = new EventEmitter;
    formBuilder: FormBuilder;
    route : ActivatedRoute;
    router : Router;
    toastr : ToastrService;
    translateService : TranslateService;
    form: FormGroup;    
    id;
    save;

    constructor(injector: Injector) {
        this.formBuilder = injector.get(FormBuilder);
        this.route = injector.get(ActivatedRoute);
        this.router = injector.get(Router);
        this.toastr = injector.get(ToastrService);
        this.translateService = injector.get(TranslateService)
    }

    ngOnInit() {        
        this.id = this.route.snapshot.params.id;                    
    }    
    
    f(name) { return this.form.controls[name]; }     
    
    getErrorMessage(control: FormControl) {        
        return control.hasError('required') ? 'mustEnterValue' :
               control.hasError('email') ? 'notValidEmail' :
               control.hasError('minlength') ? 'incorrectValue' :
                '';
    }
    
    validate(customForm?: FormGroup){
        let form = this.form;
        if (customForm) { form = customForm}
        for (let control in form.controls) {
           form.controls[control].markAsTouched();
        };

        if (form.invalid){
            this.toastr.error(this.translate('makeSureAllFieldsCorrect'),this.translate('attention'))
        }

        return form.invalid
    }

    translate(key){        
        return this.translateService.data[key];
    }
}