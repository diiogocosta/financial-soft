import { NgModule  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HeadComponent, MenuComponent } from '../../_directives';
import {MatButtonModule, 
        MatCheckboxModule,
        MatInputModule,
        MatDialogModule,
        MatRadioModule,
        MatOptionModule,
        MatSelectModule} from '@angular/material';
import { TemplateComponent } from './template.component';
import { TranslatePipe } from '../../_translate/translate.pipe';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, 
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatInputModule,
    MatDialogModule,
    MatRadioModule,
    MatOptionModule,
    MatSelectModule,
    PerfectScrollbarModule
  ],
  exports: [CommonModule,
            RouterModule,
            ReactiveFormsModule,
            FormsModule,
            MatButtonModule, 
            MatCheckboxModule,
            MatInputModule,
            MatDialogModule,
            MatRadioModule,
            MatOptionModule,
            MatSelectModule,
            PerfectScrollbarModule,
            TemplateComponent,            
            TranslatePipe],            

  declarations: [TemplateComponent, TranslatePipe],
  providers: []
})
export class SharedModule { }