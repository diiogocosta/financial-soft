import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { CurrenciesService } from '../../../_services';
import { Currency } from '../../../_models';
import { TemplateComponent } from '../../_shared/template.component';

@Component({
    selector: 'currency-form',
    templateUrl: './currencies-form.component.html',
    styleUrls: ['./currencies-form.component.css']
})

export class CurrenciesFormComponent extends TemplateComponent {    

    currency: Currency = new Currency();        

    constructor(private currenciesService: CurrenciesService, injector: Injector) { 
        super(injector);    
    }

    ngOnInit() {
        this.initForm();
    }            

    initForm(id = null){     
        this.id = id;    

        this.form = this.formBuilder.group({                
                CURRENCY: ['',Validators.required],
                LABEL: ['',Validators.required],
                SYMBOL: ['',Validators.required],
                ACTIVE: ['1'],                
        });

        if (id){                
            this.currenciesService.get(id).subscribe((res: Currency[])=>{                                
                console.log(res);
                this.currency = res.shift();                
                delete this.currency.NUM_CODE;
                this.form.patchValue(this.currency);                  
            })
            this.save = this.currenciesService.update.bind(this.currenciesService);            
        } else {
            this.save = this.currenciesService.create.bind(this.currenciesService);            
        }    
        
        this.form.markAsUntouched();
    }
    
    doSave() {
        Object.assign(this.currency,this.form.value);
                       
        if (this.validate()) return 

        this.save(this.id,this.currency)
        .subscribe(
            data => {
                console.log(data);
                this.toastr.success('Currency saved!','Success')    
                this.submit.emit();
            },
            error => {                              
                console.log(error);
                this.toastr.error('Attention, make sure your record has been saved.','Danger')
            }
        );
    }    
}