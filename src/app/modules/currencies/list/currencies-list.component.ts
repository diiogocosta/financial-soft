import { Component, OnInit, OnDestroy, ViewChild, ElementRef  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { Currency } from '../../../_models';
import { CurrenciesService } from '../../../_services';
import { CurrenciesFormComponent } from '../form/currencies-form.component';

declare var UIkit: any;

@Component({
  selector: 'app-currencies-list',
  templateUrl: './currencies-list.component.html'
})

export class CurrenciesListComponent implements OnDestroy, OnInit {    
    @ViewChild(DataTableDirective) dtElement: DataTableDirective;
    @ViewChild('modalComponent') modalForm: ElementRef;
    
    dtOptions: DataTables.Settings = {};
    currencies: Currency[] = [];
    dtTrigger= new Subject();
    modal = false;

    constructor(private currenciesService: CurrenciesService) {}
    
    ngOnInit() {
        this.loadCurrencies();
    }
    
    private loadCurrencies() {
        const that = this;

        this.currenciesService.get()
        .subscribe(
            data => {
                that.currencies = data;    
                this.rerender();            
            },
            error => {
                console.log(error);
            }
        );
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
          dtInstance.destroy();          
          this.dtTrigger.next();
        });
      }

    ngOnDestroy(): void {                        
        this.dtTrigger.unsubscribe();
    }

    ngAfterViewInit(){
        this.dtTrigger.next();                
    }

    removeCurrency(id){

        this.currenciesService.delete(id)
        .subscribe(
            data => {                                
                this.loadCurrencies();
            },
            error => {        
                console.log(error);
            }
        );
    }

    newCurrency(form: CurrenciesFormComponent){
        form.initForm();        
        this.showModal();
    }

    editCurrency(form: CurrenciesFormComponent,id){
        form.initForm(id);
        this.showModal();
    }    

    showModal(){
        UIkit.modal(document.getElementById('currency-form')).show();        
    }

    closeModal(){                
        UIkit.modal(document.getElementById('currency-form')).hide();
        this.loadCurrencies();
    }    
}