import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CurrenciesRoutingModule } from './currencies-routing.module';

import { CurrenciesListComponent } from './list/currencies-list.component';
import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { CurrenciesFormComponent } from './form/currencies-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CurrenciesService } from '../../_services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    CurrenciesRoutingModule,
    SharedModule    
  ],
  declarations: [CurrenciesListComponent, CurrenciesFormComponent],
  providers: [CurrenciesService]
})
export class CurrenciesModule { }
