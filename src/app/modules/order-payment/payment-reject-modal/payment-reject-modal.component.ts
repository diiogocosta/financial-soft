import { Component } from '@angular/core';
import { CustomerOrderData } from '../../../_models';

@Component({
    selector: 'payment-reject-modal',
    templateUrl: './payment-reject-modal.component.html',
    styleUrls: ['./payment-reject-modal.component.css'],
    inputs : ['orders']
})

export class PaymentRejectModalComponent {

    orders : CustomerOrderData[];

    constructor() {
    }

    ngOnInit() {
    }
}