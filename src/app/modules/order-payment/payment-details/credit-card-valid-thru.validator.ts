import { AbstractControl } from "@angular/forms";

export function creditCardValidThruValidator(c : AbstractControl) : { [key : string] : any } | null {


    const expirationMonth = c.get('EXPIRATION_MONTH');
    const expirationYear = c.get('EXPIRATION_YEAR');

    const invalidObj = {
        'cardExpired' : true
    }

    let d = new Date();
    let y = d.getFullYear();
    let m = d.getMonth() + 1;

    if (expirationMonth.valid && expirationYear.valid) {
        if ((expirationYear.value != null) && (expirationMonth.value != null) && (y == expirationYear.value) && (expirationMonth.value < m)){
            expirationMonth.setErrors(invalidObj);
        }
    }

    return null;    
}