import { ChangeDetectorRef, Component, OnInit, OnDestroy, AfterViewInit, Input, Injector, Output, EventEmitter } from '@angular/core';
import { OrderPaymentService } from '../../../_services/order-payment.service';
import { CountriesService, AvailableCountries } from '../../../_services/countries.service';
import { StatesService } from '../../../_services/states.service';
import { PaymentMethod, CustomerOrderData, Card, Payment, CustomerOrder } from '../../../_models';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Observable, Subscription, of, forkJoin } from 'rxjs';
import { creditCardValidator } from './credit-card.validator';
import { creditCardValidThruValidator } from "./credit-card-valid-thru.validator";
import { CardService } from '../../../_services';
import { getIpInfo, getBrowserInfo, BrowserInfo } from '../../../_helpers/browser-info';
import { TranslateService } from '../../../_translate/translate.service';
import { Month, MonthService } from '../../../_services/month.service';
import { map, concatMap } from 'rxjs/operators';

declare var UIkit: any;

export interface CardInfo {
    ACCOUNT_NUMBER : string,
    FULL_NAME : string,
    EXPIRATION_MONTH : string,
    EXPIRATION_YEAR : string,
    CV_NUMBER : string
}

export interface BillingInfo {
    
    BILLTO_FIRSTNAME ?: string;
    BILLTO_LASTNAME ?: string;
    BILLTO_PHONENUMBER ?: string;
    BILLTO_EMAIL ?: string;
    BILLTO_STREET1 ?: string;
    BILLTO_STREET_NUMBER ?: string;
    BILLTO_STREET2 ?: string;
    BILLTO_COUNTRY ?: string;
    BILLTO_STATE ?: string;
    BILLTO_CITY ?: string;
    BILLTO_POSTALCODE ?: string;
}

@Component({
  selector: 'app-payment-details',
  templateUrl: './payment-details.component.html',
  styleUrls: ['./payment-details.component.css'],
  inputs: ['customer','data','paymentMethod','amount','regularOrders','orders'],
  outputs: ['onPayment']
})
export class PaymentDetailsComponent implements OnDestroy, OnInit, AfterViewInit {

    paymentMethod : PaymentMethod;
    amount : number;
    serviceFee : number;
    serviceFeePercentage : number;
    amountFeePercentage : number;
    grandTotal : number;
    form: FormGroup;
    months : Month[];
    _validCards : Card[];
    insertedCard : Card;

    regularOrders : CustomerOrderData[];
    orders : CustomerOrderData[];
    customer : CustomerOrder;

    modalBlock : any = null;
    
    countries : string[];
    states : string[];
    expirationYears : Number[];

    translateService : TranslateService;
    ip : string;
    browserInfo : BrowserInfo;

    data : string;

    @Input() events: Observable<PaymentMethod>;
    private eventsSubscription: Subscription;
    private _countryChangeSub : Subscription;
    private _accountNumberChangeSub : Subscription;
    private _pmtMethodChangeSub : Subscription;
    private _getCountriesSub : Subscription;
    private _getMonthsSub : Subscription;
    private _postPaymentSub: Subscription;
    private _validCards$ : Observable<any>;
    @Output() onPayment = new EventEmitter();

    private _postPayment$ : Observable<any>;

    public PMT_METHOD_TYPE = {
        CARD : 'CC',
        CHECK : 'EC',
        MAIL: 'MAIL',
        MANUAL: 'MANUAL'
    };

    acceptCard : any;
    
    constructor(private _injector: Injector,
                private _orderPaymentService: OrderPaymentService, 
                private _fb : FormBuilder,
                private _countiesService : CountriesService,
                private _statesService : StatesService,
                private _cardService : CardService,
                private _monthService : MonthService) {
        this.translateService = this._injector.get(TranslateService);
    }

    translate(key){
        return this.translateService.data[key];
    }

    ngOnInit() {

        this.eventsSubscription = this.events.subscribe((paymentMethod) => {
            if (paymentMethod)
                this._setPaymentMethodType(paymentMethod.PMT_METHOD_TYPE)
        });

        this._validCards$ = this._orderPaymentService.getListValidCards(this.data);
        
        this._validCards$.subscribe(validCards => this._validCards = validCards);

        this._loadAllProviders();

        this.amount = 0;
        this.orders.map(o => this.amount = Math.round(((this.amount * 100) + (parseFloat(o.ORDER_AMOUNT) * 100))) / 100)

        this.serviceFee = 0;
        this.serviceFeePercentage = 0;
        this.amountFeePercentage = 0;
        this.grandTotal = this.amount;

        this.form = this._initPaymentMethodFormGroup();
        this.form.markAsUntouched();

        const ctrl = (<any>this.form).controls;
        const cardCtrl = ctrl.card;
        const checkCtrl = ctrl.check;
        const mailCtrl = ctrl.mail;
        const manualCtrl = ctrl.manual;

        console.log(ctrl);

        const changes$ = ctrl.type.valueChanges;

        this._pmtMethodChangeSub = changes$.subscribe(paymentMethodType => {

            if (paymentMethodType == this.PMT_METHOD_TYPE.CARD) {

                Object.keys(cardCtrl.controls).forEach(key => {
                    cardCtrl.controls[key].setValidators(this._initPaymentMethodCardModel()[key][1]);
                    cardCtrl.controls[key].updateValueAndValidity();

                    if (key == "BILLTO_COUNTRY") {
                        this._countryChangeSub = cardCtrl.controls[key].valueChanges
                        .subscribe(country => {
                            return this._statesService.getStates(country)
                            .subscribe(states => {
                                if ((states.length == 0) && (this.states != undefined) && (this.states.length != 0)) {
                                    this.states = states;
                                    cardCtrl.controls["BILLTO_COUNTRY"].setValidators(null);
                                    cardCtrl.controls["BILLTO_COUNTRY"].updateValueAndValidity();
                                } else if ((states.length != 0) && (this.states != undefined) && (this.states.length == 0)) {
                                    this.states = states;
                                    cardCtrl.controls["BILLTO_COUNTRY"].setValidators(this._initPaymentMethodCardModel()["BILLTO_COUNTRY"][1]);
                                    cardCtrl.controls["BILLTO_COUNTRY"].updateValueAndValidity();
                                } else {
                                    this.states = states;
                                }
                            });
                        });
                    }

                    if (key == "ACCOUNT_NUMBER") {
                        this._accountNumberChangeSub = cardCtrl.controls[key].statusChanges.subscribe(status => {
                            if (status == "VALID") {
                                
                                this.insertedCard = this._validCards.find(card => new RegExp(card.REGEX_VALID_CARD.replace(/\//g,'')).test(cardCtrl.controls['ACCOUNT_NUMBER'].value));
                                
                                let cardValues = this._cardService.applyCardFees(this.insertedCard, this.amount);
                                
                                this.grandTotal = cardValues.grandTotal;
                                this.amountFeePercentage = cardValues.amountFeePercentage;
                                this.serviceFeePercentage = cardValues.serviceFeePercentage;
                                this.serviceFee = cardValues.serviceFee;
                            }
                        });
                    }
                });

                cardCtrl.controls["BILLTO_COUNTRY"].setValue(AvailableCountries.Brazil);
                cardCtrl.controls["BILLTO_COUNTRY"].updateValueAndValidity();

                Object.keys(checkCtrl.controls).forEach(key => {
                    checkCtrl.controls[key].setValidators(null);
                    checkCtrl.controls[key].updateValueAndValidity();
                });

                Object.keys(mailCtrl.controls).forEach(key => {
                    mailCtrl.controls[key].setValidators(null);
                    mailCtrl.controls[key].updateValueAndValidity();
                });

                Object.keys(manualCtrl.controls).forEach(key => {
                    manualCtrl.controls[key].setValidators(null);
                    manualCtrl.controls[key].updateValueAndValidity();
                });

            } else if (paymentMethodType == this.PMT_METHOD_TYPE.CHECK) {
  
                Object.keys(cardCtrl.controls).forEach(key => {
                    cardCtrl.controls[key].setValidators(null);
                    cardCtrl.controls[key].setAsyncValidators(null);

                    if ('EXPIRATION_MONTH' == key){
                        cardCtrl.controls[key].value = null;
                    }
                    cardCtrl.controls[key].updateValueAndValidity();
                });

                Object.keys(checkCtrl.controls).forEach(key => {
                    checkCtrl.controls[key].setValidators(this._initPaymentMethodCheckModel()[key][1]);
                    checkCtrl.controls[key].updateValueAndValidity();

                    if (key == "BILLTO_COUNTRY") {
                        this._countryChangeSub = checkCtrl.controls[key].valueChanges
                        .subscribe(country => {
                            return this._statesService.getStates(country)
                            .subscribe(states => {
                                if ((states.length == 0) && (this.states != undefined) && (this.states.length != 0)) {
                                    this.states = states;
                                    checkCtrl.controls["BILLTO_COUNTRY"].setValidators(null);
                                    checkCtrl.controls["BILLTO_COUNTRY"].updateValueAndValidity();
                                } else if ((states.length != 0) && (this.states != undefined) && (this.states.length == 0)) {
                                    this.states = states;
                                    checkCtrl.controls["BILLTO_COUNTRY"].setValidators(this._initPaymentMethodCardModel()["BILLTO_COUNTRY"][1]);
                                    checkCtrl.controls["BILLTO_COUNTRY"].updateValueAndValidity();
                                } else {
                                    this.states = states;
                                }
                            });
                        });
                    }
                });

                checkCtrl.controls["BILLTO_COUNTRY"].setValue(AvailableCountries.Brazil);
                checkCtrl.controls["BILLTO_COUNTRY"].updateValueAndValidity();

                Object.keys(mailCtrl.controls).forEach(key => {
                    mailCtrl.controls[key].setValidators(null);
                    mailCtrl.controls[key].updateValueAndValidity();
                });

                Object.keys(manualCtrl.controls).forEach(key => {
                    manualCtrl.controls[key].setValidators(null);
                    manualCtrl.controls[key].updateValueAndValidity();
                });
            } else if (paymentMethodType == this.PMT_METHOD_TYPE.MAIL) {
  
                Object.keys(cardCtrl.controls).forEach(key => {
                    cardCtrl.controls[key].setValidators(null);
                    cardCtrl.controls[key].setAsyncValidators(null);

                    if ('EXPIRATION_MONTH' == key){
                        cardCtrl.controls[key].value = null;
                    }
                    cardCtrl.controls[key].updateValueAndValidity();
                });

                Object.keys(checkCtrl.controls).forEach(key => {
                    checkCtrl.controls[key].setValidators(null);
                    checkCtrl.controls[key].updateValueAndValidity();
                });

                Object.keys(mailCtrl.controls).forEach(key => {
                    mailCtrl.controls[key].setValidators(this._initPaymentMethodMailModel()[key][1]);
                    mailCtrl.controls[key].updateValueAndValidity();
                });

                Object.keys(manualCtrl.controls).forEach(key => {
                    manualCtrl.controls[key].setValidators(null);
                    manualCtrl.controls[key].updateValueAndValidity();
                });
            } else if (paymentMethodType == this.PMT_METHOD_TYPE.MANUAL) {
  
                Object.keys(cardCtrl.controls).forEach(key => {
                    cardCtrl.controls[key].setValidators(null);
                    cardCtrl.controls[key].setAsyncValidators(null);

                    if ('EXPIRATION_MONTH' == key){
                        cardCtrl.controls[key].value = null;
                    }
                    cardCtrl.controls[key].updateValueAndValidity();
                });

                Object.keys(checkCtrl.controls).forEach(key => {
                    checkCtrl.controls[key].setValidators(null);
                    checkCtrl.controls[key].updateValueAndValidity();
                });

                Object.keys(mailCtrl.controls).forEach(key => {
                    mailCtrl.controls[key].setValidators(null);
                    mailCtrl.controls[key].updateValueAndValidity();
                });

                Object.keys(manualCtrl.controls).forEach(key => {
                    manualCtrl.controls[key].setValidators(this._initPaymentMethodManualModel()[key][1]);
                    manualCtrl.controls[key].updateValueAndValidity();
                });
            }
        });
        
        this._setPaymentMethodType(this.paymentMethod.PMT_METHOD_TYPE);
    }

    _setPaymentMethodType(type : string) {

        const ctrl : FormControl = (<any>this.form).controls.type;
        ctrl.setValue(type);
    }

    _initPaymentMethodFormGroup() : any {
        
        const group = this._fb.group({
            type : [''],
            card : this._fb.group(this._initPaymentMethodCardModel(),{ validator : creditCardValidThruValidator }),
            check : this._fb.group(this._initPaymentMethodCheckModel()),
            mail : this._fb.group(this._initPaymentMethodMailModel()),
            manual : this._fb.group(this._initPaymentMethodManualModel())
        });

        return group;
    }

    _initPaymentMethodCardModel(): any {

        const model = {
            BILLTO_FIRSTNAME : ['', Validators.required],
            BILLTO_LASTNAME : ['', Validators.required],
            BILLTO_PHONENUMBER : ['', Validators.required],
            BILLTO_EMAIL : ['', Validators.required],
            BILLTO_STREET1 : ['', Validators.required],
            BILLTO_STREET_NUMBER : ['', Validators.required],
            BILLTO_STREET2 : ['', Validators.required],
            BILLTO_COUNTRY : ['', Validators.required],
            BILLTO_STATE : ['', Validators.required],
            BILLTO_CITY : ['', Validators.required],
            BILLTO_POSTALCODE : ['', Validators.required],
            ACCOUNT_NUMBER : ['', [Validators.required], creditCardValidator(this._validCards$)],
            CARD_NAME : ['', Validators.required],
            EXPIRATION_MONTH : ['', Validators.required],
            EXPIRATION_YEAR : ['', Validators.required],
            CARD_CVC : ['', Validators.required],
        }

        return model;
    }

    _initPaymentMethodCheckModel(): any {
        
        const model = {
            BILLTO_FIRSTNAME : ['', Validators.required],
            BILLTO_LASTNAME : ['', Validators.required],
            BILLTO_PHONENUMBER : ['', Validators.required],
            BILLTO_EMAIL : ['', Validators.required],
            BILLTO_STREET1 : ['', Validators.required],
            BILLTO_STREET_NUMBER : ['', Validators.required],
            BILLTO_STREET2 : ['', Validators.required],
            BILLTO_COUNTRY : ['', Validators.required],
            BILLTO_STATE : ['', Validators.required],
            BILLTO_CITY : ['', Validators.required],
            BILLTO_POSTALCODE : ['', Validators.required],
            BANK_TRANSIT_NUMBER : ['', Validators.required],
            CHECK_ACCOUNT_NUMBER : ['', Validators.required],
            ACCOUNT_TYPE : ['', Validators.required],
        }

        return model;
    }

    _initPaymentMethodMailModel(): any {

        const model = {
            EMAIL : ['', Validators.required]
        }

        return model;
    }

    _initPaymentMethodManualModel(): any {
        
        const model = {
            MANUAL_TYPE : ['', Validators.required]
        }

        return model;
    }
    
    private _loadAllProviders() {

        this._getCountriesSub = this._countiesService.getCountries().subscribe(countries => this.countries = countries);
        this._getMonthsSub = this._monthService.getMonths().subscribe(months => this.months = months);
        this.expirationYears = this._cardService.getExpirationYear();

        getIpInfo()
            .then(ip => {

                console.log('ip obtido:');
                console.log(ip);

                (ip) ? this.ip = ip : this.ip = "";
            })
            .catch(e => console.error(this.translate('unableToGetIP')));

        this.browserInfo = getBrowserInfo();
    }

    ngAfterViewInit(): void {
    }

    ngOnDestroy(): void {

        this._countryChangeSub.unsubscribe();
        this._accountNumberChangeSub.unsubscribe();
        this._pmtMethodChangeSub.unsubscribe();
        this._getCountriesSub.unsubscribe();
        this._getMonthsSub.unsubscribe();

        if (this._postPaymentSub)
            this._postPaymentSub.unsubscribe();
    }

    onSubmit(formVal) {

        const ctrl = (<any>this.form).controls;

        if (this.form.invalid) return;

        let arrOrderId : Array<string> = [];

        this.orders.map(o => arrOrderId.push(o.ID));

        let payment : Payment = {
            CUSTOMERID : this.orders[0].CUSTOMERID,
            ORDER_ID : arrOrderId,
            PAYEE_CREDENTIALS : this.ip + " " + ((this.browserInfo.name) ? this.browserInfo.name : "") + " " + ((this.browserInfo.version) ? this.browserInfo.version : "")
        }
        
        let card : { model : Card, info : CardInfo, billingInfo : BillingInfo } | null = null;

        if (this.paymentMethod.PMT_METHOD_TYPE == this.PMT_METHOD_TYPE.CARD) {
            
            const cardCtrl = ctrl.card;

            payment.CARD_TYPE = this.insertedCard.TYPE;
            payment.FULL_NAME = cardCtrl.controls['CARD_NAME'].value;
            payment.EXPIRATION_MONTH = cardCtrl.controls['EXPIRATION_MONTH'].value;
            payment.EXPIRATION_YEAR = cardCtrl.controls['EXPIRATION_YEAR'].value;
            payment.CURRENCY = 'USD';
            payment.CV_NUMBER = cardCtrl.controls['CARD_CVC'].value;
            payment.ACCOUNT_NUMBER = cardCtrl.controls['ACCOUNT_NUMBER'].value;

            card = {
                model : this.insertedCard,
                info : {
                    ACCOUNT_NUMBER : cardCtrl.controls['ACCOUNT_NUMBER'].value,
                    FULL_NAME : cardCtrl.controls['CARD_NAME'].value,
                    EXPIRATION_MONTH : cardCtrl.controls['EXPIRATION_MONTH'].value,
                    EXPIRATION_YEAR : cardCtrl.controls['EXPIRATION_YEAR'].value,
                    CV_NUMBER : cardCtrl.controls['CARD_CVC'].value
                },
                billingInfo : {
                    BILLTO_FIRSTNAME : cardCtrl.controls['BILLTO_FIRSTNAME'].value,
                    BILLTO_LASTNAME : cardCtrl.controls['BILLTO_LASTNAME'].value,
                    BILLTO_PHONENUMBER : cardCtrl.controls['BILLTO_PHONENUMBER'].value,
                    BILLTO_EMAIL : cardCtrl.controls['BILLTO_EMAIL'].value,
                    BILLTO_STREET1 : cardCtrl.controls['BILLTO_STREET1'].value,
                    BILLTO_STREET_NUMBER : cardCtrl.controls['BILLTO_STREET_NUMBER'].value,
                    BILLTO_STREET2 : cardCtrl.controls['BILLTO_STREET2'].value,
                    BILLTO_COUNTRY : cardCtrl.controls['BILLTO_COUNTRY'].value,
                    BILLTO_STATE : cardCtrl.controls['BILLTO_STATE'].value,
                    BILLTO_CITY : cardCtrl.controls['BILLTO_CITY'].value,
                    BILLTO_POSTALCODE : cardCtrl.controls['BILLTO_POSTALCODE'].value
                }
            }

        } else if (this.paymentMethod.PMT_METHOD_TYPE == this.PMT_METHOD_TYPE.CHECK) {

            const checkCtrl = ctrl.check;

            payment.BANK_TRANSIT_NUMBER = checkCtrl.controls['BANK_TRANSIT_NUMBER'].value;
            payment.CHECK_ACCOUNT_NUMBER = checkCtrl.controls['CHECK_ACCOUNT_NUMBER'].value;
            payment.ACCOUNT_TYPE = checkCtrl.controls['ACCOUNT_TYPE'].value;

        } else if (this.paymentMethod.PMT_METHOD_TYPE == this.PMT_METHOD_TYPE.MAIL) {

            const mailCtrl = ctrl.mail;

            payment.EMAIL = mailCtrl.controls['EMAIL'].value;

        } else if (this.paymentMethod.PMT_METHOD_TYPE == this.PMT_METHOD_TYPE.MANUAL) {

            const manualCtrl = ctrl.manual;

            payment.MANUAL_TYPE = manualCtrl.controls['MANUAL_TYPE'].value;
        }

        this.modalBlock = UIkit.modal.blockUI("<div class='uk-text-center'><img class='uk-margin-top' src='./../../../../assets/img/spinners/spinner.gif' alt=''>"); 

        payment.DATA = this.data;

        this._postPayment$ = this._orderPaymentService.postPayment(payment);

        this._postPaymentSub = this._postPayment$.subscribe(result => {
            this.onPayment.emit({ payment : result, card : card });
        }, () => this.modalBlock.hide(), () => this.modalBlock.hide());
    }

    f = (pmtMethod, name) => (<any>this.form).controls[pmtMethod].controls[name];
    
    getErrorMessage(control: FormControl) {
        return control.hasError('required') ? 'mustEnterValue' :
               control.hasError('email') ? 'notValidEmail' :
               control.hasError('minlength') ? 'incorrectValue' :
               control.hasError('pattern') ? 'incorrectPattern' :
               control.hasError('cardExpired') ? 'cardExpired' :
               '';
    }
}