import { AbstractControl, ValidatorFn } from "@angular/forms";
import { Observable, of } from "rxjs";
import { Card } from "../../../_models";
import { map } from "rxjs/operators";

export function creditCardValidator(listValidCards : Observable<Card[]>) : ValidatorFn {

    const invalidObj = {
        'pattern' : true
    }

    return (control : AbstractControl) : { [key : string] : any } | null => {

        return listValidCards.pipe(
            map((cards) => {

                let cardsRegExp : RegExp[] = [];
                cards.forEach(card => cardsRegExp.push(new RegExp(card.REGEX_VALID_CARD.replace(/\//g,''))));
                return cardsRegExp.some(regex => regex.test(control.value)) ? null : invalidObj;
            })
        )
    }
}