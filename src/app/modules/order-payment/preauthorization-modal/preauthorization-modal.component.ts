import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { CustomerOrderData, Card } from '../../../_models';
import { CardInfo, BillingInfo } from '../payment-details/payment-details.component';
import { CardService } from '../../../_services';

@Component({
    selector: 'preauthorization-modal',
    templateUrl: './preauthorization-modal.component.html',
    styleUrls: ['./preauthorization-modal.component.css'],
    inputs : ['orders','card']
})

export class PreauthorizationModalComponent implements OnChanges {
    
    orders : CustomerOrderData[];

    regularOrders : CustomerOrderData[];
    customizedOrders : CustomerOrderData[];

    card : { model : Card, info : CardInfo, billingInfo : BillingInfo };

    roTotal : number = 0;
    coTotal : number = 0;

    amountFee : number = 0;

    grandTotal : number = 0; //consider only DECISION = ACCEPT

    bShowAmountFee : string = "";
    accountNumber : string;

    constructor(private _cardService : CardService) {
    }

    init() {

        console.log("card = ");
        console.log(this.card);

        this.regularOrders = [];      
        this.customizedOrders = [];
        this.orders.map(o => {
            
            (o.ORDER_TYPE.toUpperCase() == "REGULAR") ? this.regularOrders.push(o) : this.customizedOrders.push(o);
            
            if (o.DECISION && (o.DECISION.toUpperCase() == "ACCEPT") && o.AMOUNT)
                this.grandTotal = Math.round((this.grandTotal * 100) + (parseFloat(o.AMOUNT) * 100)) / 100;        
        });

        this.regularOrders.map((ro) => {

            if (ro.AMOUNT)
                this.roTotal = Math.round((this.roTotal * 100) + (parseFloat(ro.AMOUNT) * 100)) / 100;
        });

        this.customizedOrders.map((co) => {

            if (co.AMOUNT)
                this.coTotal = Math.round((this.coTotal * 100) + (parseFloat(co.AMOUNT) * 100)) / 100;
        });

        this._calcFee();
        this.accountNumber = this.getMaskedCardNumber(this.card.info.ACCOUNT_NUMBER);
    }

    private _calcFee() {

        let cardValues = this._cardService.applyCardFees(this.card.model, this.grandTotal);
        this.amountFee = cardValues.totalFees;

        if (this.amountFee){
            if (this.regularOrders.some(ro => ro.DECISION && ro.DECISION.toUpperCase() == "ACCEPT")){
                this.bShowAmountFee = "ro";
                this.roTotal = Math.round((this.roTotal * 100) + (this.amountFee * 100)) / 100;
            } else if(this.customizedOrders.some(co => co.DECISION && co.DECISION.toUpperCase() == "ACCEPT")){
                this.bShowAmountFee = "co";
                this.coTotal = Math.round((this.coTotal * 100) + (this.amountFee * 100)) / 100;
            }
        }
    }

    getMaskedCardNumber(account : string) : string {

        return account.replace(/\d(?=\d{4})/g, "x");
    }
    
    ngOnChanges(changes: SimpleChanges): void {

        if (!this.card) return;

        this.init();
    }
}