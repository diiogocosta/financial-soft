import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderPaymentRoutingModule } from './order-payment-routing.module';

import { SelectCustomerComponent } from './select-customer/select-customer.component';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { SharedModule } from '../_shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrderPaymentService } from '../../_services/order-payment.service';
import { OrderPaymentPanelComponent } from './order-payment-panel/order-payment-panel.component';
import { OrderDetailsComponent } from "./order-details/order-details.component";
import { PaymentMethodComponent } from "./payment-method/payment-method.component";
import { PaymentDetailsComponent } from './payment-details/payment-details.component';

import { CountriesService } from '../../_services/countries.service';
import { StatesService } from '../../_services/states.service';
import { CardService } from '../../_services/card.service';
import { PaymentRejectModalComponent } from './payment-reject-modal/payment-reject-modal.component';
import { PreauthorizationModalComponent } from './preauthorization-modal/preauthorization-modal.component';
import { MonthService } from '../../_services';
import { InvoicePaymentService } from '../../_services/invoice-payment.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    OrderPaymentRoutingModule,
    SharedModule
  ],
  declarations: [SelectCustomerComponent, CustomerInfoComponent, OrderPaymentPanelComponent, OrderDetailsComponent, PaymentMethodComponent, PaymentDetailsComponent, 
    PaymentRejectModalComponent, PreauthorizationModalComponent],
  exports: [SelectCustomerComponent, PaymentMethodComponent],
  providers: [ OrderPaymentService, InvoicePaymentService, CountriesService, StatesService, CardService, MonthService ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class OrderPaymentModule { }
