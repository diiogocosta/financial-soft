import { Component  } from '@angular/core';

import {  CustomerOrder, PaymentMethod, CustomerOrderData, Card } from '../../../_models';
import { Subject, Observable } from 'rxjs';
import { PaymentResponse, OrderPaymentService } from '../../../_services/order-payment.service';
import { CardInfo, BillingInfo } from '../payment-details/payment-details.component';
import {ActivatedRoute } from '@angular/router';

declare var UIkit: any;

@Component({
  selector: 'app-order-payment-panel',
  templateUrl: './order-payment-panel.component.html',
  styleUrls: ['./order-payment-panel.component.css']
})

export class OrderPaymentPanelComponent {
    
    customer : CustomerOrder;
    paymentMethod : PaymentMethod;
    selectedRegularOrders : CustomerOrderData[] = [];
    minSelectError : boolean;
    ordersPayment : CustomerOrderData[];
    cardPayment: { model: Card; info: CardInfo; };

    customerOrders$ : Observable<CustomerOrder[]>;

    openRejectModal : boolean = false;
    openAcceptModal : boolean = false;

    modalBlock : any = null;

    private eventsSubject: Subject<void> = new Subject<void>();
    private refreshCustomerSubject: Subject<any> = new Subject<any>();
    private paymentMethodSubject: Subject<PaymentMethod> = new Subject<PaymentMethod>();

    data : string;
    CUSTOMERID : string;
    BILLTO_FIRSTNAME : string;
    BILLTO_LASTNAME : string;
    BILLTO_EMAIL : string;

    constructor(private route : ActivatedRoute, private _service : OrderPaymentService) {

        this.customerOrders$ = _service.getCustomerOrders();
        route.params.subscribe(params => {

            this.data = params['data'];

            if (this.data) {

                $(document).ready(function(){
                    $("#page_content").css("margin-left","0px");
                });

                let decoded = atob(this.data);
                let arrCustomerInfo = decoded.split('|').reverse();

                this.BILLTO_EMAIL = arrCustomerInfo[0];
                this.CUSTOMERID = arrCustomerInfo[1];
            }
        });
    }
  
    reverse(str:string) : string {
        return str.split('').reverse().join('');
    }

    ngAfterViewInit(): void {

        let that = this;
        $('#preauthorization-modal, #payment-reject-modal').on({
            'hide.uk.modal': function(){
                that.refreshCustomerSubject.next((that.data) ? that.data : that.customer);
            }
        });
    }

    emitRemovePaymentMethod() {
        this.eventsSubject.next();
    }

    customerSelected(customer : CustomerOrder){

        this.CUSTOMERID = customer.CUSTOMERID;
        this.BILLTO_FIRSTNAME = customer.BILLTO_FIRSTNAME;
        this.BILLTO_LASTNAME = customer.BILLTO_LASTNAME;
        this.BILLTO_EMAIL = customer.BILLTO_EMAIL;

        this.customer = customer;
        this.emitRemovePaymentMethod();
    }

    setPaymentMethod(paymentMethod) {

        this.paymentMethod = paymentMethod;
      
        if (this.paymentMethod && !this.selectedRegularOrders.length){

            this.emitRemovePaymentMethod();
            this.minSelectError = true;
        } else {
            
            this.paymentMethodSubject.next(this.paymentMethod);
        }
    }
    
    setSelectedOrders(obj : { selectedOrders : CustomerOrderData[], ignoreMinSelectError ?: Boolean}){

        this.selectedRegularOrders = obj.selectedOrders;
        if (!obj.ignoreMinSelectError) {

            if (this.paymentMethod && !this.selectedRegularOrders.length){
                this.minSelectError = true;
            } else {
                this.minSelectError = false;
            }
        }

        this.emitRemovePaymentMethod();
    }

    setBillTo(orders : CustomerOrderData[]){

        if (orders != null && orders.length) {
            
            this.BILLTO_FIRSTNAME = orders[0].BILLTO_FIRSTNAME;
            this.BILLTO_LASTNAME = orders[0].BILLTO_LASTNAME;
        }
    }

    handlePayment(response ?: any) {

        let payment : PaymentResponse = response.payment;
        let card : { model : Card, info : CardInfo, billingInfo : BillingInfo } | null = response.card;

        this.ordersPayment = payment.ORDERS;
        this.cardPayment = card;

        if (payment.ORDERS.every(data => data.DECISION == "REJECT")){
            this.showRejectModal();
        } else {
            this.showAcceptModal();
        }
    }

    showRejectModal(){
        this.openRejectModal = true;
        UIkit.modal(document.getElementById('payment-reject-modal')).show();        
    }

    showAcceptModal(){
        this.openAcceptModal = true;
        UIkit.modal(document.getElementById('preauthorization-modal')).show();
    }
}