import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderPaymentPanelComponent } from './order-payment-panel/order-payment-panel.component';

const routes: Routes = [
  {
    path: '', component: OrderPaymentPanelComponent
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderPaymentRoutingModule { }
