import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, ElementRef, EventEmitter, AfterViewInit, ApplicationRef, HostBinding, ViewChildren, QueryList, Input  } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject, forkJoin, of } from 'rxjs';

import { CustomerOrderData, OrderItem, TxnOperation } from '../../../_models';
import { CustomerOrder } from '../../../_models';
import { OrderPaymentService } from '../../../_services/order-payment.service';
import { concatMap } from 'rxjs/operators';
import { TranslateService } from '../../../_translate/translate.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css'],
  inputs: ['customerOrder','showMinSelectError','data','BILLTO_EMAIL'],
  outputs: ['onChangeSelection','onLoadCustomerOrderData']
})

export class OrderDetailsComponent implements OnDestroy, OnInit, AfterViewInit {
    
    @ViewChildren(DataTableDirective) dtElement: QueryList<DataTableDirective>;

    dtRegularOrders : DataTableDirective;
    dtCustomizedOrders : DataTableDirective;

    dtOptions : DataTables.Settings = {};
    dtOptionsCustomized : DataTables.Settings = {};
    dtTrigger = new Subject();
    dtTriggerCustomized = new Subject();
    childRows = [];

    customerOrder : CustomerOrder;
    orderDetails : CustomerOrderData[];
    regularOrders : CustomerOrderData[];
    customizedOrders : CustomerOrderData[];
    onChangeSelection = new EventEmitter<any>();
    onLoadCustomerOrderData = new EventEmitter<any>();
    regularOrdersAllChecked : boolean;
    customizedOrdersAllChecked : boolean;

    showMinSelectError : boolean;

    totalRegular : number = 0;
    totalToAuthorizeRegular : number = 0;

    totalCustomized : number = 0;
    totalToAuthorizeCustomized : number = 0;

    data : string;
    BILLTO_EMAIL : string;
    
    constructor(private _orderPaymentService: OrderPaymentService, private ref: ChangeDetectorRef, private translate : TranslateService) {
    }

    openDetails(o : CustomerOrderData, id : string) {

        let table = $(`#${id}`).DataTable();
        let tr = $(`#${id} tbody tr#${o.ORDERNUMBER}`)[0];
        let row = table.row('#'+tr.id);
        let idx = $.inArray( tr.id, this.childRows );
        let icon = $(`#icon_plus_minus_${o.ORDERNUMBER}`);
        
        let r = this.regularOrders[0];
        let transactionAmount : number = (o.TRANSACTION_AMOUNT) ? parseFloat(o.TRANSACTION_AMOUNT) : 0;
        let authorizationAmount : number = (o.AUTHORIZATION_AMOUNT) ? parseFloat(o.AUTHORIZATION_AMOUNT) : 0;

        if (idx == -1) {
            
            icon.removeClass('uk-icon-plus');
            icon.addClass('uk-icon-minus');

            this._orderPaymentService.getOrderItems(o.ID)
                .pipe(
                    concatMap(orderItems => 
                        forkJoin(
                            of(orderItems),
                            ((transactionAmount > authorizationAmount) && (o.REQUESTID)) ? this._orderPaymentService.getTxnOperation(o.REQUESTID) : of(null)
                        )
                    )
                )
                .subscribe(arrItemsAndOp => {

                    let orderItems = arrItemsAndOp[0];
                    let txnOperations = arrItemsAndOp[1];

                    let arrNodeChild : Array<any> = [
                        this.getHtmlOrderItems( orderItems )
                    ];
                    
                    if (txnOperations && txnOperations.length) 
                        arrNodeChild.push(this.getHtmlTxnOperationInfo(txnOperations));

                    row.child(arrNodeChild,`txn-operation-${r.ID}`).show();

                    $(`.txn-operation-${r.ID}:last`).css({
                        'padding-bottom': '0px',
                        'padding-left': '0px',
                        'padding-right': '0px',
                        'padding-top': '0px',
                        'border-bottom-width': '0px',
                        'width':'100%',
                        'box-sizing':'unset'
                    });
                    this.childRows.push( tr.id );
                });
        } else {
            icon.removeClass('uk-icon-minus');
            icon.addClass('uk-icon-plus');
            
            if ((transactionAmount > authorizationAmount) && (o.REQUESTID)){
                this._orderPaymentService.getTxnOperation(o.REQUESTID)
                    .subscribe(txnOperations => row.child(this.getHtmlTxnOperationInfo(txnOperations)).show());
            } else 
                row.child(false).remove();

            this.childRows.splice( idx, 1 );
        }
    }
    
    ngOnInit() {
        this._loadAllProviders();
    }
    
    private _loadAllProviders() {
        const that = this;

        this.totalToAuthorizeRegular = 0;
        this.totalToAuthorizeCustomized = 0;
        this.onChangeSelection.emit({selectedOrders : [], ignoreMinSelectError : true});
        
        forkJoin(
            of(this.data),
            of(this.customerOrder)
        ).pipe(
            concatMap(arrObj => 
                (arrObj[0] != null) ? this._orderPaymentService.getExternalCustomerOrderData(this.data, this.BILLTO_EMAIL) 
                : this._orderPaymentService.getCustomerOrderData(this.customerOrder.CUSTOMERID, this.customerOrder.BILLTO_EMAIL))
        )
        .subscribe(customerOrderData => {
            that.orderDetails = customerOrderData;
            this.onLoadCustomerOrderData.emit(customerOrderData);
            that.regularOrders = that.orderDetails.filter((od)=> od.TRANSACTION_TYPE.toUpperCase() == "AUTHORIZATION");
            that.customizedOrders = that.orderDetails.filter((od)=> od.TRANSACTION_TYPE.toUpperCase() == "AUTHCAPTURE");

            this._sumTotal();
            this._renderer();
        }, error => {
            console.log(error);
        });
    }

    private _renderer() {

        this.dtRegularOrders = this.dtElement.first;
        this.dtCustomizedOrders = this.dtElement.last;

        this.dtRegularOrders.dtOptions.lengthChange = false;
        this.dtRegularOrders.dtOptions.searching = false;
        this.dtRegularOrders.dtOptions.paging = false;
        this.dtRegularOrders.dtOptions.ordering = false;

        this.dtCustomizedOrders.dtOptions.lengthChange = false;
        this.dtCustomizedOrders.dtOptions.searching = false;
        this.dtCustomizedOrders.dtOptions.paging = false;
        this.dtCustomizedOrders.dtOptions.ordering = false;

        this.dtRegularOrders.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
        });

        this.dtCustomizedOrders.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTriggerCustomized.next();
        });

        this.regularOrders.map(o => {

            let transactionAmount : number = (o.TRANSACTION_AMOUNT) ? parseFloat(o.TRANSACTION_AMOUNT) : 0;
            let authorizationAmount : number = (o.AUTHORIZATION_AMOUNT) ? parseFloat(o.AUTHORIZATION_AMOUNT) : 0;

            if ((transactionAmount > authorizationAmount) && (o.REQUESTID)){

                this._orderPaymentService.getTxnOperation(o.REQUESTID)
                .subscribe(txnOperations => {

                    if (!txnOperations || !txnOperations.length) return;

                    let id = 'regular-orders';
                    let table = $(`#${id}`).DataTable();
                    let tr = $(`#${id} tbody tr#${o.ORDERNUMBER}`)[0];
                    let row = table.row('#'+tr.id);
                    
                    row.child([
                        this.getHtmlTxnOperationInfo(txnOperations),
                    ],`txn-operation-${o.ID}`).show();

                    $(`.txn-operation-${o.ID}:last`).css({
                        'padding-bottom': '0px',
                        'padding-left': '0px',
                        'padding-right': '0px',
                        'padding-top': '0px',
                        'border-bottom-width': '0px',
                        'width':'100%',
                        'box-sizing':'unset'
                    });
                })
            }
        });
    }

    getHtmlOrderItems(orderItems : OrderItem[]) {

        let table = 
        `<table cellpadding="5" cellspacing="0" border="0" class="table table-striped" style="width : 100%;">
            <tbody>
                <tr class="">
                    <th width="30%">${ this.translate.data['name'] }</th>
                    <th width="20%">${ this.translate.data['sku'] }</th>
                    <th class="uk-text-center" width="10%">${ this.translate.data['qty'] }</th>
                    <th class="uk-text-right" width="20%">${ this.translate.data['unitPrice'] }</th>
                    <th class="uk-text-right" width="20%">${ this.translate.data['extendedPrice'] }</th>
                </tr>`;

        for (let i = 0; i < orderItems.length; i++){
            table += 
                `<tr id="order-items-tr-${orderItems[i].ID}" style="background: #fff;">
                    <td width="30%">${orderItems[i].ID}</td>
                    <td width="20%">${orderItems[i].SKU}</td>
                    <td class="uk-text-center" width="10%">${orderItems[i].QUANTITY}</td>
                    <td class="uk-text-right" width="20%">${orderItems[i].UNITPRICE}</td>
                    <td class="uk-text-right" width="20%">${orderItems[i].EXTENDEDPRICE}</td>
                </tr>`;
        }
        
        table += `
            </tbody>
        </table>`;

        return table;
    }

    getHtmlTxnOperationInfo(txnOperations : TxnOperation[]) {

        let header = 
            `<tr role="row" style="width:100%;box-sizing:unset">
                <th width="5%"></th>
            
                <th width="15%" class="uk-text-right">Authorization Amount</th>
                <th width="10%" class="uk-text-center">Authorization Date</th>
                <th width="15%" class="uk-text-left">Card Number</th>
                <th width="15%" class="uk-text-left">Card Type</th>
                <th width="10%" class="uk-text-center">Expiration Month</th>
                <th width="20%" class="uk-text-center">Expiration Year</th>   
                <th width="5%"></th>                      
            </tr>`;

        let rows = '';

        txnOperations.forEach(i => {
            rows += 
                `<tr style="background: #fff; width:100%; box-sizing:unset" role="row" class="odd">
                    <td></td>
                    
                    <td class="uk-text-right value_content">${i.AMOUNT}</td>
                    <td class="uk-text-center">${i.TIMESTAMP}</td>
                    <td class="uk-text-left">${i.CARD_LASTFOUR}</td>
                    <td class="uk-text-left">${i.CARD_TYPE}</td>
                    <td class="uk-text-center">${i.CARD_EXPIRATIONMONTH}</td>
                    <td class="uk-text-center">${i.CARD_EXPIRATIONYEAR}</td>
                    <td></td>
                </tr>`;
        });

        return header + rows;
    }

    private _sumTotal() {
        this.totalRegular = 0;
        this.regularOrders.map((ro) => {

            if (ro.ORDER_AMOUNT)
                this.totalRegular = Math.round(((this.totalRegular * 100) + (parseFloat(ro.ORDER_AMOUNT) * 100))) / 100;
        });

        this.totalCustomized = 0;
        this.customizedOrders.map((ro) => {

            if (ro.ORDER_AMOUNT)
                this.totalCustomized = Math.round(((this.totalCustomized * 100) + (parseFloat(ro.ORDER_AMOUNT) * 100))) / 100;
        });
    }

    check(order : CustomerOrderData, checked : boolean, type : string) {

        order.SELECTED = checked;

        if (type == 'regular-orders') {    
    
            //https://www.freecodecamp.org/forum/t/exact-change-exercise-why-the-floating-point-error-and-best-approach-to-subtract-integer-from-floating-point/72376/7
            if (order.ORDER_AMOUNT) {
                if (checked) {
                    this.totalToAuthorizeRegular = Math.round(((this.totalToAuthorizeRegular * 100) + (parseFloat(order.ORDER_AMOUNT) * 100))) / 100;
                } else {
                    this.totalToAuthorizeRegular = Math.round(((this.totalToAuthorizeRegular * 100) - (parseFloat(order.ORDER_AMOUNT) * 100))) / 100;
                }
            }
    
            this.regularOrdersAllChecked = this.regularOrders.every(ro => ro.SELECTED);
        } else {
            
            if (order.ORDER_AMOUNT) {
                if (checked) {
                    this.totalToAuthorizeCustomized = Math.round(((this.totalToAuthorizeCustomized * 100) + (parseFloat(order.ORDER_AMOUNT) * 100))) / 100;
                } else {
                    this.totalToAuthorizeCustomized = Math.round(((this.totalToAuthorizeCustomized * 100) - (parseFloat(order.ORDER_AMOUNT) * 100))) / 100;
                }
            }
            
            this.customizedOrdersAllChecked = this.customizedOrders.every(ro => ro.SELECTED);
        }

        this.onChangeSelection.emit({selectedOrders : this.regularOrders.concat(this.customizedOrders).filter((o) => o.SELECTED)});
    }

    checkAll(checked : boolean, type : string) {

        if (type == 'regular-orders'){

            this.totalToAuthorizeRegular = 0;
            this.regularOrders.forEach(o => {
                o.SELECTED = checked;
        
                if (checked) {
                    this.totalToAuthorizeRegular = Math.round(((this.totalToAuthorizeRegular * 100) + (parseFloat(o.ORDER_AMOUNT) * 100))) / 100;
                }
            });        
        } else {

            this.totalToAuthorizeCustomized = 0;
            this.customizedOrders.forEach(o => {
                o.SELECTED = checked;
        
                if (checked) {
                    this.totalToAuthorizeCustomized = Math.round(((this.totalToAuthorizeCustomized * 100) + (parseFloat(o.ORDER_AMOUNT) * 100))) / 100;
                }
            });
        }

        this.onChangeSelection.emit({selectedOrders : this.regularOrders.concat(this.customizedOrders).filter((o) => o.SELECTED)});
    }

    ngAfterViewInit(): void {
        this.dtTrigger.next();
        this.dtTriggerCustomized.next();
    }

    ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
        this.dtTriggerCustomized.unsubscribe();
    }

    indexTrackBy(index: number, regularOrder: CustomerOrderData) {
        return regularOrder.ID;
    }
}