import { Component, EventEmitter, Host, Inject, Input  } from '@angular/core';

import { CustomerOrder } from '../../../_models';
import { OrderPaymentService } from '../../../_services/order-payment.service';
import { Observable } from 'rxjs';
import { InvoicePaymentService } from '../../../_services/invoice-payment.service';

@Component({
  selector: 'app-select-customer',
  templateUrl: './select-customer.component.html',
  outputs: ['onCustomerSelected'],
  inputs: ['parent']
})
export class SelectCustomerComponent {
    
    customerOrders : CustomerOrder[];
    onCustomerSelected : EventEmitter<CustomerOrder>;
    load$ : Observable<any>;
    parent : string;

    constructor(private _order : OrderPaymentService, private _invoice : InvoicePaymentService) {
        this.onCustomerSelected = new EventEmitter
        console.log('constructor select customer');
    }

    clicked(customer : CustomerOrder) {
        this.onCustomerSelected.emit(customer);
    }
    
    ngOnInit() {
        this.loadAllProviders();
        console.log('init select customer');
    }
    
    private loadAllProviders() {

        const that = this;

        if (this.parent == "order")
            this.load$ = this._order.getCustomerOrders()
        else 
            this.load$ = this._invoice.getCustomerOrders();

        this.load$.subscribe(
            data => {
                that.customerOrders = data;
            },
            error => {
                console.log(error);
            }
        );
    }

    public getOption(customerOrder) {

        return `${ customerOrder.CUSTOMERID } - ${ customerOrder.BILLTO_FIRSTNAME } ${ customerOrder.BILLTO_LASTNAME } - ${ customerOrder.BILLTO_EMAIL }`;
    }
}