import { Component } from '@angular/core';

@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  inputs: [ 'CUSTOMERID', 'BILLTO_FIRSTNAME', 'BILLTO_LASTNAME', 'BILLTO_EMAIL' ]
})

export class CustomerInfoComponent {
    
    CUSTOMERID : string;
    BILLTO_FIRSTNAME : string;
    BILLTO_LASTNAME : string;
    BILLTO_EMAIL : string;

    constructor() {
    }
}