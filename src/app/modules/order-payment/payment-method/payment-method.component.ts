import { Component, OnInit, OnDestroy, EventEmitter, Input } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import {PaymentMethod } from '../../../_models';
import { OrderPaymentService } from '../../../_services/order-payment.service';
import { map, concatAll, filter } from 'rxjs/operators';
import { InvoicePaymentService } from '../../../_services/invoice-payment.service';

@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.css'],
  outputs: ['onSelectPaymentMethod'],
  inputs: ['data', 'parent']
})

export class PaymentMethodComponent implements OnDestroy, OnInit {

    options : PaymentMethod[] = [];
    selected : PaymentMethod;
    onSelectPaymentMethod = new EventEmitter();
    data : string;
    parent : string;

    load$ : Observable<any[]>;
    
    private _subscription: Subscription;
    private eventsSubscription: Subscription;
    
    @Input() events: Observable<void>;
    @Input() changeMethodPaymentEnabled: Boolean = true;
    
    constructor(private _order: OrderPaymentService, private _invoice : InvoicePaymentService) {
    }
    
    ngOnInit() {
        this._loadAllProviders();
        this.eventsSubscription = this.events.subscribe(() => this.removePaymentMethod());
    }
    
    private _loadAllProviders() {

        this.load$ = (this.parent == "order") ? this._order.getPaymentMethodList(this.data) : this._invoice.getPaymentMethodList();
        
        this._subscription = this.load$
        .pipe(concatAll())
        .pipe(
            filter(pmt => 
                (pmt.PMT_METHOD_TYPE.toUpperCase() == "CC") ||
                (pmt.PMT_METHOD_TYPE.toUpperCase() == "EC") ||
                (pmt.PMT_METHOD_TYPE.toUpperCase() == "MAIL")
            ),
            map(pmt => {

                if (pmt.PMT_METHOD_TYPE.toUpperCase() == "CC") pmt.PAYMENT_METHOD = "PMT_METHOD_TYPE_CC";
                else if (pmt.PMT_METHOD_TYPE.toUpperCase() == "EC") pmt.PAYMENT_METHOD = "PMT_METHOD_TYPE_EC";
                else if (pmt.PMT_METHOD_TYPE.toUpperCase() == "MAIL") pmt.PAYMENT_METHOD = "PMT_METHOD_TYPE_MAIL";

                return pmt;
            })
        ).subscribe(pmt => {
            
            if (this.options.findIndex(i => (i.PMT_METHOD_TYPE.toUpperCase() == pmt.PMT_METHOD_TYPE.toUpperCase())) < 0) {
                this.options.push(pmt);
            }
        });
    }

    onSelectOption(option : PaymentMethod) {

        this.selected = option;
        this.onSelectPaymentMethod.emit(this.selected);
    }

    removePaymentMethod() {
        this.selected = null;
        this.onSelectPaymentMethod.emit(this.selected);
    }

    ngOnDestroy(): void {
        this._subscription.unsubscribe();
        this.eventsSubscription.unsubscribe();
    }
}