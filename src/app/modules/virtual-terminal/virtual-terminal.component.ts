import { Component, OnInit, Injector  } from '@angular/core';
import { TemplateComponent } from '../_shared/template.component';
import { Validators } from '@angular/forms';
import { ProductFormComponent } from './form/product-form.component';
import { ProductService, TransactionOriginService } from 'src/app/_services';
import { ListConfiguration } from 'src/app/_components/list/list.component';

declare var UIkit: any;

@Component({
  selector: 'virtual-terminal',
  templateUrl: './virtual-terminal.component.html',
  styleUrls: ['./virtual-terminal.component.scss']
})

export class VirtualTerminalComponent extends TemplateComponent implements OnInit {                    
    products = [];
    productList = new ListConfiguration();
    transactionOrigins = [];
    constructor(injector: Injector, private productService: ProductService, private transactionOriginService: TransactionOriginService) {
        super(injector);
        this.productList.labels = ['product','code','quantity','unitPrice','tax','total'];
        this.productList.columns = ['PRODUCT','SKU','QUANTITY','UNIT_PRICE','SALES_TAX','TOTAL_VALUE'];
    }
    
    ngOnInit() {      
        this.initForm();   
        this.load();           
    }

    initForm(){             
        this.form = this.formBuilder.group({                
            TXN_ORIGIN: [],
            REF_CODE: ['',Validators.required]
        });         
    }

    load() {
        this.transactionOriginService.get().subscribe(res => this.transactionOrigins = res);
        this.productService.get().subscribe(res => {
            this.products = res;
        })
    }

    createNew(form: ProductFormComponent){
        form.initForm(this.products);        
        this.showModal();
    }

    showModal(){
        UIkit.modal(document.getElementById('product-form')).show();        
    }

    closeModal(){                
        UIkit.modal(document.getElementById('product-form')).hide();
    }    

    addToList(product) {
        this.toastr.success('Product added.','Success'); 
        this.productList.rows.push(product);
        this.load();
    }

    getTotal() {
        if (this.productList.rows.length > 0) {
            let total = this.productList.rows.reduce((a, b) => { return a + b.TOTAL_VALUE}, 0);
            return total;
        }
    }

    remove(product) {
        this.productList.rows.splice(this.productList.rows.indexOf(product),1)
    }

    edit(form: ProductFormComponent, product){
        form.initForm(this.products, product.ID);        
        this.showModal();
    }
}