import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VirtualTerminalRoutingModule } from './virtual-terminal-routing.module';

import { VirtualTerminalComponent } from './virtual-terminal.component';
import { SharedModule } from '../_shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductService, TransactionOriginService } from 'src/app/_services';
import { ListModule } from '../../_components/list/list.module';
import { ProductFormComponent } from './form/product-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    VirtualTerminalRoutingModule,
    ListModule,
    SharedModule
  ],
  declarations: [VirtualTerminalComponent, ProductFormComponent],
  providers: [ProductService, TransactionOriginService]
})
export class VirtualTerminalModule { }
