import { Component, OnInit, Injector, Output, EventEmitter, ViewChild, Input, ElementRef } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

import { Product } from '../../../_models';
import { TemplateComponent } from '../../_shared/template.component';
import { ProductService } from '../../../_services/product.service';
import { MatSelect } from '@angular/material';

declare var UIkit: any;

@Component({
    selector: 'product-form',
    templateUrl: './product-form.component.html',
    styleUrls: ['./product-form.component.css']
})

export class ProductFormComponent extends TemplateComponent {    
    product = new Product();  
    selectedProduct = new Product();
    products: Product[];   
    formList: FormGroup;   
    productAdded = false;
    activePage = 0;
    @Output() add = new EventEmitter();
    @Input() productList: Product[] = [];
    @ViewChild(MatSelect) selectValue: MatSelect;
    @ViewChild('switcher') switcher: ElementRef;

    constructor(private productService: ProductService, injector: Injector) { 
        super(injector);    
    }

    ngOnInit() {
        this.initForm([]);
    }            

    initForm(products, id=null){     
        this.id = id;        
        this.products = products;  
        this.activePage = 0;
        this.selectedProduct = new Product();

        if (this.selectValue && !id) { 
            this.selectValue.value = null;
            this.selectValue.setDisabledState(false);
        }
        
        this.form = this.formBuilder.group({   
            PRODUCT:  ['', Validators.required],
            SKU:  [''],
            SALES_TAX: [''],
            UNIT_PRICE: ['', Validators.required],
            QUANTITY: ['', Validators.required],
            TOTAL_VALUE: ['']                                        
        });   
        
        this.formList = this.formBuilder.group({     
        ID: ['', Validators.required],
        PRODUCT:  ['', Validators.required],
        SKU:  [''],
        SALES_TAX: [''],
        UNIT_PRICE: ['', Validators.required],
        QUANTITY: ['', Validators.required],
        TOTAL_VALUE: [''] });

        if (id){                                                 
            this.selectedProduct = this.productList.filter(product => product.ID == id).shift();                
            this.formList.patchValue(this.selectedProduct);   
            this.selectedProduct = this.products.filter(product => product.ID == id)[0];

            setTimeout(() => {
                this.selectValue.setDisabledState(true);    
            }, 500);
                    
        }
        this.form.markAsUntouched();
        
        UIkit.switcher(this.switcher.nativeElement, {connect:'#form', animation: 'slide-horizontal', active: 0});
        UIkit.switcher(this.switcher.nativeElement).show(0);
    }
    
    addToList() {
        let product = new Product();

        this.activeForm.controls['TOTAL_VALUE'].setValue(this.getTotal());

        Object.assign(this.selectedProduct, this.activeForm.value);
        Object.assign(product, this.selectedProduct);

        if (this.validate(this.activeForm)) return 

        if (this.activePage == 1) {
            delete product.QUANTITY;
            delete product.TOTAL_VALUE;
            delete product.ID;

            this.productService.getByName(product.PRODUCT).subscribe(res => {
                if (res.length > 0) 
                    return this.productAdded = true;
                
                this.productService.create(null, product).subscribe(res => {
                    Object.assign(product, res[0]);
                    Object.assign(product, this.selectedProduct);
                    this.productAdded = false;
                    this.add.emit(product);
                    this.submit.emit();
                })
                
            })
        } else {
            if (this.productList.map(row => row.ID).includes(product.ID)) {
                if (this.id) {
                    Object.assign(this.productList[this.productList.indexOf(this.productList.filter(product => product.ID == this.id)[0])], this.selectedProduct)
                }
                return this.productAdded = true;
            }

            this.productAdded = false;
            this.add.emit(product);
            this.submit.emit();
        }
    }    

    getTotal() {
        return (parseInt(this.activeForm.controls['UNIT_PRICE'].value || 0) + parseInt(this.activeForm.controls['SALES_TAX'].value || 0)) * this.activeForm.controls['QUANTITY'].value 
    }

    get activeForm() {
        if (this.activePage === 0) return this.formList; else return this.form
    }

}