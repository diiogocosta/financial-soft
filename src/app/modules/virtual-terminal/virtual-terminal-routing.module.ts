import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VirtualTerminalComponent } from './virtual-terminal.component';
const routes: Routes = [
  {
    path: '', component: VirtualTerminalComponent
  }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VirtualTerminalRoutingModule { }
