﻿import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AuthGuard } from './_guards';
import { JwtInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService } from './_services';

import { ToastrModule } from 'ngx-toastr';
import { TranslateService } from './_translate/translate.service';

import {
	PerfectScrollbarModule,
	PERFECT_SCROLLBAR_CONFIG,
	PerfectScrollbarConfigInterface
} from 'ngx-perfect-scrollbar';
import { AppRoutingModule } from './app-routing.module';
import { LoginModule } from './login/login.module';
import { HomeModule } from './home/home.module';
import { LogoutModule } from './logout/logout.module';
import { RegisterModule } from './register/register.module';
import { ResendModule } from './resend/resend.module';
import { ResetModule } from './reset/reset.module';
import { HelpModule } from './help/help.module';
import { SharedModule } from './modules/_shared/shared.module';
import { OrderPaymentModule } from './modules/order-payment/order-payment.module';
import { InvoicePaymentModule } from './modules/invoice-payment/invoice-payment.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	suppressScrollX: true
};

export function setupTranslateFactory(
	service: TranslateService): Function {
	return () => service.use('pt');
}

@NgModule({
	imports: [
		BrowserModule,					
		HttpClientModule,
		BrowserAnimationsModule,		
		ToastrModule.forRoot(),		
		LoginModule,	
		LogoutModule,
		RegisterModule,
		ResendModule,
		ResetModule,
		OrderPaymentModule,
		InvoicePaymentModule,
		HelpModule,	
		SharedModule,
		AppRoutingModule
	],
	exports: [],
	declarations: [AppComponent],
	providers: [
		AuthGuard,
		AlertService,
		AuthenticationService,
		UserService,
		TranslateService,
		{
			provide: APP_INITIALIZER,
			useFactory: setupTranslateFactory,
			deps: [TranslateService],
			multi: true
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtInterceptor,
			multi: true
		},
		{
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }		
	],
	bootstrap: [AppComponent]
})

export class AppModule {
	public view = '';
	constructor() {
		this.view = 'LOGIN'
	}
}