export interface BrowserInfo {
    name : string;
    version : string;
}

export function getBrowserInfo() : BrowserInfo {

    let ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
        return {name:'IE ',version:(tem[1]||'')};
    }
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null) return {name:'Opera', version:tem[1]};
    }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return {
      name: M[0],
      version: M[1]
    };
}

export function getIpInfo() : Promise<any> {

    return new Promise(r=>{
        var w=window,
        a = new ((<any>w).RTCPeerConnection||(<any>w).mozRTCPeerConnection||(<any>w).webkitRTCPeerConnection)
        ({iceServers:[]}),
        b=()=>{};
        a.createDataChannel("");
        a.createOffer(c=>a.setLocalDescription(c,b,b),b);
        a.onicecandidate=c=>{
            try{
                c.candidate.candidate.match(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g).forEach(r)
            } catch(e)
            {}
        }
    });
}