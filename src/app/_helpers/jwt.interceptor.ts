import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http'
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { tap} from 'rxjs/operators';
import { AuthenticationService } from '../_services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private route: ActivatedRoute, private router: Router){ }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if(request.method != "options"){

            let token = localStorage.getItem('token-bearer')
            if (token) {
                request = request.clone({
                    setHeaders: { 
                        Authorization: `${token}`
                    }
                });
            }
        }

        return next.handle(request)
        .pipe(
	        tap(event => { }, error => {
                // if(request.url.indexOf('authentication/token') == -1){
                //     // http response status code
                //     console.log("----response----");
                //     console.error("status code:");
                //     console.error(error.status);
                //     console.error(error.message);
                //     console.log("--- end of response---");
                //     this.router.navigate(['/logout']);
                // }
	        })
	      );
    }
}