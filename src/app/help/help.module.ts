import { NgModule } from '@angular/core';
import { HelpComponent } from './help.component';
import { SharedModule } from '../modules/_shared/shared.module';

@NgModule({
  imports: [
    SharedModule    
  ],
  declarations: [HelpComponent],
  providers: []
})
export class HelpModule { }
