﻿import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, AuthenticationService } from '../_services';

@Component({ templateUrl: 'help.component.html' })
export class HelpComponent implements OnInit {
    view: string = 'HELP';
    ngOnInit(): void {
        
    }
}
