import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RichEditorComponent } from './rich-editor.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [RichEditorComponent],
  declarations: [RichEditorComponent]
})
export class RichEditorModule { }
