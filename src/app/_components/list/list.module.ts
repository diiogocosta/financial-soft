import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list.component';
import { DataTablesModule } from 'angular-datatables';
import { SharedModule } from '../../modules/_shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    SharedModule
  ],
  exports: [ListComponent],
  declarations: [ListComponent]
})
export class ListModule { }
