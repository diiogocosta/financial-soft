import { Component, OnInit, Input, ViewChild, Output, EventEmitter, AfterViewInit, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

export class ListConfiguration {
  labels = [];
  rows = [];
  columns= [];
}

@Component({
  selector: 'data-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() set rows(rows: any[]){ 
    this._rows = rows;
    if (rows.length > 0 && this.isLoaded)
      this.rerender();
  };
  @Input() columns: any[];
  @Input() labels: any[];
  @Output() edit = new EventEmitter();
  @Output() remove = new EventEmitter();
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  @Input() set dtOptions(options) {
    Object.assign(this._dtOptions,options);
  }; 
  dtTrigger= new Subject();
  private _rows;
  private _dtOptions : DataTables.Settings = {
    info: false,
    ordering: false,
    paging: false,
    searching: false
  };
  private isLoaded = false;

  constructor() { }

  ngOnInit() {
  }

  get rows() {
    return this._rows;
  }

  get dtOptions() {
    return this._dtOptions;
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {          
      dtInstance.destroy();          
      this.dtTrigger.next();
    });
  }

  ngAfterViewInit(){
    this.isLoaded = true;
    this.dtTrigger.next();     
    this.rerender();           
  }

  ngOnDestroy(): void {                        
    this.dtTrigger.unsubscribe();
  }

}
