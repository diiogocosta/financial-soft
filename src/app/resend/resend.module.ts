import { NgModule } from '@angular/core';
import { ResendComponent } from './resend.component';
import { SharedModule } from '../modules/_shared/shared.module';

@NgModule({
  imports: [
    SharedModule    
  ],
  declarations: [ResendComponent],
  providers: []
})
export class ResendModule { }
