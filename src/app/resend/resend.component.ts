﻿import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, AuthenticationService } from '../_services';

@Component({
    templateUrl: 'resend.component.html',
    styleUrls: ['../../assets/css/custom.css'],
    encapsulation: ViewEncapsulation.None
})
export class ResendComponent implements OnInit {
    view: string = 'RESEND';
    ngOnInit() {
       
    }
}
