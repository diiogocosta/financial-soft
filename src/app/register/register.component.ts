﻿import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, UserService } from '../_services';

@Component({
    templateUrl: 'register.component.html',
    styleUrls: ['../../assets/css/custom.css'],
    encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    validatingEmail = false;

    view: string = 'REGISTER';

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService)
    { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            company:    ['', Validators.required],
            firstName:  ['', Validators.required],
            lastName:   ['', Validators.required],
            email:      ['', Validators.required],
            workPhone:  [''],
            mobilePhone:  [''],
            phone:  [''],
            address:  ['', Validators.required],
            address2:  [''],
            city:  ['', Validators.required],
            state:  [''],
            country:  ['US', Validators.required],
            postalCode:  [''],
        });
    }

    get f() { return this.registerForm.controls; }

    onChangeEmail(e){
        console.log(this.registerForm.value.email)
    }

    onSubmit() {
        this.submitted = true;

        if (this.registerForm.invalid) {
            console.log('invalido')
            return;
        }
        console.log(this.registerForm.value);

        this.loading = true;

        this.userService.create(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    console.log(data);
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    error.error.erro = error.error.erro || "Desculpe-nos pelo inconveniente, houve um erro inesperado e já estamos trabalhando nisso."
                    this.alertService.error(error.error.erro);
                    this.loading = false;
                });
    }
}