import { NgModule } from '@angular/core';
import { RegisterComponent } from './register.component';
import { SharedModule } from '../modules/_shared/shared.module';

@NgModule({
  imports: [    
    SharedModule,    
  ],
  declarations: [RegisterComponent],
  providers: []
})
export class RegisterModule { }
