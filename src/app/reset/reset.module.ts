import { NgModule } from '@angular/core';
import { ResetComponent } from './reset.component';
import { SharedModule } from '../modules/_shared/shared.module';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [ResetComponent],
  providers: []
})
export class ResetModule { }
