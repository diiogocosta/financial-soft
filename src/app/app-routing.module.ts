﻿import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login';
import { LogoutComponent } from './logout';
import { RegisterComponent } from './register';
import { HelpComponent } from './help';
import { ResendComponent } from './resend';
import { ResetComponent } from './reset';
import { OrderPaymentPanelComponent } from './modules/order-payment/order-payment-panel/order-payment-panel.component';

const routes: Routes = [    
    { path: 'login', component: LoginComponent},    
    { path: 'page', loadChildren: './home/home.module#HomeModule'},    
    { path: 'logout', component: LogoutComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'help', component: HelpComponent },
    { path: 'resend', component: ResendComponent },
    { path: 'reset', component: ResetComponent },        
    { path: 'authorizations/:data', component: OrderPaymentPanelComponent, pathMatch: 'prefix'},
    { path: '**', redirectTo: 'page', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}