export class Settings {
    ID: string;
    URL_REST: string;
    MAINTENANCE_MODE: string;
    MAINTENANCE_DESCRIPTION: string;
    URL_REST_CONSULT: string;
    FTP_URL: string;
    FTP_ORDER_FOLDER: string;
    FTP_INVOICE_FOLDER: string
    FTP_PASS: string;
    FTP_USER: string;
}