export class Provider {
    ACCOUNT_UPDATE_FLAG : string;
    ACTIVE : string;
    AMEX_AUTH_RTIND : string;
    AMEX_BATCH_RTIND : string;
    COUNTRY_ISO_CODE : string;
    DECIMAL_SEPARATOR : string;
    ID : string;
    INTERNAL_PROVIDER : string;
    PRD_FLAG : string;
    NAME : string;
    PROVIDER_CODE : string;
    PRD_ENDPOINT: string;
    RECONC_PURCHASE_REFUND_URL : string;
    TOKENIZATION_FLG : string;
    TST_ENDPOINT: string;
}