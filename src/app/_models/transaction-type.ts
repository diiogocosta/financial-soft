export class TransactionType {
    ID: string;
    PROVIDER_ID: string;
    TXN_NAME: string;
    DISPLAY_NAME: string;
    DISPLAY_FLG: string;
    TYPE: string;    
    PROVIDER?: string;
}