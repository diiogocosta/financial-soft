export class Currency {
    CURRENCY: string;
    ACTIVE: number;
    LABEL: string;
    SYMBOL: string;
    NUM_CODE: number;    
}