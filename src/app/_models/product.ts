export class Product {
    ID: string;
    PRODUCT:  string;
    SKU:  string;
    SALES_TAX: number;
    UNIT_PRICE: number;
    QUANTITY?: number;
    TOTAL_VALUE?: number;
}