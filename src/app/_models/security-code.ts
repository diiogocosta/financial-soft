export class SecurityCode {
    ID: string;
    PROVIDER_ID: string;
    CODE_TYPE: string;
    CODE: string;
    DESCRIPTION: string;    
    PROVIDER?: string;
}