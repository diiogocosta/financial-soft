export class PaymentMethod {
    ID: string;
    PAYMENT_METHOD: string;
    PMT_METHOD_TYPE: string;
    PROVIDER_DECRYPT_FLG: string;
}