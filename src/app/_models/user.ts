﻿export class User {
    id: number;
    company: string;
    firstName: string;
    lastName: string;
    email: string;
    workPhone: string;
    mobilePhone: string;
    phone: string;
    address: string;
    address2: string;
    city: string;
    state: string;
    country: string;
    postalCode: string;
}