export class ReasonCode {
    ID: string;
    PROVIDER_ID: string;
    INTERNAL_PROV_REASON_CODE_ID: string;
    CODE: string;
    DESCRIPTION: string;    
    PROVIDER?: string;
}