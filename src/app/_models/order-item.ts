export class OrderItem {
    ID : string;
    SKU : string;
    UNITPRICE : string;
    QUANTITY : string;
    EXTENDEDPRICE : string;
}