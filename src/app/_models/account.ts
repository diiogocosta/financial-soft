export class Account {
    ID: string;
    NAME: string;
    ADDRESS: string;
    CITY: string;
    STATE: string;
    COUNTY: string;
    COUNTRY: string;
    POSTALCODE: string
    EMAIL: string;
}