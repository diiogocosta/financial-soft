import { CustomerOrderData } from "./customer-order-data";

export class Payment {
    
    CUSTOMERID : string;
    ORDER_ID : string[];
    PAYEE_CREDENTIALS : string;
    CARD_TYPE ?: string;
    FULL_NAME ?: string;
    EXPIRATION_MONTH ?: string;
    EXPIRATION_YEAR ?: string;
    CURRENCY ?: string;
    CV_NUMBER ?: string;
    ACCOUNT_NUMBER ?: string;
    BANK_TRANSIT_NUMBER ?: string;
    CHECK_ACCOUNT_NUMBER ?: string;
    ACCOUNT_TYPE ?: string;
    EMAIL ?: string;
    MANUAL_TYPE ?: string;
    DATA ?: string;
}