export class Card {
    ID?: string;
    PROVIDER_ID?: string;
    TYPE?: string;
    CARD?: string;
    ACTIVE?: string;
    REGEX_VALID_CARD?: string;    
    PRE_AUTH_AMOUNT?: number;
    AUTH_REVERS_DAYS?: number;    
    PROVIDER_CODE?: string;    
    INSTALLMENT_TYPE?: string;
    INSTALLMENT?: number;
    PROVIDER?: string;
    SERVICE_FEE ?: string;
    SERVICE_FEE_PERCENTAGE ?: string;
}