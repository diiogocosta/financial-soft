export class CustomerInvoiceData {
    "ID": string;
    "DT_CREATED": string;
    "DT_UPDATED": string;
    "CREATED_BY_USER_ID": string;
    "UPDATED_BY_USER_ID": string;
    "CREATED_BY_SYSTEM": string;
    "UPDATED_BY_SYSTEM": string;
    "PROFILE_ID": string;
    "MERCHANTID": string;
    "CUSTOMERID": string;
    "INVOICENUMBER": string;
    "MERCHANTREFERENCECODE": string;
    "AMOUNT": string;
    "TAX": string;
    "TOTAL_AMOUNT": string;
    "PAID_AMOUNT": string;
    "OPEN_AMOUNT": string;
    "AMOUNT_TO_PAY": string;
    "DUE_DATE": string;
    "ISSUE_DATE": string;
    "BILLTO_CITY": string;
    "BILLTO_COUNTRY": string;
    "BILLTO_CUSTOMERID": string;
    "BILLTO_EMAIL": string;
    "BILLTO_FIRSTNAME": string;
    "BILLTO_LASTNAME": string;
    "BILLTO_PHONENUMBER": string;
    "BILLTO_POSTALCODE": string;
    "BILLTO_STATE": string;
    "BILLTO_STREET1": string;
    "BILLTO_STREET2": string;
    "SHIPTO_CITY": string;
    "SHIPTO_COUNTRY": string;
    "SHIPTO_FIRSTNAME": string;
    "SHIPTO_LASTNAME": string;
    "SHIPTO_PHONENUMBER": string;
    "SHIPTO_POSTALCODE": string;
    "SHIPTO_STATE": string;
    "SHIPTO_STREET1": string;
    "SHIPTO_STREET2": string;
    "STATUS": string;
    "PAYMENT_TYPE": string;
    "CURRENCY": string;
    "LAST_PAID": string;
    "SEND_EMAIL_FLG": string;
    "NAME": string;
    "ACTIVE": string;
    "ACCOUNT_ID": string;
    "CURRENCY_ID": string;
    "LOGO": string;
    "PASS": string;
    "OVERRIDE_ERRORURL": string;
    "OVERRIDE_SUCCESSURL": string;
    "OVERRIDE_REJECTURL": string;
    "FORM_TYPE": string;
    "SHOW_CARD_IMAGE": string;
    "SPLIT_PAYMENT": string;
    "ORDER_DETAILS": string;
    "BILING_INFORMATION": string;
    "PAYMENT_DETAILS": string;
    "TEXT_BUTTON_SEND": string;
    "POSTBACKURL": string;
    "ORDER_DETAILS_VIRTUAL": string;
    "BILING_INFORMATION_VIRTUAL": string;
    "PAYMENT_DETAILS_VIRTUAL": string;
    "TEXT_BUTTON_SEND_VIRTUAL": string;
    "SHIPPING_INFORMATION_VIRTUAL": string;
    "INVOICE_DESCRIPTION": string;
    "INVOICE_DESCRIPTION_CONTACT": string;
    "IGNORE_AVS": string;
    "IGNORE_CV": string;
    "ENABLE_AUTO_REJECT": string;
    "CURRENCY_CONVERSION_FLG": string;
    "CURRENCY_CONVERSION_AGREE_TEXT": string;
    "CURRENCY_CONVERSION_SERV_RATE": string;
    "SMTP_HOST": string;
    "SMTP_EMAIL": string;
    "SMTP_PASS": string;
    "SMTP_PORT": string;
    "ENABLE_FRAUD_CHECK": string;
    "ENABLE_FR_CHECK_AFT_AUTH": string;
    "ENABLE_FR_CHECK_BEF_AUTH": string;
    "ENABLE_FR_CHECK_RUN_ON_REJECT": string;
    "COUNTRY_CODE": string;
    "MERCHANT_CATEGORY_CODE": string;
    "TERMINAL_ID": string;
    "FORCE_COMPARE": string;
    "CONDITION_TYPE": string;
    "PRIMARY_EMAIL_ADDRESS": string;
    "LOCALE_ID": string;
    "PAYMENT_INSTRUCTIONS" ?: string;
    "SELECTED" ?: boolean;
}