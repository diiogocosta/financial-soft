export class Bank {
    ID: string;
    PROVIDER_ID: string;
    BANK: string;
    ACTIVE: string;
    TYPE: string;
    URL_ICON: string;    
    PROVIDER_CODE: string;    
    PROVIDER?: string;
}
