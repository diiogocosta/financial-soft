export class CustomerInvoice {
    BILLTO_EMAIL : string;
    CUSTOMERID : string;
    BILLTO_FIRSTNAME : string;
    BILLTO_LASTNAME : string;
}