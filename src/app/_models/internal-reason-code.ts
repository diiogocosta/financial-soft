export class InternalReasonCode {
    ID: string;
    CODE: string;
    INTERNAL_PROV_REASON_CODE_ID: string;    
    DESCRIPTION: string;        
}