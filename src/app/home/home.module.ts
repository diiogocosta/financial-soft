import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HeadModule } from '../_directives/head/head.module';
import { MenuModule } from '../_directives/menu/menu.module';
import { SharedModule } from '../modules/_shared/shared.module';

@NgModule({
  imports: [    
    HomeRoutingModule,                
    HeadModule,
    MenuModule,
    SharedModule
  ],
  declarations: [
    HomeComponent    
  ],
  exports: [HomeComponent]  
})
export class HomeModule { }
