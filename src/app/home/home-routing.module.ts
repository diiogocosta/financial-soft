import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../_guards';
import { HomeComponent } from './home.component';
const routes: Routes = [    
    { path: '', component: HomeComponent, children:[
      { path: '', loadChildren: '../modules/dashboard/dashboard.module#DashboardModule'},
      { path: 'order-payment', loadChildren: '../modules/order-payment/order-payment.module#OrderPaymentModule'},
      { path: 'invoice-payment', loadChildren: '../modules/invoice-payment/invoice-payment.module#InvoicePaymentModule'},
      { path: 'virtual-terminal', loadChildren: '../modules/virtual-terminal/virtual-terminal.module#VirtualTerminalModule'},
      { path: 'provider', loadChildren: '../modules/provider/provider.module#ProviderModule'},
      { path: 'dashboard', loadChildren: '../modules/dashboard/dashboard.module#DashboardModule'},
      { path: 'account', loadChildren: '../modules/account/account.module#AccountModule' },
      { path: 'settings', loadChildren: '../modules/settings/settings.module#SettingsModule' },
      { path: 'currencies', loadChildren: '../modules/currencies/currencies.module#CurrenciesModule' },
      { path: 'security-codes', loadChildren: '../modules/security-codes/security-codes.module#SecurityCodesModule' },
      { path: 'transaction-types', loadChildren: '../modules/transaction-types/transaction-types.module#TransactionTypesModule' },
      { path: 'txn-origin', loadChildren: '../modules/transaction-origin/transaction-origin.module#TransactionOriginModule' },
      { path: 'banks', loadChildren: '../modules/banks/banks.module#BankModule' },
      { path: 'cards', loadChildren: '../modules/cards/cards.module#CardModule' },
      { path: 'payment-methods', loadChildren: '../modules/payment-methods/payment-methods.module#PaymentMethodModule' },    
      { path: 'reason-codes', loadChildren: '../modules/reason-codes/reason-codes.module#ReasonCodesModule' },              
    ]}

    // redirect to home    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
