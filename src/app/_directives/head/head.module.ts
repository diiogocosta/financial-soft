import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeadComponent } from './head.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,   
    RouterModule         
  ],
  declarations: [    
    HeadComponent,    
  ],
  exports: [HeadComponent]  
})
export class HeadModule { }
