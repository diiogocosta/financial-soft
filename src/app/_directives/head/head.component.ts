﻿import { Component, Renderer2 } from '@angular/core';

@Component({
    selector: 'heading',
    templateUrl: 'head.component.html'
})

export class HeadComponent{
    private sidebar : boolean = true;
    public profile : boolean = false;
    constructor(private renderer: Renderer2) {
        this.renderer.removeClass(document.body, 'login_page');
        this.renderer.addClass(document.body, 'sidebar_main_active');
        this.renderer.addClass(document.body, 'sidebar_main_swipe');
        this.renderer.addClass(document.body, 'header_full');
     }
    toggleSidebar(){
        if(this.sidebar)
            this.renderer.removeClass(document.body, 'sidebar_main_active');
        else
            this.renderer.addClass(document.body, 'sidebar_main_active');
        this.sidebar = !this.sidebar;

    }

    toggleProfile(){
        this.profile = !this.profile;
    }
}