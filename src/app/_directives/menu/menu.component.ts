﻿import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'menu',
    templateUrl: 'menu.component.html',
    styleUrls: ['menu.component.scss']
})

export class MenuComponent implements OnInit {
    public permissions;
    activeTitle = 'Dashboard';
    constructor() { }        

    ngOnInit() {
        this.permissions = JSON.parse(localStorage.getItem("token-body")).permissions;        
    }

    showIf(item){    
        return true;    
        //@FIXME - REMOVER APÓS ESTILIZAR MENU.
        // return item == '' || (typeof this.permissions[item] != 'undefined' && this.permissions[item]['READ_FLAG'] == 'Y');
    }

    public menuToggle = {};

    toggle(item){
        console.log(item);
        if(typeof this.menuToggle[item] == "undefined")
            this.menuToggle[item] = false;

        this.menuToggle[item] = !this.menuToggle[item];
    }

    isActive(title) {
        return title == this.activeTitle;
    }
}