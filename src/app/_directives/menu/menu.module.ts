import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MenuComponent } from './menu.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
  imports: [
    CommonModule,   
    RouterModule,
    PerfectScrollbarModule,   
  ],
  declarations: [    
    MenuComponent,    
  ],
  exports: [MenuComponent]  
})
export class MenuModule { }
