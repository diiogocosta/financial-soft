import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { SharedModule } from '../modules/_shared/shared.module';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [LoginComponent],
  exports: [LoginComponent],
  providers: []
})
export class LoginModule { }
