﻿import { Component, OnInit, ViewEncapsulation, Renderer2, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, AuthenticationService } from '../_services';
import { TemplateComponent } from '../modules/_shared/template.component';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['../../assets/css/custom.css'],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent extends TemplateComponent implements OnInit {    
    loading = false;
    submitted = false;
    returnUrl: string;

    view: string = 'LOGIN';

    constructor(injector: Injector,
        private renderer: Renderer2,                 
        private authenticationService: AuthenticationService,
        private alertService: AlertService) { super(injector)}

    ngOnInit() {        
        this.renderer.addClass(document.body, 'login_page');        

        this.form = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        this.authenticationService.logout();        
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'page';        
    }    

    onSubmit() {
        this.submitted = true;
        
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        
        this.authenticationService.login(this.f('username').value, this.f('password').value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    var msgError = error.error.Message || "Erro ao efetuar o seu login";
                    this.alertService.error(msgError);
                    this.loading = false;
                });
    }
}
